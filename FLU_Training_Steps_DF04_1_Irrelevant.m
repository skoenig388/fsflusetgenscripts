clear,clc,close all

rng(20181201); %seed random number generator, do not change EVER &...
%always run file from the start and generate all files

current_dir = pwd;

RewardMag = 3;
context_nums = -1;
max_num_distractors = 1;%number of distractors

num_blocks = 11;%must be odd number, want 10 switches
num_sets = 20;
min_num_trials = [30 35 40 45];
max_num_trials = 80;

shapes = {'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

patternless_colors = {'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070287_6070287', 'C6070335_6070335'};
%patterned_colors =   {'C7000000_5000000', 'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000', 'C7070286_5000000', 'C7070335_5000000'};
gray_pattern_color = 'C7000000_5000000';

arms = {'A00_E01', 'A00_E02','A00_E03', 'A01_E00', 'A01_E01', 'A01_E02', 'A01_E03', 'A02_E00', 'A02_E01', 'A02_E02', 'A02_E03'};

%%
file_base_name = 'FLU_DF04_1_irrel_Set';
for file = 1:num_sets
    
    %---Determine Stimuli to Show---%
    %use brute force method to randomly generate files
    %trys to balance all feature dims since can only show once per session
    %but does not explicilty control for this
    
    generated = false;
    attempts = 0;
    while ~generated
        
        %Target
        %Randomize block pairs and Remove Difficult pairs to discrimnate
        % Colors "next" to each other
        % Wavy(6) vs Triangular(7) && Checkered(04) Vs Diamonds (05)
        shapes_index = randperm(length(shapes));
        pattern_index = 100+randperm(length(patterns));
        while any(abs(diff(pattern_index)) == 1)
            pattern_index = 100+randperm(length(patterns));
        end
        color_index = 200+randperm(length(patternless_colors));
        %circular color space so begining and end can't be next to each other
        while any(abs(diff(color_index)) == 1) || any(abs(color_index(1:2:end-1)-color_index(2:2:end)) == 6)
            color_index = 200+randperm(length(patternless_colors));
        end
        arm_index = 300+randperm(length(arms));
        
        intra_extra = [zeros(1,(num_blocks-1)/2) ones(1,(num_blocks-1)/2)];
        intra_extra = intra_extra(randperm(length(intra_extra)));
        
        %Distractor
        %Randomize block pairs and Remove Difficult pairs to discrimnate
        % Colors "next" to each other
        % Wavy(6) vs Triangular(7) && Checkered(04) Vs Diamonds (05)
        shapes_index2 = randperm(length(shapes));
        pattern_index2 = 100+randperm(length(patterns));
        while any(abs(diff(pattern_index2)) == 1)
            pattern_index2 = 100+randperm(length(patterns));
        end
        color_index2 = 200+randperm(length(patternless_colors));
        %circular color space so begining and end can't be next to each other
        while any(abs(diff(color_index2)) == 1) || any(abs(color_index2(1:2:end-1)-color_index2(2:2:end)) == 6)
            color_index2 = 200+randperm(length(patternless_colors));
        end
        arm_index2 = 300+randperm(length(arms));
        
        attempts = attempts+1;
        
        stimuli = NaN(num_blocks,2);
        shape_ind = 1;
        pattern_ind = 1;
        color_ind = 1;
        arm_ind = 1;
        
        distractor_stimuli = NaN(num_blocks,2);
        distractor_shape_ind = 1;
        distractor_pattern_ind = 1;
        distractor_color_ind = 1;
        distractor_arm_ind = 1;
        
        for b = 1:num_blocks
            if b == 1 %pick a random stimulus for the first set
                dim = randi(4);
            elseif intra_extra(b-1) == 0 %intra dimensional shift
                dim = dim; %same as last time
            else %extra-dimensional shift
                dims = 1:4;
                dims(dims == dim) = [];
                dim = dims(randi(3));
            end
            
            distractor_dim = [1 2 3 4];
            distractor_dim(dim) = [];
            distractor_dim = distractor_dim(randi(3));
            
            if dim == 1
                if shape_ind > length(shapes_index)/2
                    %disp('Ran out of shape stimuli to show...trying again')
                    break
                end
                stimuli(b,:) = [shapes_index(2*shape_ind-1) shapes_index(2*shape_ind)];
                shape_ind = shape_ind + 1;
            elseif dim == 2
                if pattern_ind > length(pattern_index)/2
                    %disp('Ran out of patterned stimuli to show...trying again')
                    break
                end
                stimuli(b,:) = [pattern_index(2*pattern_ind-1) pattern_index(2*pattern_ind)];
                pattern_ind = pattern_ind + 1;
            elseif dim == 3
                if color_ind > length(color_index)/2
                    %disp('Ran out of color stimuli to show...trying again')
                    break
                end
                stimuli(b,:) = [color_index(2*color_ind-1) color_index(2*color_ind)];
                color_ind = color_ind + 1;
                
            elseif dim == 4
                if arm_ind > length(arm_index)/2
                    %disp('Ran out of arm stimuli to show...trying again')
                    break
                end
                stimuli(b,:) = [arm_index(2*arm_ind-1) arm_index(2*arm_ind)];
                arm_ind = arm_ind + 1;
            else
                error('Stimulus dimension not recognized')
            end
            
            if distractor_dim == 1
                if distractor_shape_ind > length(shapes_index)/2
                    %disp('Ran out of shape distractor_stimuli to show...trying again')
                    break
                end
                distractor_stimuli(b,:) = [shapes_index(2*distractor_shape_ind-1) shapes_index(2*distractor_shape_ind)];
                distractor_shape_ind = distractor_shape_ind + 1;
            elseif distractor_dim == 2
                if distractor_pattern_ind > length(pattern_index)/2
                    %disp('Ran out of patterned distractor_stimuli to show...trying again')
                    break
                end
                distractor_stimuli(b,:) = [pattern_index(2*distractor_pattern_ind-1) pattern_index(2*distractor_pattern_ind)];
                distractor_pattern_ind = distractor_pattern_ind + 1;
            elseif distractor_dim == 3
                if distractor_color_ind > length(color_index)/2
                    %disp('Ran out of color distractor_stimuli to show...trying again')
                    break
                end
                distractor_stimuli(b,:) = [color_index(2*distractor_color_ind-1) color_index(2*distractor_color_ind)];
                distractor_color_ind = distractor_color_ind + 1;
            elseif distractor_dim == 4
                if distractor_arm_ind > length(arm_index)/2
                    %disp('Ran out of arm distractor_stimuli to show...trying again')
                    break
                end
                distractor_stimuli(b,:) = [arm_index(2*distractor_arm_ind-1) arm_index(2*distractor_arm_ind)];
                distractor_arm_ind = distractor_arm_ind + 1;
            else
                error('Stimulus distractor_dimension not recognized')
            end
        end
        if sum(isnan(stimuli(:))) == 0 && sum(isnan(distractor_stimuli(:))) == 0
            generated = true;
            disp(['Number of attempts = ' num2str(attempts)]);
        elseif any(abs(stimuli(:)-distractor_stimuli(:)) < 15)
            error('Overlapping Relevant and Irrelevant Dimensions')
        else
            generated = false;
        end
    end

    %---Generate the BlockDef----%
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    num_Quaddles = max_num_distractors+1;
    
    for block = 1:num_blocks
        num_trials1 = min_num_trials(randi(length(min_num_trials)));
        num_trials2 = max_num_trials;
        
        odd_stim_ind = 2*(file-1)+1;
        even_stim_ind = 2*(file-1)+2;
        
        %---Make Active Array---%
        shape_str = [];
        pattern_str = [];
        color_str = [];
        arm_str = [];
        
        if stimuli(block,1) < 100 %shape
            shape_str = ['[' num2str(stimuli(block,1)) ',' num2str(stimuli(block,2)) ']'];
        elseif stimuli(block,1) < 200 %pattern
            pattern_str = ['[' num2str(stimuli(block,1)-100) ',' num2str(stimuli(block,2)-100) ']'];
        elseif stimuli(block,1) < 300 %color
             color_str = ['[' num2str(stimuli(block,1)-200) ',' num2str(stimuli(block,2)-200) ']'];
        elseif stimuli(block,1) < 400 %amrs
             arm_str = ['[' num2str(stimuli(block,1)-300) ',' num2str(stimuli(block,2)-300) ']'];
        else
            error('Stimulus not recognized')
        end
        
        if distractor_stimuli(block,1) < 100 %shape
            shape_str = ['[' num2str(distractor_stimuli(block,1)) ',' num2str(distractor_stimuli(block,2)) ']'];
        elseif distractor_stimuli(block,1) < 200 %pattern
            pattern_str = ['[' num2str(distractor_stimuli(block,1)-100) ',' num2str(distractor_stimuli(block,2)-100) ']'];
        elseif distractor_stimuli(block,1) < 300 %color
             color_str = ['[' num2str(distractor_stimuli(block,1)-200) ',' num2str(distractor_stimuli(block,2)-200) ']'];
        elseif distractor_stimuli(block,1) < 400 %amrs
             arm_str = ['[' num2str(distractor_stimuli(block,1)-300) ',' num2str(distractor_stimuli(block,2)-300) ']'];
        else
            error('Stimulus not recognized')
        end
        
        if isempty(shape_str)
            shape_str = '[0]';
        end
        if isempty(pattern_str)
            pattern_str = '[0]';
        end
        if isempty(color_str)
            color_str = '[0]';
        end
        if isempty(arm_str)
            arm_str = '[0]';
        end
        
        active_array = [shape_str ',' pattern_str ',' color_str ',[0],' arm_str];
          
        
        %---Make Rule Array---%
        %since stimuli are chosen at random jsut set 1st one as rewarded
        if stimuli(block,1) < 100 %shape
            rule_array = ['[' num2str(stimuli(block,1)) '],[-1],[-1],[-1],[-1]'];%since stimuli are chosen at random jsut set 1st one as rewarded
        elseif stimuli(block,1) < 200 %pattern
            rule_array = ['[-1],[' num2str(stimuli(block,1)-100) '],[-1],[-1],[-1]'];%since stimuli are chosen at random jsut set 1st one as rewarded
        elseif stimuli(block,1) < 300 %color
            rule_array = ['[-1],[-1],[' num2str(stimuli(block,1)-200) '],[-1],[-1]'];
        elseif stimuli(block,1) < 400 %amrs
            rule_array = ['[-1],[-1],[-1],[-1],[' num2str(stimuli(block,1)-300) ']'];
        else
            error('Stimulus not recognized')
        end
        
        if block == 1
            block_name = [filebase '_B' num2str(block) '_First'];
        else
            if intra_extra(block-1) == 1 %extra dimensional shift
                block_name = [filebase '_B' num2str(block) '_Extra'];
            else %intra-dimensional shift
                
                block_name = [filebase '_B' num2str(block) '_Intra'];
            end
        end
        
        bdstr2 = ['{"BlockID": "blockid.' block_name '",' ...
            ' "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '],' ...
            ' "ActiveFeatureTemplate": [' active_array '],' ...
            ' "ContextNums": [' num2str(context_nums) '],' ...
            ' "NumRelevantObjectsPerTrial": [' num2str(num_Quaddles) ',' num2str(num_Quaddles) '], "NumIrrelevantObjectsPerTrial": [0,0] ,'...
            ' "NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [' rule_array '], "ContextNums": [' num2str(context_nums) '], ' ...
            '"RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if block == num_blocks
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
    end
    fclose(blockfID);
end
cd(current_dir)
fclose all;

