clear,clc,close all

rng(20181101); %seed random number generator, do not change EVER &...
%always run file from the start and generate all files

current_dir = pwd;

RewardMag = 3;
context_nums = -1;
max_num_distractors = 1;%number of distractors

num_blocks = 11;%want 10 switches
min_num_trials = 30;
max_num_trials = 80;

shapes = {'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
arms = {'A01_E00', 'A02_E00', 'A00_E01', 'A00_E02', 'A00_E03'};
patterns = {'P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

patternless_colors = {'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070287_6070287', 'C6070335_6070335'};
%patterned_colors =   {'C7000000_5000000', 'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000', 'C7070286_5000000', 'C7070335_5000000'};
gray_pattern_color = 'C7000000_5000000';

%used for indexing in Unity's array
full_arms_array = {'A00_E00', 'A00_E01', 'A00_E02','A00_E03', 'A01_E00', 'A01_E01', 'A01_E02', 'A01_E03', 'A02_E00', 'A02_E01', 'A02_E02', 'A02_E03'};

true_arm_index = NaN(1,length(arms));
for a = 1:length(arms)
    true_arm_index(a) = find(contains(full_arms_array,arms{a}))-1;%reindex since Unity starts at 0
end
%% Randomize and Remove Difficult pairs to discrimnate
% Colors "next" to each other
% Wavy(6) vs Triangular(7) && Checkered(04) Vs Diamonds (05)
shape_index = randperm(length(shapes));
arm_index = randperm(length(arms));
pattern_index = randperm(length(patterns));
while any(abs(diff(pattern_index)) == 1)
    pattern_index = randperm(length(patterns));
end
color_index = randperm(length(patternless_colors));
%circular color space so begining and end can't be next to each other
while any(abs(diff(color_index)) == 1) || any(abs(color_index(1:2:end-1)-color_index(2:2:end)) == 6)
    color_index = randperm(length(patternless_colors));
end

%%
%---For Colors---%
file_base_name = 'FLU_DF02_Color_Set';
for file = 1:floor(length(color_index)/2)
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    odd_stim_ind = 2*(file-1)+1;
    even_stim_ind = 2*(file-1)+2;
    
    active_array = ['[0],[0],[' num2str(color_index(odd_stim_ind)) ',' num2str(color_index(even_stim_ind)) '],[0],[0]'];
    odd_rule_array = ['[-1],[-1],[' num2str(color_index(odd_stim_ind)) '],[-1],[-1]'];
    even_rule_array = ['[-1],[-1],[' num2str(color_index(even_stim_ind)) '],[-1],[-1]'];
    
    num_trials1 = min_num_trials;
    num_trials2 = max_num_trials;
    
    num_Quaddles = max_num_distractors+1;
    
    
    for block = 1:num_blocks
        
        if rem(block,2) == 1
            bdstr2 = ['{"BlockID": "blockid.' [filebase '_B' num2str(block)] '",' ...
                ' "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '],' ...
                ' "ActiveFeatureTemplate": [' active_array '],' ...
                ' "ContextNums": [' num2str(context_nums) '],' ...
                ' "NumRelevantObjectsPerTrial": [' num2str(num_Quaddles) ',' num2str(num_Quaddles) '], "NumIrrelevantObjectsPerTrial": [0,0] ,'...
                ' "NumIrrelevantObjectsPerBlock": 0,...\n' ...
                '"RuleArray": [{"RelevantFeatureTemplate": [' odd_rule_array '], "ContextNums": [' num2str(context_nums) '], ' ...
                '"RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
                '],...' '\n'...
                '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
            
        else
            bdstr2 = ['{"BlockID": "blockid.' [filebase '_B' num2str(block)] '",' ...
                ' "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '],' ...
                ' "ActiveFeatureTemplate": [' active_array '],' ...
                ' "ContextNums": [' num2str(context_nums) '],' ...
                ' "NumRelevantObjectsPerTrial": [' num2str(num_Quaddles) ',' num2str(num_Quaddles) '], "NumIrrelevantObjectsPerTrial": [0,0] ,'...
                ' "NumIrrelevantObjectsPerBlock": 0,...\n' ...
                '"RuleArray": [{"RelevantFeatureTemplate": [' even_rule_array '], "ContextNums": [' num2str(context_nums) '], ' ...
                '"RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
                '],...' '\n'...
                '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        end
        
        if block == num_blocks
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
    end
end
fclose(blockfID);
cd(current_dir)
fclose all;

%---For Shapes---%
file_base_name = 'FLU_DF02_Shape_Set';
for file = 1:floor(length(shape_index)/2)
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    odd_stim_ind = 2*(file-1)+1;
    even_stim_ind = 2*(file-1)+2;
    
    active_array = ['[' num2str(shape_index(odd_stim_ind)) ',' num2str(shape_index(even_stim_ind)) '],[0],[0],[0],[0]'];
    odd_rule_array = ['[' num2str(shape_index(odd_stim_ind)) '],[-1],[-1],[-1],[-1]'];
    even_rule_array = ['[' num2str(shape_index(even_stim_ind)) '],[-1],[-1],[-1],[-1]'];
    
    num_trials1 = min_num_trials;
    num_trials2 = max_num_trials;
    
    num_Quaddles = max_num_distractors+1;
    
    
    for block = 1:num_blocks
        
        if rem(block,2) == 1
            bdstr2 = ['{"BlockID": "blockid.' [filebase '_B' num2str(block)] '",' ...
                ' "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '],' ...
                ' "ActiveFeatureTemplate": [' active_array '],' ...
                ' "ContextNums": [' num2str(context_nums) '],' ...
                ' "NumRelevantObjectsPerTrial": [' num2str(num_Quaddles) ',' num2str(num_Quaddles) '], "NumIrrelevantObjectsPerTrial": [0,0] ,'...
                ' "NumIrrelevantObjectsPerBlock": 0,...\n' ...
                '"RuleArray": [{"RelevantFeatureTemplate": [' odd_rule_array '], "ContextNums": [' num2str(context_nums) '], ' ...
                '"RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
                '],...' '\n'...
                '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
            
        else
            bdstr2 = ['{"BlockID": "blockid.' [filebase '_B' num2str(block)] '",' ...
                ' "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '],' ...
                ' "ActiveFeatureTemplate": [' active_array '],' ...
                ' "ContextNums": [' num2str(context_nums) '],' ...
                ' "NumRelevantObjectsPerTrial": [' num2str(num_Quaddles) ',' num2str(num_Quaddles) '], "NumIrrelevantObjectsPerTrial": [0,0] ,'...
                ' "NumIrrelevantObjectsPerBlock": 0,...\n' ...
                '"RuleArray": [{"RelevantFeatureTemplate": [' even_rule_array '], "ContextNums": [' num2str(context_nums) '], ' ...
                '"RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
                '],...' '\n'...
                '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        end
        
        if block == num_blocks
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
    end
end
fclose(blockfID);
cd(current_dir)
fclose all;

%---For Arms---%
file_base_name = 'FLU_DF02_Arm_Set';
for file = 1:floor(length(arm_index)/2)
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    odd_stim_ind = 2*(file-1)+1;
    even_stim_ind = 2*(file-1)+2;
    
    active_array = ['[0],[0],[0],[0],[' num2str(true_arm_index(arm_index(odd_stim_ind))) ',' num2str(true_arm_index(arm_index(even_stim_ind))) ']'];
    odd_rule_array = ['[-1],[-1],[-1],[-1],[' num2str(true_arm_index(arm_index(odd_stim_ind))) ']'];
    even_rule_array = ['[-1],[-1],[-1],[-1],[' num2str(true_arm_index(arm_index(even_stim_ind))) ']'];
    
    num_trials1 = min_num_trials;
    num_trials2 = max_num_trials;
    
    num_Quaddles = max_num_distractors+1;
    
    
    for block = 1:num_blocks
        
        if rem(block,2) == 1
            bdstr2 = ['{"BlockID": "blockid.' [filebase '_B' num2str(block)] '",' ...
                ' "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '],' ...
                ' "ActiveFeatureTemplate": [' active_array '],' ...
                ' "ContextNums": [' num2str(context_nums) '],' ...
                ' "NumRelevantObjectsPerTrial": [' num2str(num_Quaddles) ',' num2str(num_Quaddles) '], "NumIrrelevantObjectsPerTrial": [0,0] ,'...
                ' "NumIrrelevantObjectsPerBlock": 0,...\n' ...
                '"RuleArray": [{"RelevantFeatureTemplate": [' odd_rule_array '], "ContextNums": [' num2str(context_nums) '], ' ...
                '"RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
                '],...' '\n'...
                '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
            
        else
            bdstr2 = ['{"BlockID": "blockid.' [filebase '_B' num2str(block)] '",' ...
                ' "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '],' ...
                ' "ActiveFeatureTemplate": [' active_array '],' ...
                ' "ContextNums": [' num2str(context_nums) '],' ...
                ' "NumRelevantObjectsPerTrial": [' num2str(num_Quaddles) ',' num2str(num_Quaddles) '], "NumIrrelevantObjectsPerTrial": [0,0] ,'...
                ' "NumIrrelevantObjectsPerBlock": 0,...\n' ...
                '"RuleArray": [{"RelevantFeatureTemplate": [' even_rule_array '], "ContextNums": [' num2str(context_nums) '], ' ...
                '"RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
                '],...' '\n'...
                '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        end
        
        if block == num_blocks
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
    end
end
fclose(blockfID);
cd(current_dir)
fclose all;

%---For Patterns---%
file_base_name = 'FLU_DF02_Pattern_Set';
for file = 1:floor(length(pattern_index)/2)
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    odd_stim_ind = 2*(file-1)+1;
    even_stim_ind = 2*(file-1)+2;
    
    active_array = ['[0],[' num2str(pattern_index(odd_stim_ind)) ',' num2str(pattern_index(even_stim_ind)) '],[0],[0],[0]'];
    odd_rule_array = ['[-1],[' num2str(pattern_index(odd_stim_ind)) '],[-1],[-1],[-1]'];
    even_rule_array = ['[-1],[' num2str(pattern_index(even_stim_ind)) '],[-1],[-1],[-1]'];
    
    num_trials1 = min_num_trials;
    num_trials2 = max_num_trials;
    
    num_Quaddles = max_num_distractors+1;
    
    
    for block = 1:num_blocks
        
        if rem(block,2) == 1
            bdstr2 = ['{"BlockID": "blockid.' [filebase '_B' num2str(block)] '",' ...
                ' "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '],' ...
                ' "ActiveFeatureTemplate": [' active_array '],' ...
                ' "ContextNums": [' num2str(context_nums) '],' ...
                ' "NumRelevantObjectsPerTrial": [' num2str(num_Quaddles) ',' num2str(num_Quaddles) '], "NumIrrelevantObjectsPerTrial": [0,0] ,'...
                ' "NumIrrelevantObjectsPerBlock": 0,...\n' ...
                '"RuleArray": [{"RelevantFeatureTemplate": [' odd_rule_array '], "ContextNums": [' num2str(context_nums) '], ' ...
                '"RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
                '],...' '\n'...
                '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
            
        else
            bdstr2 = ['{"BlockID": "blockid.' [filebase '_B' num2str(block)] '",' ...
                ' "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '],' ...
                ' "ActiveFeatureTemplate": [' active_array '],' ...
                ' "ContextNums": [' num2str(context_nums) '],' ...
                ' "NumRelevantObjectsPerTrial": [' num2str(num_Quaddles) ',' num2str(num_Quaddles) '], "NumIrrelevantObjectsPerTrial": [0,0] ,'...
                ' "NumIrrelevantObjectsPerBlock": 0,...\n' ...
                '"RuleArray": [{"RelevantFeatureTemplate": [' even_rule_array '], "ContextNums": [' num2str(context_nums) '], ' ...
                '"RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
                '],...' '\n'...
                '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        end
        
        if block == num_blocks
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
    end
end
fclose(blockfID);
cd(current_dir)
fclose all;