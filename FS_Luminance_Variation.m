clear,clc,close all

rng(20190411); %seed random number generator, do not change EVER &...
%always run file from the start and generate all files


%Example Paths for Dual-Drive Computer
% stim_path = 'D:\\QuaddleRepository_20180515\\';
% itemCND_path = 'D:\\\\TrialFiles\\\\Feature_Search_Training\\\\';

%Example Paths for Kiosk Computers with 1 drive
stim_path = 'C:\\Users\\Womelsdorf Lab\\Documents\\Quaddles\\TexturelessQuaddleRepo\\';
itemCND_path = 'C:\\\\Users\\\\Womelsdorf Lab\\\\Documents\\\\BlockDefinitions\\\\Feature_Search_Training\\\\';


current_dir = pwd;

spatial_scale = 0.05;
RewardMag = 2;
context_nums = -1;

y_default = 0.4;
x_spacing = 2;%also exlucde within 2 units of center, arms make us need more spacing on x-axis
z_spacing = 1.5;%also exlucde within 1.5 units of center
x_max = 8;%magnitude
z_max = 4.5;%magnitude

num_distractor_Quaddles = [3 5 9 12];
fam_num_trials = 10;
min_num_trials = 120;
max_num_trials = 120;

shapes = {'S00'};
patterns = {'P00','P01', 'P02', 'P04', 'P09'};

patternless_colors50 = {'C5000000_5000000', 'C5050014_5050014', 'C5050059_5050059','C5050106_5050106',...
    'C5050148_5050148', 'C5050194_5050194', 'C5050238_5050238', 'C5050287_5050287','C5050335_5050335'};

patternless_colors60 = {'C6000000_6000000', 'C6070014_6070014', 'C6070059_6070059','C6070106_6070106',...
    'C6070148_6070148', 'C6070194_6070194', 'C6070239_6070239', 'C6070287_6070287', 'C6070335_6070335'};

patternless_colors70 = {'C7000000_7000000', 'C7070014_7070014', 'C7070059_7070059', 'C7070106_7070106',...
    'C7070148_7070148', 'C7070194_7070194', 'C7070240_7070240', 'C7070286_7070286', 'C7070335_7070335'};

patterned_colors =  {'C7000000_5000000', 'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000',...
    'C7070148_5000000', 'C7070194_5000000', 'C7070240_5000000', 'C7070286_5000000', 'C7070335_5000000'};

arms = {'A00_E00'};

numcolors=length(patterned_colors)-1;
shuffcolors_index=randperm(numcolors);

numpatterns=length(patterns)-1;
shuffpatterns_index=[randperm(numpatterns),randperm(numpatterns)];

%% Generate all possible positions
xzlocs = [];
count = 0;
for x = -x_max:x_spacing:x_max
    for z = -z_max:z_spacing:z_max
        if x == 0 && z == 0
            continue
        else
            count = count+1;
            xzlocs(1,count) = x;
            xzlocs(2,count) = z;
        end
    end
end


%% Generate ITM and CND Headers which are always the same for all files (items=quaddles, along with location/rewvalue/filepaths etc)

itm_header=['StimCode' '\t' 'StimName' '\t' 'StimPath' '\t' ...
    'StimDimVals1' '\t' 'StimDimVals2' '\t' 'StimDimVals3' '\t' 'StimDimVals4' '\t' 'StimDimVals5' '\t' ...
    'StimLocationX' '\t' 'StimLocationY' '\t' 'StimLocationZ' '\t' ...
    'StimRotationX' '\t' 'StimRotationY' '\t' 'StimRotationZ' '\t' ...
    'StimTrialRewardProb' '\t' 'StimTrialRewardMag' '\t' 'TimesUsedInBlock' ...
    '\t' 'isRelevant' '\t' 'SetLocation' '\t' 'SetRotation' '\n'];

cnd_header=['TrialName' '\t' 'TrialCode' '\t' 'ContextNum' '\t' 'ContextName' '\t' ...
    'Stim1' '\t'  'Stim2' '\t'  'Stim3' '\t'  'Stim4' '\t'  'Stim5' '\t'  'Stim6' '\t' ...
    'Stim7' '\t'  'Stim8' '\t'  'Stim9' '\t'  'Stim10' '\t'  'Stim11' '\t'  'Stim12' '\t' ...
    'Stim13' '\t'  'Stim14' '\n'];

%% Generate 2D sets

number_of_Quaddle_dimensions = 2;
file_base_name = 'Luminance_FS_set';
for file = 1:8
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----% points to item and condition files
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    %---Second Generate the Item and Condition files for each Block----%
    for block = 1:2
        
        if block == 1%for fam block
            num_trials1 = fam_num_trials;
            num_trials2 = 2*fam_num_trials;
        else
            num_trials1 = min_num_trials;
            num_trials2 = max_num_trials;
        end
        
        blockpath = [itemCND_path filebase '\\\\' filebase '_Block' num2str(block)];
        bdstr2 = ['{"BlockID": "blockid.' [filebase '_Block' num2str(block)] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if block == 2
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        %make patterned targets
        target = ['S00_' patterns{shuffpatterns_index(file)+1} '_' patterned_colors{shuffcolors_index(file)+1} '_T00_A00_E00.fbx'];
        
        target_full_path = [Determine_Quaddle_Path(target,stim_path,false) target];
        %make distractors
        distractors = cell(1,3);
        distractor_full_path = cell(1,3);
        distractors{1} = ['S00_P00_' patternless_colors50{shuffcolors_index(file)+1} '_T00_A00_E00.fbx'];
        distractors{2} = ['S00_P00_' patternless_colors60{shuffcolors_index(file)+1} '_T00_A00_E00.fbx'];
        distractors{3} = ['S00_P00_' patternless_colors70{shuffcolors_index(file)+1} '_T00_A00_E00.fbx'];
        for d=1:3
            distractor_full_path{d} = [Determine_Quaddle_Path(distractors{d},stim_path,false) distractors{d}];
        end
        
        
        %-----Generate Item File---%
        fileID = fopen([filebase '_Block' num2str(block) '_Itm.txt'],'w');
        fprintf(fileID,itm_header);
        item_num = 0;
        item_locc_index = NaN(length(distractors)+1,length(xzlocs));
        for Q = 1:length(distractors)+1
            for locs = 1:length(xzlocs)
                item_num = item_num+1;
                item_locc_index(Q,locs) = item_num;
                if any(Q == 1)%target
                    str = [num2str(item_num) '\t' target '\t' target_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '1' '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                else %distractor
                    str = [num2str(item_num) '\t' distractors{Q-1} '\t' distractor_full_path{Q-1} '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '0' '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                end
                fprintf(fileID,str);
            end
        end
        fclose(fileID);
        
        
        %---Generate Distractor Luminance Combos---%
        distrctor_luminance_combos = NaN(3,length(num_distractor_Quaddles));
        for numD = 1:length(num_distractor_Quaddles)
            for num_luminance = 1:3
                if num_luminance == 1
                    distrctor_luminance_combos(num_luminance,numD) = numD;
                elseif num_luminance == 2
                    distrctor_luminance_combos(num_luminance,numD) = 10*numD;
                else
                    distrctor_luminance_combos(num_luminance,numD) = 100*numD;
                end
            end
        end
        distrctor_luminance_combos = distrctor_luminance_combos(:);
        
        all_distrctor_luminance_combos = [];
        for num_iter = 1:ceil(max_num_trials/length(distrctor_luminance_combos))
            all_distrctor_luminance_combos = [all_distrctor_luminance_combos; distrctor_luminance_combos];
        end
        all_distrctor_luminance_combos = all_distrctor_luminance_combos(randperm(length(all_distrctor_luminance_combos)));
        
        
        %----Generate Condition file-----%
        fileID = fopen([filebase '_Block' num2str(block) '_CND.txt'],'w');
        fprintf(fileID,cnd_header);
        
        for t = 1:num_trials2
            
            if block == 1 %familarization block
                str1 = ['TrialNum' num2str(t+1000*block) '\t' num2str(t+1000*block) '\t'];
                str2 = ['ContextNum' num2str(context_nums) '\t'  num2str(context_nums)  '\t'];
                
                item_index = randperm(length(xzlocs));
                str3 = num2str(item_locc_index(1,item_index(1)));
                str4 = [];
            else %test block w/ distractors
                
                %Determine luminance and distractor count
                this_distractor_count = all_distrctor_luminance_combos(t);
                if this_distractor_count < 10
                    this_luminance = 1;
                    %this_distractor_count = this_distractor_count;
                elseif this_distractor_count < 100
                    this_luminance = 2;
                    this_distractor_count = this_distractor_count/10;
                else
                    this_luminance = 3;
                    this_distractor_count = this_distractor_count/100;
                end
                
                str1 = ['TrialNum_' num2str(t) '_' num2str(all_distrctor_luminance_combos(t))  '\t' num2str(t+1000*block) '\t'];
                str2 = ['ContextNum' num2str(context_nums) '\t'  num2str(context_nums)  '\t'];
                
                item_index = randperm(length(xzlocs));
                distractor_index = item_locc_index(this_luminance+1,item_index);
                
                str3 = [num2str(item_locc_index(1,item_index(1))) '\t'];
                distractor_index(1) = [];%first item is target location
                str4 = [];
                for stim = 1:num_distractor_Quaddles(this_distractor_count)
                    if stim == num_distractor_Quaddles(this_distractor_count)
                        str4= [str4  num2str(distractor_index(stim))];
                    else
                        str4= [str4  num2str(distractor_index(stim)) '\t'];
                    end
                end
            end
            fprintf(fileID,[str1 str2 str3  str4 '\n']);
        end
        
        fclose(fileID);
    end
    fclose(blockfID);
end
cd(current_dir);
fclose all