%Scripts generate ITM and CND files for Feature Search Training Steps for
%the monkeys

clear,clc,close all
rng(20180906); %seed random number generator

%Example Paths for Dual-Drive Computer
%%stim_path = 'D:\\MonkeyTrainingStim\\TrainingQuaddles\\';
%%itemCND_path = 'D:\\\\TrialFiles\\\\Feature_Search_Training\\\\';

%Example Paths for Kiosk Computers with 1 drive
stim_path = 'C:\\Users\\Womelsdorf Lab\\Documents\\Quaddles\\MonkeyTrainingStim\\TrainingQuaddles\\';
itemCND_path = 'C:\\\\Users\\\\Womelsdorf Lab\\\\Documents\\\\BlockDefinitions\\\\Feature_Search_Training\\\\';

current_dir = pwd;

spatial_scale = 0.05;
RewardMag = 3;
context_nums = -1;

y_default = 0.4;
x_spacing = 2;%also exlucde within 2 units of center, arms make us need more spacing on x-axis
z_spacing = 1.5;%also exlucde within 1.5 units of center
x_max = 8;%magnitude
z_max = 4.5;%magnitude

num_distractor_Quaddles = [0 1 3 5 9 12];
fam_num_trials = 10;
min_num_trials = 40;
max_num_trials = 100;

%% Special Unique colors for first training steps

Quaddle_prefix = 'S00_P00_';
Quaddle_suffix = '_T00_A00_E00.fbx';

Neutral_color = {'C2000000_2000000','C3000000_3000000','C4000000_4000000','C8000000_8000000'};

colors = cell(1,4);
colors{1} = {'C2020010_2020010','C2020120_2020120','C2020213_2020213','C2020270_2020270'};
colors{2} = {'C3020010_3020010','C3020120_3020120','C3020214_3020214','C3020270_3020270'};
colors{3} = {'C4040010_4040010','C4040120_4040120','C4040216_4040216','C4040270_4040270'};
colors{4} = {'C8080009_8080009','C8080132_8080132','C8080221_8080221','C8080276_8080276'};


%% Generate all possible positions
xzlocs = [];
count = 0;
for x = -x_max:x_spacing:x_max
    for z = -z_max:z_spacing:z_max
        if x == 0 && z == 0
            continue
        else
            count = count+1;
            xzlocs(1,count) = x;
            xzlocs(2,count) = z;
        end
    end
end

%% Generate ITM and CND Headers which are always the same for all files

itm_header=['StimCode' '\t' 'StimName' '\t' 'StimPath' '\t' ...
    'StimDimVals1' '\t' 'StimDimVals2' '\t' 'StimDimVals3' '\t' 'StimDimVals4' '\t' 'StimDimVals5' '\t' ...
    'StimLocationX' '\t' 'StimLocationY' '\t' 'StimLocationZ' '\t' ...
    'StimRotationX' '\t' 'StimRotationY' '\t' 'StimRotationZ' '\t' ...
    'StimTrialRewardProb' '\t' 'StimTrialRewardMag' '\t' 'TimesUsedInBlock' ...
    '\t' 'isRelevant' '\t' 'SetLocation' '\t' 'SetRotation' '\n'];

cnd_header=['TrialName' '\t' 'TrialCode' '\t' 'ContextNum' '\t' 'ContextName' '\t' ...
    'Stim1' '\t'  'Stim2' '\t'  'Stim3' '\t'  'Stim4' '\t'  'Stim5' '\t'  'Stim6' '\t' ...
    'Stim7' '\t'  'Stim8' '\t'  'Stim9' '\t'  'Stim10' '\t'  'Stim11' '\t'  'Stim12' '\t' ...
    'Stim13' '\t'  'Stim14' '\n'];

%% Make BlockDef and Item/CND for easiset level, 1 bright color vs 1 dark gray quaddle

file_base_name = 'FS_DF02_Easy_Set';%D is for the # of distractors
for file = 1:length(colors{4})
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    %---Second Generate the Item and Condition files for each Block----%
    for numD = 1:length(num_distractor_Quaddles)
        
        if numD == 1%for fam block
            num_trials1 = fam_num_trials;
            num_trials2 = min_num_trials;
        elseif numD  == length(num_distractor_Quaddles)%for last block do lots of trials so won't finish block before stop working
            num_trials1 = 4*max_num_trials;
            num_trials2 = 4*max_num_trials;
        else
            num_trials1 = min_num_trials;
            num_trials2 = max_num_trials;
        end
        
        blockpath = [itemCND_path filebase '\\\\' filebase '_D' num2str(num_distractor_Quaddles(numD))];
        bdstr2 = ['{"BlockID": "blockid.' [filebase '_D' num2str(num_distractor_Quaddles(numD))] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0], "NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if numD == length(num_distractor_Quaddles)
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        target = [Quaddle_prefix colors{4}{file} Quaddle_suffix];
        target_full_path = [stim_path target];
        distractor = [Quaddle_prefix Neutral_color{1} Quaddle_suffix];
        distractor_full_path = [stim_path distractor];
        
        %-----Generate Item File---%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_Itm.txt'],'w');
        fprintf(fileID,itm_header);
        item_num = 0;
        item_locc_index = NaN(2,length(xzlocs));
        for Q = 1:2
            for locs = 1:length(xzlocs)
                item_num = item_num+1;
                item_locc_index(Q,locs) = item_num;
                if any(Q == 1)%target
                    str = [num2str(item_num) '\t' target '\t' target_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '1' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                else %distractor
                    str = [num2str(item_num) '\t' distractor '\t' distractor_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '0' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                end
                %%
                fprintf(fileID,str);
            end
        end
        fclose(fileID);
        
        %----Generate Condition file-----%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_CND.txt'],'w');
        fprintf(fileID,cnd_header);
        
        for t = 1:num_trials2
            
            item_index = randperm(length(xzlocs));
            
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(context_nums) '\t'  num2str(context_nums)  '\t'];
            
            if num_distractor_Quaddles(numD) == 0
                str3 = [num2str(item_locc_index(1,item_index(1)))];
            else
                str3 = [num2str(item_locc_index(1,item_index(1))) '\t'];
            end
            
            str4 = [];
            for stim = 1:num_distractor_Quaddles(numD)
                if stim == num_distractor_Quaddles(numD)
                    str4= [str4  num2str(item_locc_index(2,item_index(stim+1)))];
                else
                    str4= [str4  num2str(item_locc_index(2,item_index(stim+1))) '\t'];
                end
            end
            fprintf(fileID,[str1 str2 str3  str4 '\n']);
        end
        
        fclose(fileID);
    end
    fclose(blockfID);
end
%% Make BlockDef and Item/CND for easiset level, 1 bright color vs 1 darkish gray quaddle

file_base_name = 'FS_DF02_Medium1_Set';%D is for the # of distractors
for file = 1:length(colors{4})
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    %---Second Generate the Item and Condition files for each Block----%
    for numD = 1:length(num_distractor_Quaddles)
        
        if numD == 1%for fam block
            num_trials1 = fam_num_trials;
            num_trials2 = min_num_trials;
        elseif numD  == length(num_distractor_Quaddles)%for last block do lots of trials so won't finish block before stop working
            num_trials1 = 4*max_num_trials;
            num_trials2 = 4*max_num_trials;
        else
            num_trials1 = min_num_trials;
            num_trials2 = max_num_trials;
        end
        
        blockpath = [itemCND_path filebase '\\\\' filebase '_D' num2str(num_distractor_Quaddles(numD))];
        bdstr2 = ['{"BlockID": "blockid.' [filebase '_D' num2str(num_distractor_Quaddles(numD))] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if numD == length(num_distractor_Quaddles)
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        target = [Quaddle_prefix colors{4}{file} Quaddle_suffix];
        target_full_path = [stim_path target];
        distractor = [Quaddle_prefix Neutral_color{3} Quaddle_suffix];
        distractor_full_path = [stim_path distractor];
        
        %-----Generate Item File---%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_Itm.txt'],'w');
        fprintf(fileID,itm_header);
        item_num = 0;
        item_locc_index = NaN(2,length(xzlocs));
        for Q = 1:2
            for locs = 1:length(xzlocs)
                item_num = item_num+1;
                item_locc_index(Q,locs) = item_num;
                if any(Q == 1)%target
                    str = [num2str(item_num) '\t' target '\t' target_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '1' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                else %distractor
                    str = [num2str(item_num) '\t' distractor '\t' distractor_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '0' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                end
                fprintf(fileID,str);
            end
        end
        fclose(fileID);
        
        %----Generate Condition file-----%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_CND.txt'],'w');
        fprintf(fileID,cnd_header);
        
        for t = 1:num_trials2
            
            item_index = randperm(length(xzlocs));
            
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(context_nums) '\t'  num2str(context_nums)  '\t'];
            
            if num_distractor_Quaddles(numD) == 0
                str3 = [num2str(item_locc_index(1,item_index(1)))];
            else
                str3 = [num2str(item_locc_index(1,item_index(1))) '\t'];
            end
            
            str4 = [];
            for stim = 1:num_distractor_Quaddles(numD)
                if stim == num_distractor_Quaddles(numD)
                    str4= [str4  num2str(item_locc_index(2,item_index(stim+1)))];
                else
                    str4= [str4  num2str(item_locc_index(2,item_index(stim+1))) '\t'];
                end
            end
            fprintf(fileID,[str1 str2 str3  str4 '\n']);
        end
        
        fclose(fileID);
    end
    fclose(blockfID);
end

% Make BlockDef and Item/CND for medium level2, 1 bright color vs 1 dark color quaddles

color_lumens_row = [1 1 2 2 3 3];%increase luminance with # of distractors
file_base_name = 'FS_DF02_Medium2_Set';%D is for the # of distractors
for file = 1:length(colors{4})
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    %---Second Generate the Item and Condition files for each Block----%
    for numD = 1:length(num_distractor_Quaddles)
        
        if numD == 1%for fam block
            num_trials1 = fam_num_trials;
            num_trials2 = min_num_trials;
        elseif numD  == length(num_distractor_Quaddles)%for last block do lots of trials so won't finish block before stop working
            num_trials1 = 4*max_num_trials;
            num_trials2 = 4*max_num_trials;
        else
            num_trials1 = min_num_trials;
            num_trials2 = max_num_trials;
        end
        
        blockpath = [itemCND_path filebase '\\\\' filebase '_D' num2str(num_distractor_Quaddles(numD))];
        bdstr2 = ['{"BlockID": "blockid.' [filebase '_D' num2str(num_distractor_Quaddles(numD))] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if numD == length(num_distractor_Quaddles)
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        target = [Quaddle_prefix colors{4}{file} Quaddle_suffix];
        target_full_path = [stim_path target];
        distractors = {};
        distractor_full_path = {};
        distractor_colors = {};
        for d = 1:length(colors{color_lumens_row(numD)})+1
            if d == length(colors{color_lumens_row(numD)})+1
                distractors{d} = [Quaddle_prefix Neutral_color{color_lumens_row(numD)} Quaddle_suffix];
                distractor_colors{d} = Neutral_color{color_lumens_row(numD)};
            else
                distractors{d} = [Quaddle_prefix colors{color_lumens_row(numD)}{d} Quaddle_suffix];
                distractor_colors{d} = colors{color_lumens_row(numD)}{d};
            end
            distractor_full_path{d} = [stim_path distractors{d}];
        end
        
        
        %-----Generate Item File---%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_Itm.txt'],'w');
        fprintf(fileID,itm_header);
        item_num = 0;
        item_locc_index = NaN(2,length(xzlocs));
        for Q = 1:length(distractors)+1
            for locs = 1:length(xzlocs)
                item_num = item_num+1;
                item_locc_index(Q,locs) = item_num;
                if any(Q == 1)%target
                    str = [num2str(item_num) '\t' target '\t' target_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '1' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                else %distractor
                    str = [num2str(item_num) '\t' distractors{Q-1} '\t' distractor_full_path{Q-1} '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '0' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                end
                fprintf(fileID,str);
            end
        end
        fclose(fileID);
        
        %----Generate Condition file-----%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_CND.txt'],'w');
        fprintf(fileID,cnd_header);
        
        for t = 1:num_trials2
            
            item_index = randperm(length(xzlocs));
            distractor_index = [1:length(distractors) 1:length(distractors) 1:length(distractors)];
            distractor_index = distractor_index(randperm(length(distractor_index)));
            
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(context_nums) '\t'  num2str(context_nums)  '\t'];
            
            if num_distractor_Quaddles(numD) == 0
                str3 = [num2str(item_locc_index(1,item_index(1)))];
            else
                str3 = [num2str(item_locc_index(1,item_index(1))) '\t'];
            end
            
            str4 = [];
            for stim = 1:num_distractor_Quaddles(numD)
                if stim == num_distractor_Quaddles(numD)
                    str4= [str4  num2str(item_locc_index(1+distractor_index(stim),item_index(stim+1)))];
                else
                    str4= [str4  num2str(item_locc_index(1+distractor_index(stim),item_index(stim+1))) '\t'];
                end
            end
            fprintf(fileID,[str1 str2 str3  str4 '\n']);
        end
        
        fclose(fileID);
    end
    fclose(blockfID);
end
%% Make BlockDef and Item/CND for hard level, 1 bright color vs 1 bright color quaddles

color_lumens_row = 4;%increase luminance with # of distractors
file_base_name = 'FS_DF02_Hard_Set';%D is for the # of distractors
for file = 1:length(colors{4})
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    %---Second Generate the Item and Condition files for each Block----%
    for numD = 1:length(num_distractor_Quaddles)
        
        if numD == 1%for fam block
            num_trials1 = fam_num_trials;
            num_trials2 = min_num_trials;
        elseif numD  == length(num_distractor_Quaddles)%for last block do lots of trials so won't finish block before stop working
            num_trials1 = 4*max_num_trials;
            num_trials2 = 4*max_num_trials;
        else
            num_trials1 = min_num_trials;
            num_trials2 = max_num_trials;
        end
        
        blockpath = [itemCND_path filebase '\\\\' filebase '_D' num2str(num_distractor_Quaddles(numD))];
        bdstr2 = ['{"BlockID": "blockid.' [filebase '_D' num2str(num_distractor_Quaddles(numD))] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if numD == length(num_distractor_Quaddles)
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        target = [Quaddle_prefix colors{4}{file} Quaddle_suffix];
        target_full_path = [stim_path target];
        distractors = {};
        distractor_colors = {};
        distractor_full_path = {};
        count = 1;
        for d = 1:length(colors{color_lumens_row})+1
            if d == file
                continue
            end
            if d == length(colors{color_lumens_row})+1
                distractors{count} = [Quaddle_prefix Neutral_color{color_lumens_row} Quaddle_suffix];
                distractor_colors{count} = Neutral_color{color_lumens_row};
            else
                distractors{count} = [Quaddle_prefix colors{color_lumens_row}{d} Quaddle_suffix];
                distractor_colors{count} = colors{color_lumens_row}{d};
            end
            distractor_full_path{count} = [stim_path distractors{count}];
            count = count+1;
        end
        
        
        %-----Generate Item File---%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_Itm.txt'],'w');
        fprintf(fileID,itm_header);
        item_num = 0;
        item_locc_index = NaN(2,length(xzlocs));
        for Q = 1:length(distractors)+1
            for locs = 1:length(xzlocs)
                item_num = item_num+1;
                item_locc_index(Q,locs) = item_num;
                if any(Q == 1)%target
                    str = [num2str(item_num) '\t' target '\t' target_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '1' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                else %distractor
                    str = [num2str(item_num) '\t' distractors{Q-1} '\t' distractor_full_path{Q-1} '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '0' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                end
                fprintf(fileID,str);
            end
        end
        fclose(fileID);
        
        %----Generate Condition file-----%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_CND.txt'],'w');
        fprintf(fileID,cnd_header);
        
        for t = 1:num_trials2
            
            item_index = randperm(length(xzlocs));
            distractor_index = [1:length(distractors) 1:length(distractors) 1:length(distractors)];
            distractor_index = distractor_index(randperm(length(distractor_index)));
            
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(context_nums) '\t'  num2str(context_nums)  '\t'];
            
            if num_distractor_Quaddles(numD) == 0
                str3 = [num2str(item_locc_index(1,item_index(1)))];
            else
                str3 = [num2str(item_locc_index(1,item_index(1))) '\t'];
            end
            
            str4 = [];
            for stim = 1:num_distractor_Quaddles(numD)
                if stim == num_distractor_Quaddles(numD)
                    str4= [str4  num2str(item_locc_index(1+distractor_index(stim),item_index(stim+1)))];
                else
                    str4= [str4  num2str(item_locc_index(1+distractor_index(stim),item_index(stim+1))) '\t'];
                end
            end
            fprintf(fileID,[str1 str2 str3  str4 '\n']);
        end
        
        fclose(fileID);
    end
    fclose(blockfID);
end
fclose all;
cd(current_dir)