clear,clc,close all

rng(20181025); %seed random number generator, do not change EVER &...
%always run file from the start and generate all files

current_dir = pwd;

RewardMag = 3;
context_nums = -1;
max_num_distractors = 2;%number of distractors

num_blocks = 6;%50 trials per feature value: 10 fam + 10 test
fam_num_trials = 10;
min_num_trials = 40;
max_num_trials = 40;

shapes = {'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
arms = {'A01_E00', 'A02_E00', 'A00_E01', 'A00_E02', 'A00_E03'};
patterns = {'P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

patternless_colors = {'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070287_6070287', 'C6070335_6070335'};
%patterned_colors =   {'C7000000_5000000', 'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000', 'C7070286_5000000', 'C7070335_5000000'};
gray_pattern_color = 'C7000000_5000000';

%used for indexing in Unity's array
full_arms_array = {'A00_E00', 'A00_E01', 'A00_E02','A00_E03', 'A01_E00', 'A01_E01', 'A01_E02', 'A01_E03', 'A02_E00', 'A02_E01', 'A02_E02', 'A02_E03'};      

true_arm_index = NaN(1,length(arms));
for a = 1:length(arms)
    true_arm_index(a) = find(contains(full_arms_array,arms{a}))-1;%reindex since Unity starts at 0
end
%%
%randomize...so that...
shape_index = randperm(length(shapes));
arm_index = randperm(length(arms));
pattern_index = randperm(length(patterns));
color_index = randperm(length(patternless_colors));

num_features = length(shapes) + length(arms) + length(patterns) + length(patternless_colors)-1;

%randomize...so that...
dims = [shape_index pattern_index+100 color_index+200 arm_index+300];

while any(abs(diff(diff(dims))) < 10) % so no more than 2 of any dim are in a row
    dims = dims(randperm(length(dims)));
end
%%
file_base_name = 'FLU_DF01_Set';%D is for the diffculty mode
for file = 1:5
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    
    for block = 1:num_blocks
        %---Second Generate the Item and Condition files for each Block----%
        stim_index = (file-1)*num_blocks+block;
        
        if dims(stim_index) < 100
            target = dims(stim_index);
            all_quaddles = length(shapes);%includes target and distractors
            
            active_template_array2 = [];
            for d = 1:all_quaddles
                if d == 1
                    active_template_array2 = [active_template_array2 '[' num2str(d) ];
                elseif d == all_quaddles
                    active_template_array2 = [active_template_array2 ',' num2str(d) ']'];
                else
                    active_template_array2 = [active_template_array2 ',' num2str(d)];
                end
            end
            active_template_array1 = ['[' num2str(target) '],[-1],[-1],[-1],[-1]'];
            active_template_array2 = [active_template_array2 ',[0],[0],[0],[0]'];
            
        elseif dims(stim_index) > 100 && dims(stim_index) < 200
            target = dims(stim_index)-100;
            all_quaddles = length(patterns);%includes target and distractors
                        
            active_template_array2 = [];
            for d = 1:all_quaddles
                if d == 1
                    active_template_array2 = [active_template_array2 '[' num2str(d) ];
                elseif d == all_quaddles
                    active_template_array2 = [active_template_array2 ',' num2str(d) ']'];
                else
                    active_template_array2 = [active_template_array2 ',' num2str(d)];
                end
            end
            active_template_array1 = ['[-1],[' num2str(target) '],[-1],[-1],[-1]'];
            active_template_array2 = ['[0],' active_template_array2 ',[0],[0],[0]'];
            
        elseif dims(stim_index) > 200 && dims(stim_index) < 300
            target = dims(stim_index)-200;
            all_quaddles = length(patternless_colors); %includes target and distractors

            active_template_array2 = [];
            for d = 1:all_quaddles
                if d == 1
                    active_template_array2 = [active_template_array2 '[' num2str(d) ];
                elseif d == all_quaddles
                    active_template_array2 = [active_template_array2 ',' num2str(d) ']'];
                else
                    active_template_array2 = [active_template_array2 ',' num2str(d)];
                end
            end
            active_template_array1 = ['[-1],[-1],[' num2str(target) '],[-1],[-1]'];
            active_template_array2 = ['[0],[0],' active_template_array2 ',[0],[0]'];
            
        elseif dims(stim_index) > 300 && dims(stim_index) < 400
            %arms are organized a little differently
            target = true_arm_index(dims(stim_index)-300);
            all_quaddles = true_arm_index; %includes target and distractors

            active_template_array2 = [];
            for d = 1:length(all_quaddles)
                if d == 1
                    active_template_array2 = [active_template_array2 '[' num2str(all_quaddles(d)) ];
                elseif d == length(all_quaddles)
                    active_template_array2 = [active_template_array2 ',' num2str(all_quaddles(d)) ']'];
                else
                    active_template_array2 = [active_template_array2 ',' num2str(all_quaddles(d))];
                end
            end
            active_template_array1 = ['[-1],[-1],[-1],[-1],[' num2str(target) ']'];
            active_template_array2 = ['[0],[0],[0],[0],' active_template_array2];
        else
            disp('Dimension Not recognized')
        end
        
        for numD = 1:2
            
            if numD == 1%for fam block
                num_trials1 = fam_num_trials;
                num_trials2 = fam_num_trials;
                num_Quaddles = 1;
            else
                num_trials1 = min_num_trials;
                num_trials2 = max_num_trials;
                num_Quaddles = max_num_distractors+1;
            end
            
            bdstr2 = ['{"BlockID": "blockid.' [filebase '_D' num2str(num_Quaddles)  '_B' num2str(block)] '",' ...
                ' "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '],' ...
                ' "ActiveFeatureTemplate": [' active_template_array2 '],' ...
                ' "ContextNums": [' num2str(context_nums) '],' ...
                ' "NumRelevantObjectsPerTrial": [' num2str(num_Quaddles) ',' num2str(num_Quaddles) '], "NumIrrelevantObjectsPerTrial": [0,0] ,'...
                ' "NumIrrelevantObjectsPerBlock": 0,...\n' ...
                '"RuleArray": [{"RelevantFeatureTemplate": [' active_template_array1 '], "ContextNums": [' num2str(context_nums) '], ' ...
                '"RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
                '],...' '\n'...
                '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
            
            if block == num_blocks && numD == 2
                bdstr3 =  ']\n//end of blockDef[]';
            else
                bdstr3 = '\n\n';
            end
            fprintf(blockfID,bdstr2);
            fprintf(blockfID,bdstr3);
        end
    end
    fclose(blockfID);
end
cd(current_dir)
fclose all;