clear,clc,close all

rng(20180907); %seed random number generator, do not change EVER &...
%always run file from the start and generate all files

%Example Paths for Dual-Drive Computer
%%stim_path = 'D:\\QuaddleRepository_20180515\\';
%%itemCND_path = 'D:\\\\TrialFiles\\\\Feature_Search_Training\\\\';

%Example Paths for Kiosk Computers with 1 drive
stim_path = 'C:\\Users\\Womelsdorf Lab\\Documents\\Quaddles\\TexturelessQuaddleRepo\\';
itemCND_path = 'C:\\\\Users\\\\Womelsdorf Lab\\\\Documents\\\\BlockDefinitions\\\\Feature_Search_Training\\\\';

current_dir = pwd;

spatial_scale = 0.05;
RewardMag = 3;
context_nums = -1;

y_default = 0.4;
x_spacing = 2;%also exlucde within 2 units of center, arms make us need more spacing on x-axis
z_spacing = 1.5;%also exlucde within 1.5 units of center
x_max = 8;%magnitude
z_max = 4.5;%magnitude

num_distractor_Quaddles = [0 1 3 5 9 12];
fam_num_trials = 10;
min_num_trials = 40;
max_num_trials = 100;

shapes = {'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
arms = {'A01_E00', 'A02_E00', 'A00_E01', 'A00_E02', 'A00_E03'};
patterns = {'P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

patternless_colors = {'C6000000_6000000', 'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070287_6070287', 'C6070335_6070335'};
%patterned_colors =   {'C7000000_5000000', 'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000', 'C7070286_5000000', 'C7070335_5000000'};
gray_pattern_color = 'C7000000_5000000';

%randomize...so that...
shape_index = randperm(length(shapes));
arm_index = randperm(length(arms));
pattern_index = randperm(length(patterns));
color_index = randperm(length(patternless_colors));
color_index(color_index == 1) = [];%first color is gray

%% Generate all possible positions
xzlocs = [];
count = 0;
for x = -x_max:x_spacing:x_max
    for z = -z_max:z_spacing:z_max
        if x == 0 && z == 0
            continue
        else
            count = count+1;
            xzlocs(1,count) = x;
            xzlocs(2,count) = z;
        end
    end
end

%% Generate ITM and CND Headers which are always the same for all files

itm_header=['StimCode' '\t' 'StimName' '\t' 'StimPath' '\t' ...
    'StimDimVals1' '\t' 'StimDimVals2' '\t' 'StimDimVals3' '\t' 'StimDimVals4' '\t' 'StimDimVals5' '\t' ...
    'StimLocationX' '\t' 'StimLocationY' '\t' 'StimLocationZ' '\t' ...
    'StimRotationX' '\t' 'StimRotationY' '\t' 'StimRotationZ' '\t' ...
    'StimTrialRewardProb' '\t' 'StimTrialRewardMag' '\t' 'TimesUsedInBlock' ...
    '\t' 'isRelevant' '\t' 'SetLocation' '\t' 'SetRotation' '\n'];

cnd_header=['TrialName' '\t' 'TrialCode' '\t' 'ContextNum' '\t' 'ContextName' '\t' ...
    'Stim1' '\t'  'Stim2' '\t'  'Stim3' '\t'  'Stim4' '\t'  'Stim5' '\t'  'Stim6' '\t' ...
    'Stim7' '\t'  'Stim8' '\t'  'Stim9' '\t'  'Stim10' '\t'  'Stim11' '\t'  'Stim12' '\t' ...
    'Stim13' '\t'  'Stim14' '\n'];
%% Generate 3 sets for the first 3 feature values in each feature dimension

%---For Shapes---%
file_base_name = 'FS_DF03_Shape_Set';%D is for the diffculty mode
for file = 1:3
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    %---Second Generate the Item and Condition files for each Block----%
    for numD = 1:length(num_distractor_Quaddles)
        
        if numD == 1%for fam block
            num_trials1 = fam_num_trials;
            num_trials2 = min_num_trials;
        elseif numD  == length(num_distractor_Quaddles)%for last block do lots of trials so won't finish block before stop working
            num_trials1 = 4*max_num_trials;
            num_trials2 = 4*max_num_trials;
        else
            num_trials1 = min_num_trials;
            num_trials2 = max_num_trials;
        end
        
        blockpath = [itemCND_path filebase '\\\\' filebase '_D' num2str(num_distractor_Quaddles(numD))];
        bdstr2 = ['{"BlockID": "blockid.' [filebase '_D' num2str(num_distractor_Quaddles(numD))] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if numD == length(num_distractor_Quaddles)
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        target = [shapes{shape_index(file)} '_P00_' patternless_colors{1} '_T00_A00_E00.fbx'];
        target_full_path = [Determine_Quaddle_Path(target,stim_path,false) target]; 
        distractor = ['S00_P00_' patternless_colors{1} '_T00_A00_E00.fbx'];
        distractor_full_path = [Determine_Quaddle_Path(distractor,stim_path,false) distractor]; 
        
        %-----Generate Item File---%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_Itm.txt'],'w');
        fprintf(fileID,itm_header);
        item_num = 0;
        item_locc_index = NaN(2,length(xzlocs));
        for Q = 1:2
            for locs = 1:length(xzlocs)
                item_num = item_num+1;
                item_locc_index(Q,locs) = item_num;
                if any(Q == 1)%target
                    str = [num2str(item_num) '\t' target '\t' target_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '1' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                else %distractor
                    str = [num2str(item_num) '\t' distractor '\t' distractor_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '0' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                end
                fprintf(fileID,str);
            end
        end
        fclose(fileID);
        
        %----Generate Condition file-----%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_CND.txt'],'w');
        fprintf(fileID,cnd_header);
        
        for t = 1:num_trials2
            
            item_index = randperm(length(xzlocs));
            
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(context_nums) '\t'  num2str(context_nums)  '\t'];
            
            if num_distractor_Quaddles(numD) == 0
                str3 = [num2str(item_locc_index(1,item_index(1)))];
            else
                str3 = [num2str(item_locc_index(1,item_index(1))) '\t'];
            end
            
            str4 = [];
            for stim = 1:num_distractor_Quaddles(numD)
                if stim == num_distractor_Quaddles(numD)
                    str4= [str4  num2str(item_locc_index(2,item_index(stim+1)))];
                else
                    str4= [str4  num2str(item_locc_index(2,item_index(stim+1))) '\t'];
                end
            end
            fprintf(fileID,[str1 str2 str3  str4 '\n']);
        end
        
        fclose(fileID);
    end
    fclose(blockfID);
end

%---For Arms---%
file_base_name = 'FS_DF03_Arms_Set';%D is for the diffculty mode
for file = 1:3
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    %---Second Generate the Item and Condition files for each Block----%
    for numD = 1:length(num_distractor_Quaddles)
        
        if numD == 1%for fam block
            num_trials1 = fam_num_trials;
            num_trials2 = min_num_trials;
        elseif numD  == length(num_distractor_Quaddles)%for last block do lots of trials so won't finish block before stop working
            num_trials1 = 4*max_num_trials;
            num_trials2 = 4*max_num_trials;
        else
            num_trials1 = min_num_trials;
            num_trials2 = max_num_trials;
        end
        
        blockpath = [itemCND_path filebase '\\\\' filebase '_D' num2str(num_distractor_Quaddles(numD))];
        bdstr2 = ['{"BlockID": "blockid.' [filebase '_D' num2str(num_distractor_Quaddles(numD))] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if numD == length(num_distractor_Quaddles)
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        target = ['S00_P00_' patternless_colors{1} '_T00_' arms{arm_index(file)} '.fbx'];
        target_full_path = [Determine_Quaddle_Path(target,stim_path,false) target]; 
        distractor = ['S00_P00_' patternless_colors{1} '_T00_A00_E00.fbx'];
        distractor_full_path = [Determine_Quaddle_Path(distractor,stim_path,false) distractor]; 
        
        %-----Generate Item File---%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_Itm.txt'],'w');
        fprintf(fileID,itm_header);
        item_num = 0;
        item_locc_index = NaN(2,length(xzlocs));
        for Q = 1:2
            for locs = 1:length(xzlocs)
                item_num = item_num+1;
                item_locc_index(Q,locs) = item_num;
                if any(Q == 1)%target
                    str = [num2str(item_num) '\t' target '\t' target_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '1' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                else %distractor
                    str = [num2str(item_num) '\t' distractor '\t' distractor_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '0' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                end
                fprintf(fileID,str);
            end
        end
        fclose(fileID);
        
        %----Generate Condition file-----%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_CND.txt'],'w');
        fprintf(fileID,cnd_header);
        
        for t = 1:num_trials2
            
            item_index = randperm(length(xzlocs));
            
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(context_nums) '\t'  num2str(context_nums)  '\t'];
            
            if num_distractor_Quaddles(numD) == 0
                str3 = [num2str(item_locc_index(1,item_index(1)))];
            else
                str3 = [num2str(item_locc_index(1,item_index(1))) '\t'];
            end
            
            str4 = [];
            for stim = 1:num_distractor_Quaddles(numD)
                if stim == num_distractor_Quaddles(numD)
                    str4= [str4  num2str(item_locc_index(2,item_index(stim+1)))];
                else
                    str4= [str4  num2str(item_locc_index(2,item_index(stim+1))) '\t'];
                end
            end
            fprintf(fileID,[str1 str2 str3  str4 '\n']);
        end
        
        fclose(fileID);
    end
    fclose(blockfID);
end

%---For Patterns---%
file_base_name = 'FS_DF03_Patterns_Set';%D is for the diffculty mode
for file = 1:3
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    %---Second Generate the Item and Condition files for each Block----%
    for numD = 1:length(num_distractor_Quaddles)
        
        if numD == 1%for fam block
            num_trials1 = fam_num_trials;
            num_trials2 = min_num_trials;
        elseif numD  == length(num_distractor_Quaddles)%for last block do lots of trials so won't finish block before stop working
            num_trials1 = 4*max_num_trials;
            num_trials2 = 4*max_num_trials;
        else
            num_trials1 = min_num_trials;
            num_trials2 = max_num_trials;
        end
        
        blockpath = [itemCND_path filebase '\\\\' filebase '_D' num2str(num_distractor_Quaddles(numD))];
        bdstr2 = ['{"BlockID": "blockid.' [filebase '_D' num2str(num_distractor_Quaddles(numD))] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if numD == length(num_distractor_Quaddles)
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        target = ['S00_' patterns{pattern_index(file)} '_' gray_pattern_color '_T00_A00_E00.fbx'];
        target_full_path = [Determine_Quaddle_Path(target,stim_path,false) target]; 
        distractor = ['S00_P00_' patternless_colors{1} '_T00_A00_E00.fbx'];
        distractor_full_path = [Determine_Quaddle_Path(distractor,stim_path,false) distractor]; 
        
        %-----Generate Item File---%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_Itm.txt'],'w');
        fprintf(fileID,itm_header);
        item_num = 0;
        item_locc_index = NaN(2,length(xzlocs));
        for Q = 1:2
            for locs = 1:length(xzlocs)
                item_num = item_num+1;
                item_locc_index(Q,locs) = item_num;
                if any(Q == 1)%target
                    str = [num2str(item_num) '\t' target '\t' target_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '1' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                else %distractor
                    str = [num2str(item_num) '\t' distractor '\t' distractor_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '0' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                end
                fprintf(fileID,str);
            end
        end
        fclose(fileID);
        
        %----Generate Condition file-----%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_CND.txt'],'w');
        fprintf(fileID,cnd_header);
        
        for t = 1:num_trials2
            
            item_index = randperm(length(xzlocs));
            
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(context_nums) '\t'  num2str(context_nums)  '\t'];
            
            if num_distractor_Quaddles(numD) == 0
                str3 = [num2str(item_locc_index(1,item_index(1)))];
            else
                str3 = [num2str(item_locc_index(1,item_index(1))) '\t'];
            end
            
            str4 = [];
            for stim = 1:num_distractor_Quaddles(numD)
                if stim == num_distractor_Quaddles(numD)
                    str4= [str4  num2str(item_locc_index(2,item_index(stim+1)))];
                else
                    str4= [str4  num2str(item_locc_index(2,item_index(stim+1))) '\t'];
                end
            end
            fprintf(fileID,[str1 str2 str3  str4 '\n']);
        end
        
        fclose(fileID);
    end
    fclose(blockfID);
end


%---For Colors---%
file_base_name = 'FS_DF03_Colors_Set';%D is for the diffculty mode
for file = 1:3
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    %---Second Generate the Item and Condition files for each Block----%
    for numD = 1:length(num_distractor_Quaddles)
        
        if numD == 1%for fam block
            num_trials1 = fam_num_trials;
            num_trials2 = min_num_trials;
        elseif numD  == length(num_distractor_Quaddles)%for last block do lots of trials so won't finish block before stop working
            num_trials1 = 4*max_num_trials;
            num_trials2 = 4*max_num_trials;
        else
            num_trials1 = min_num_trials;
            num_trials2 = max_num_trials;
        end
        
        blockpath = [itemCND_path filebase '\\\\' filebase '_D' num2str(num_distractor_Quaddles(numD))];
        bdstr2 = ['{"BlockID": "blockid.' [filebase '_D' num2str(num_distractor_Quaddles(numD))] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if numD == length(num_distractor_Quaddles)
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        target = ['S00_P00_' patternless_colors{color_index(file)} '_T00_A00_E00.fbx'];
        target_full_path = [Determine_Quaddle_Path(target,stim_path,false) target]; 
        distractor = ['S00_P00_' patternless_colors{1} '_T00_A00_E00.fbx'];
        distractor_full_path = [Determine_Quaddle_Path(distractor,stim_path,false) distractor];
        
        %-----Generate Item File---%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_Itm.txt'],'w');
        fprintf(fileID,itm_header);
        item_num = 0;
        item_locc_index = NaN(2,length(xzlocs));
        for Q = 1:2
            for locs = 1:length(xzlocs)
                item_num = item_num+1;
                item_locc_index(Q,locs) = item_num;
                if any(Q == 1)%target
                    str = [num2str(item_num) '\t' target '\t' target_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '1' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                else %distractor
                    str = [num2str(item_num) '\t' distractor '\t' distractor_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '0' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                end
                fprintf(fileID,str);
            end
        end
        fclose(fileID);
        
        %----Generate Condition file-----%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_CND.txt'],'w');
        fprintf(fileID,cnd_header);
        
        for t = 1:num_trials2
            
            item_index = randperm(length(xzlocs));
            
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(context_nums) '\t'  num2str(context_nums)  '\t'];
            
            if num_distractor_Quaddles(numD) == 0
                str3 = [num2str(item_locc_index(1,item_index(1)))];
            else
                str3 = [num2str(item_locc_index(1,item_index(1))) '\t'];
            end
            
            str4 = [];
            for stim = 1:num_distractor_Quaddles(numD)
                if stim == num_distractor_Quaddles(numD)
                    str4= [str4  num2str(item_locc_index(2,item_index(stim+1)))];
                else
                    str4= [str4  num2str(item_locc_index(2,item_index(stim+1))) '\t'];
                end
            end
            fprintf(fileID,[str1 str2 str3  str4 '\n']);
        end
        
        fclose(fileID);
    end
    fclose(blockfID);
end

cd(current_dir)
fclose all
%% Generate Multiple feature dimensions sessions with 1 feature per block

rng(20180915); %seed random number generator, do not change EVER &...
%always run file from the start and generate all files

%randomize...so that...
dims = [shape_index(4:end) arm_index(4:end)+100 pattern_index(4:end)+200 color_index(4:end)+300];

while any(abs(diff(diff(dims))) < 10) % so no more than 2 of any dim are in a row
    dims = dims(randperm(length(dims)));
end


min_num_trials = 75;
max_num_trials = 75;

%---For Shapes---%
file_base_name = 'FS_DF03_Multi_Set';%D is for the diffculty mode
for file = 1:5
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    if file == 5 %want 4 "nlocks" of 1 block fam + 1 block 12 distractor search
        num_blocks = 2;
    else
        num_blocks = 4;
    end
    for block = 1:num_blocks
        %---Second Generate the Item and Condition files for each Block----%
        stim_index = (file-1)*4+block;
        for numD = 1:2
            
            if numD == 1%for fam block
                num_trials1 = fam_num_trials;
                num_trials2 = fam_num_trials;
                num_distractor_Quaddles = 0;
            else
                num_trials1 = min_num_trials;
                num_trials2 = max_num_trials;
                num_distractor_Quaddles = 12;
            end
            
            blockpath = [itemCND_path filebase '\\\\' filebase '_D' num2str(num_distractor_Quaddles) '_B' num2str(block)];
            bdstr2 = ['{"BlockID": "blockid.' [filebase '_D' num2str(num_distractor_Quaddles)  '_B' num2str(block)] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
                blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
                '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
                '],...' '\n'...
                '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
            
            if block == num_blocks && numD == 2
                bdstr3 =  ']\n//end of blockDef[]';
            else
                bdstr3 = '\n\n';
            end
            fprintf(blockfID,bdstr2);
            fprintf(blockfID,bdstr3);
            
            if dims(stim_index) < 100
                target = [shapes{dims(stim_index)} '_P00_' patternless_colors{1} '_T00_A00_E00.fbx'];
                        target_full_path = [Determine_Quaddle_Path(target,stim_path,false) target]; 
            elseif dims(stim_index) > 100 && dims(stim_index) < 200
                target = ['S00_P00_' patternless_colors{1} '_T00_' arms{dims(stim_index)-100} '.fbx'];
                        target_full_path = [Determine_Quaddle_Path(target,stim_path,false) target]; 
            elseif dims(stim_index) > 200 && dims(stim_index) < 300
                target = ['S00_' patterns{dims(stim_index)-200} '_' gray_pattern_color '_T00_A00_E00.fbx'];
                        target_full_path = [Determine_Quaddle_Path(target,stim_path,false) target]; 
            elseif dims(stim_index) > 300 && dims(stim_index) < 400
                target = ['S00_P00_' patternless_colors{dims(stim_index)-300} '_T00_A00_E00.fbx'];
                        target_full_path = [Determine_Quaddle_Path(target,stim_path,false) target]; 
            else
                disp('Dimension Not recognized')
            end
            
            
            distractor = ['S00_P00_' patternless_colors{1} '_T00_A00_E00.fbx'];
            distractor_full_path = [Determine_Quaddle_Path(distractor,stim_path,false) distractor]; 
            
            %-----Generate Item File---%
            fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles)  '_B' num2str(block) '_Itm.txt'],'w');
            fprintf(fileID,itm_header);
            item_num = 0;
            item_locc_index = NaN(2,length(xzlocs));
            for Q = 1:2
                for locs = 1:length(xzlocs)
                    item_num = item_num+1;
                    item_locc_index(Q,locs) = item_num;
                    if any(Q == 1)%target
                        str = [num2str(item_num) '\t' target '\t' target_full_path '\t' ...
                            '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                            num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                            '80' '\t' '10' '\t' '10' '\t' ...
                            '1' '\t' '3' '\t' '9999' '\t' ...
                            'true' '\t' 'true' '\t' 'true' '\n'];
                    else %distractor
                        str = [num2str(item_num) '\t' distractor '\t' distractor_full_path '\t' ...
                            '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                            num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                            '80' '\t' '10' '\t' '10' '\t' ...
                            '0' '\t' '3' '\t' '9999' '\t' ...
                            'true' '\t' 'true' '\t' 'true' '\n'];
                    end
                    fprintf(fileID,str);
                end
            end
            fclose(fileID);
            
            %----Generate Condition file-----%
            fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles) '_B' num2str(block) '_CND.txt'],'w');
            fprintf(fileID,cnd_header);
            
            for t = 1:num_trials2
                
                item_index = randperm(length(xzlocs));
                
                str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
                str2 = ['ContextNum' num2str(context_nums) '\t'  num2str(context_nums)  '\t'];
                
                if num_distractor_Quaddles == 0
                    str3 = [num2str(item_locc_index(1,item_index(1)))];
                else
                    str3 = [num2str(item_locc_index(1,item_index(1))) '\t'];
                end
                
                str4 = [];
                for stim = 1:num_distractor_Quaddles
                    if stim == num_distractor_Quaddles
                        str4= [str4  num2str(item_locc_index(2,item_index(stim+1)))];
                    else
                        str4= [str4  num2str(item_locc_index(2,item_index(stim+1))) '\t'];
                    end
                end
                fprintf(fileID,[str1 str2 str3  str4 '\n']);
            end
            
            fclose(fileID);
        end
    end
    fclose(blockfID);
end
cd(current_dir)
fclose all;

%% Generate Extra Arms Sets because monkeys sometimes perform poorly (even at chance)
%these include block shifts

feature_value_order = [[3 2 1]; [1 2 3];[2 3 1]];
num_distractor_Quaddles = [0 1 3]; %cap since also introducing block shifting
fam_num_trials = 10;
min_num_trials = 40;
max_num_trials = 100;


%---For Extra Arms---%
file_base_name = 'FS_DF03_Exta_Arms_Set';%D is for the diffculty mode
for file = 1:3
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    for superblock = 1:3
        
        %---Second Generate the Item and Condition files for each Block----%
        for numD = 1:length(num_distractor_Quaddles)
            
            if numD == 1%for fam block
                num_trials1 = fam_num_trials;
                num_trials2 = min_num_trials;
            else
                num_trials1 = min_num_trials;
                num_trials2 = max_num_trials;
            end
            
            blockpath = [itemCND_path filebase '\\\\' filebase '_A' num2str(superblock) '_D' num2str(num_distractor_Quaddles(numD))];
            bdstr2 = ['{"BlockID": "blockid.' [filebase  '_A' num2str(superblock) '_D' num2str(num_distractor_Quaddles(numD))] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
                blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
                '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
                '],...' '\n'...
                '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
            
            if superblock == 3 && numD == length(num_distractor_Quaddles)
                bdstr3 =  ']\n//end of blockDef[]';
            else
                bdstr3 = '\n\n';
            end
            fprintf(blockfID,bdstr2);
            fprintf(blockfID,bdstr3);
            
            target = ['S00_P00_' patternless_colors{1} '_T00_' arms{arm_index(feature_value_order(file,superblock))} '.fbx'];
                    target_full_path = [Determine_Quaddle_Path(target,stim_path,false) target]; 
            distractor = ['S00_P00_' patternless_colors{1} '_T00_A00_E00.fbx'];
            distractor_full_path = [Determine_Quaddle_Path(distractor,stim_path,false) distractor]; 
            
            %-----Generate Item File---%
            fileID = fopen([filebase  '_A' num2str(superblock) '_D' num2str(num_distractor_Quaddles(numD)) '_Itm.txt'],'w');
            fprintf(fileID,itm_header);
            item_num = 0;
            item_locc_index = NaN(2,length(xzlocs));
            for Q = 1:2
                for locs = 1:length(xzlocs)
                    item_num = item_num+1;
                    item_locc_index(Q,locs) = item_num;
                    if any(Q == 1)%target
                        str = [num2str(item_num) '\t' target '\t' target_full_path '\t' ...
                            '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                            num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                            '80' '\t' '10' '\t' '10' '\t' ...
                            '1' '\t' '3' '\t' '9999' '\t' ...
                            'true' '\t' 'true' '\t' 'true' '\n'];
                    else %distractor
                        str = [num2str(item_num) '\t' distractor '\t' distractor_full_path '\t' ...
                            '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                            num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                            '80' '\t' '10' '\t' '10' '\t' ...
                            '0' '\t' '3' '\t' '9999' '\t' ...
                            'true' '\t' 'true' '\t' 'true' '\n'];
                    end
                    fprintf(fileID,str);
                end
            end
            fclose(fileID);
            
            %----Generate Condition file-----%
            fileID = fopen([filebase  '_A' num2str(superblock) '_D' num2str(num_distractor_Quaddles(numD)) '_CND.txt'],'w');
            fprintf(fileID,cnd_header);
            
            for t = 1:num_trials2
                
                item_index = randperm(length(xzlocs));
                
                str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
                str2 = ['ContextNum' num2str(context_nums) '\t'  num2str(context_nums)  '\t'];
                
                if num_distractor_Quaddles(numD) == 0
                    str3 = [num2str(item_locc_index(1,item_index(1)))];
                else
                    str3 = [num2str(item_locc_index(1,item_index(1))) '\t'];
                end
                
                str4 = [];
                for stim = 1:num_distractor_Quaddles(numD)
                    if stim == num_distractor_Quaddles(numD)
                        str4= [str4  num2str(item_locc_index(2,item_index(stim+1)))];
                    else
                        str4= [str4  num2str(item_locc_index(2,item_index(stim+1))) '\t'];
                    end
                end
                fprintf(fileID,[str1 str2 str3  str4 '\n']);
            end
            
            fclose(fileID);
        end
    end
    fclose(blockfID);
end
cd(current_dir);