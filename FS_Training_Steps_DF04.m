clear,clc,close all

rng(20180916); %seed random number generator, do not change EVER &...
%always run file from the start and generate all files


%Example Paths for Dual-Drive Computer
%%stim_path = 'D:\\QuaddleRepository_20180515\\';
%%itemCND_path = 'D:\\\\TrialFiles\\\\Feature_Search_Training\\\\';

%Example Paths for Kiosk Computers with 1 drive
stim_path = 'C:\\Users\\Womelsdorf Lab\\Documents\\Quaddles\\TexturelessQuaddleRepo\\';
itemCND_path = 'C:\\\\Users\\\\Womelsdorf Lab\\\\Documents\\\\BlockDefinitions\\\\Feature_Search_Training\\\\';


current_dir = pwd;

spatial_scale = 0.05;
RewardMag = 3;
context_nums = -1;

y_default = 0.4;
x_spacing = 2;%also exlucde within 2 units of center, arms make us need more spacing on x-axis
z_spacing = 1.5;%also exlucde within 1.5 units of center
x_max = 8;%magnitude
z_max = 4.5;%magnitude

num_distractor_Quaddles = [0 1 3 5 9 12];
fam_num_trials = 10;
min_num_trials = 40;
max_num_trials = 100;

shapes = {'S00','S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P00','P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

patternless_colors = {'C6000000_6000000', 'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070287_6070287', 'C6070335_6070335'};
patterned_colors =   {'C7000000_5000000', 'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000', 'C7070286_5000000', 'C7070335_5000000'};

arms = {'A00_E00','A01_E00', 'A02_E00','A00_E01', 'A00_E02', 'A00_E03','A01_E01', 'A02_E01','A01_E02', 'A01_E03','A02_E02','A02_E03'};

num_features = [length(shapes)-1 length(patterns)-1 length(patternless_colors)-1 length(arms)-1];

%% Generate all possible positions
xzlocs = [];
count = 0;
for x = -x_max:x_spacing:x_max
    for z = -z_max:z_spacing:z_max
        if x == 0 && z == 0
            continue
        else
            count = count+1;
            xzlocs(1,count) = x;
            xzlocs(2,count) = z;
        end
    end
end

%% Generate ITM and CND Headers which are always the same for all files

itm_header=['StimCode' '\t' 'StimName' '\t' 'StimPath' '\t' ...
    'StimDimVals1' '\t' 'StimDimVals2' '\t' 'StimDimVals3' '\t' 'StimDimVals4' '\t' 'StimDimVals5' '\t' ...
    'StimLocationX' '\t' 'StimLocationY' '\t' 'StimLocationZ' '\t' ...
    'StimRotationX' '\t' 'StimRotationY' '\t' 'StimRotationZ' '\t' ...
    'StimTrialRewardProb' '\t' 'StimTrialRewardMag' '\t' 'TimesUsedInBlock' ...
    '\t' 'isRelevant' '\t' 'SetLocation' '\t' 'SetRotation' '\n'];

cnd_header=['TrialName' '\t' 'TrialCode' '\t' 'ContextNum' '\t' 'ContextName' '\t' ...
    'Stim1' '\t'  'Stim2' '\t'  'Stim3' '\t'  'Stim4' '\t'  'Stim5' '\t'  'Stim6' '\t' ...
    'Stim7' '\t'  'Stim8' '\t'  'Stim9' '\t'  'Stim10' '\t'  'Stim11' '\t'  'Stim12' '\t' ...
    'Stim13' '\t'  'Stim14' '\n'];
%% Generate 2D sets

number_of_Quaddle_dimensions = 2;
file_base_name = 'FS_DF04_2D_Set';%D is for the diffculty mode
for file = 1:5
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    feature_vals = NaN(1,4);
    feature_vals2 = ones(number_of_Quaddle_dimensions,4); %for distractors
    dims = randperm(4);
    for d = 1:4
        if any(dims(1:number_of_Quaddle_dimensions ) == d)
            feature_vals(d) = randi(num_features(d))+1;%first value is neutral
            if dims(1) == d
                feature_vals2(1,d) = feature_vals(d);
            elseif dims(2) == d
                feature_vals2(2,d) = feature_vals(d);
            elseif dims(3) == d
                feature_vals2(3,d) = feature_vals(d);
            else
                feature_vals2(4,d) = feature_vals(d);
            end
        else
            feature_vals(d) = 1;%neutral
        end
    end

    %---Second Generate the Item and Condition files for each Block----%
    for numD = 1:length(num_distractor_Quaddles)
        
        if numD == 1%for fam block
            num_trials1 = fam_num_trials;
            num_trials2 = min_num_trials;
        elseif numD  == length(num_distractor_Quaddles)%for last block do lots of trials so won't finish block before stop working
            num_trials1 = 4*max_num_trials;
            num_trials2 = 4*max_num_trials;
        else
            num_trials1 = min_num_trials;
            num_trials2 = max_num_trials;
        end
        
        blockpath = [itemCND_path filebase '\\\\' filebase '_D' num2str(num_distractor_Quaddles(numD))];
        bdstr2 = ['{"BlockID": "blockid.' [filebase '_D' num2str(num_distractor_Quaddles(numD))] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if numD == length(num_distractor_Quaddles)
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        if feature_vals(2) == 1 %patternless
            target = [shapes{feature_vals(1)} '_' patterns{feature_vals(2)} '_' patternless_colors{feature_vals(3)}  '_T00_' arms{feature_vals(4)}  '.fbx'];
        else %with pattern
            target = [shapes{feature_vals(1)} '_' patterns{feature_vals(2)} '_' patterned_colors{feature_vals(3)}  '_T00_' arms{feature_vals(4)}  '.fbx'];
        end
        if numD == 1
            disp([file_base_name num2str(file)])
            disp(['Target: ' target])
        end
        
        target_full_path = [Determine_Quaddle_Path(target,stim_path,false) target];
        
        distractors = cell(1,3);
        distractor_full_path = cell(1,3);
        for d = 1:(number_of_Quaddle_dimensions+1)
            if d == (number_of_Quaddle_dimensions+1)
                  distractors{d} = ['S00_P00_' patternless_colors{1} '_T00_A00_E00.fbx'];
            else
                if feature_vals2(d,2) == 1 %patternless
                    distractors{d} = [shapes{feature_vals2(d,1)} '_' patterns{feature_vals2(d,2)} '_' patternless_colors{feature_vals2(d,3)}  '_T00_' arms{feature_vals2(d,4)}  '.fbx'];
                else %with pattern
                    distractors{d} = [shapes{feature_vals2(d,1)} '_' patterns{feature_vals2(d,2)} '_' patterned_colors{feature_vals2(d,3)}  '_T00_' arms{feature_vals2(d,4)}  '.fbx'];
                end
            end
            distractor_full_path{d} = [Determine_Quaddle_Path(distractors{d},stim_path,false) distractors{d}]; 
        end
     
        
        %-----Generate Item File---%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_Itm.txt'],'w');
        fprintf(fileID,itm_header);
        item_num = 0;
        item_locc_index = NaN((number_of_Quaddle_dimensions+2),length(xzlocs));
        for Q = 1:(number_of_Quaddle_dimensions+2)
            for locs = 1:length(xzlocs)
                item_num = item_num+1;
                item_locc_index(Q,locs) = item_num;
                if any(Q == 1)%target
                    str = [num2str(item_num) '\t' target '\t' target_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '1' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                else %distractor
                    str = [num2str(item_num) '\t' distractors{Q-1} '\t' distractor_full_path{Q-1} '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '0' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                end
                fprintf(fileID,str);
            end
        end
        fclose(fileID);
        
        %----Generate Condition file-----%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_CND.txt'],'w');
        fprintf(fileID,cnd_header);
        
        for t = 1:num_trials2
            
            item_index = randperm(length(xzlocs));
            distractor_index = [1:length(distractors) 1:length(distractors) 1:length(distractors) 1:length(distractors)];
            distractor_index = distractor_index(randperm(length(distractor_index)));
            
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(context_nums) '\t'  num2str(context_nums)  '\t'];
            
            if num_distractor_Quaddles(numD) == 0
                str3 = [num2str(item_locc_index(1,item_index(1)))];
            else
                str3 = [num2str(item_locc_index(1,item_index(1))) '\t'];
            end
            
            str4 = [];
            for stim = 1:num_distractor_Quaddles(numD)
                if stim == num_distractor_Quaddles(numD)
                    str4= [str4  num2str(item_locc_index(1+distractor_index(stim),item_index(stim+1)))];
                else 
                    str4= [str4  num2str(item_locc_index(1+distractor_index(stim),item_index(stim+1))) '\t'];
                end
            end
            fprintf(fileID,[str1 str2 str3  str4 '\n']);
        end
        
        fclose(fileID);
    end
    fclose(blockfID);
end

%% Generate 3D sets

number_of_Quaddle_dimensions = 3;
file_base_name = 'FS_DF04_3D_Set';%D is for the diffculty mode
for file = 1:5
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    %---First Generate the BlockDef----%
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    feature_vals = NaN(1,4);
    feature_vals2 = ones(number_of_Quaddle_dimensions,4); %for distractors
    dims = randperm(4);
    for d = 1:4
        if any(dims(1:number_of_Quaddle_dimensions ) == d)
            feature_vals(d) = randi(num_features(d))+1;%first value is neutral
            if dims(1) == d
                feature_vals2(1,d) = feature_vals(d);
            elseif dims(2) == d
                feature_vals2(2,d) = feature_vals(d);
            elseif dims(3) == d
                feature_vals2(3,d) = feature_vals(d);
            else
                feature_vals2(4,d) = feature_vals(d);
            end
        else
            feature_vals(d) = 1;%neutral
        end
    end

    %---Second Generate the Item and Condition files for each Block----%
    for numD = 1:length(num_distractor_Quaddles)
        
        if numD == 1%for fam block
            num_trials1 = fam_num_trials;
            num_trials2 = min_num_trials;
        elseif numD  == length(num_distractor_Quaddles)%for last block do lots of trials so won't finish block before stop working
            num_trials1 = 4*max_num_trials;
            num_trials2 = 4*max_num_trials;
        else
            num_trials1 = min_num_trials;
            num_trials2 = max_num_trials;
        end
        
        blockpath = [itemCND_path filebase '\\\\' filebase '_D' num2str(num_distractor_Quaddles(numD))];
        bdstr2 = ['{"BlockID": "blockid.' [filebase '_D' num2str(num_distractor_Quaddles(numD))] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if numD == length(num_distractor_Quaddles)
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        if feature_vals(2) == 1 %patternless
            target = [shapes{feature_vals(1)} '_' patterns{feature_vals(2)} '_' patternless_colors{feature_vals(3)}  '_T00_' arms{feature_vals(4)}  '.fbx'];
        else %with pattern
            target = [shapes{feature_vals(1)} '_' patterns{feature_vals(2)} '_' patterned_colors{feature_vals(3)}  '_T00_' arms{feature_vals(4)}  '.fbx'];
        end
        if numD == 1
            disp([file_base_name num2str(file)])
            disp(['Target: ' target])
        end
        
        target_full_path = [Determine_Quaddle_Path(target,stim_path,false) target];
        
        distractors = cell(1,3);
        distractor_full_path = cell(1,3);
        for d = 1:(number_of_Quaddle_dimensions+1)
            if d == (number_of_Quaddle_dimensions+1)
                  distractors{d} = ['S00_P00_' patternless_colors{1} '_T00_A00_E00.fbx'];
            else
                if feature_vals2(d,2) == 1 %patternless
                    distractors{d} = [shapes{feature_vals2(d,1)} '_' patterns{feature_vals2(d,2)} '_' patternless_colors{feature_vals2(d,3)}  '_T00_' arms{feature_vals2(d,4)}  '.fbx'];
                else %with pattern
                    distractors{d} = [shapes{feature_vals2(d,1)} '_' patterns{feature_vals2(d,2)} '_' patterned_colors{feature_vals2(d,3)}  '_T00_' arms{feature_vals2(d,4)}  '.fbx'];
                end
            end
            distractor_full_path{d} = [Determine_Quaddle_Path(distractors{d},stim_path,false) distractors{d}]; 
        end
     
        
        %-----Generate Item File---%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_Itm.txt'],'w');
        fprintf(fileID,itm_header);
        item_num = 0;
        item_locc_index = NaN((number_of_Quaddle_dimensions+2),length(xzlocs));
        for Q = 1:(number_of_Quaddle_dimensions+2)
            for locs = 1:length(xzlocs)
                item_num = item_num+1;
                item_locc_index(Q,locs) = item_num;
                if any(Q == 1)%target
                    str = [num2str(item_num) '\t' target '\t' target_full_path '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '1' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                else %distractor
                    str = [num2str(item_num) '\t' distractors{Q-1} '\t' distractor_full_path{Q-1} '\t' ...
                        '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        '0' '\t' '3' '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                end
                fprintf(fileID,str);
            end
        end
        fclose(fileID);
        
         %----Generate Condition file-----%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_CND.txt'],'w');
        fprintf(fileID,cnd_header);
        
        for t = 1:num_trials2
            
            item_index = randperm(length(xzlocs));
            distractor_index = [1:length(distractors) 1:length(distractors) 1:length(distractors) 1:length(distractors)];
            distractor_index = distractor_index(randperm(length(distractor_index)));
            
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(context_nums) '\t'  num2str(context_nums)  '\t'];
            
            if num_distractor_Quaddles(numD) == 0
                str3 = [num2str(item_locc_index(1,item_index(1)))];
            else
                str3 = [num2str(item_locc_index(1,item_index(1))) '\t'];
            end
            
            str4 = [];
            for stim = 1:num_distractor_Quaddles(numD)
                if stim == num_distractor_Quaddles(numD)
                    str4= [str4  num2str(item_locc_index(1+distractor_index(stim),item_index(stim+1)))];
                else 
                    str4= [str4  num2str(item_locc_index(1+distractor_index(stim),item_index(stim+1))) '\t'];
                end
            end
            fprintf(fileID,[str1 str2 str3  str4 '\n']);
        end
        
        fclose(fileID);
    end
    fclose(blockfID);
end

cd(current_dir)
fclose all;