%Modified from FLU2FS_Circular_Training_Sets on 6/21/19 to implement
%8 Stimuli in FLU blocks as well so smoother transitions....means all blocks
%are hard coded. All trials for FLU, Fam, and FS have 8 stimuli in a
%concentric circle; "unused"  locations are filled with neutral Quaddles as
%space fillers.

%Code generates super blocks: 1 block of FLU task followed by 1 familiarization
%block followed by 1 block of FS; all trials within a super block have the
% same target feature values. Familarization blocksa are optional
%%
clear,clc,close all

current_dir = pwd;
%% Stim Path Parameters

%Example Paths for Dual-Drive Computer
stim_path = 'D:\\TextureLessQuaddleRepo_05032019\\';
itemCND_path = 'D:\\\\BlockDefnitions\\\\FLU2FS_Training\\\\';

%Example Paths for Kiosk Computers with 1 drive
%%stim_path = 'C:\\Users\\Womelsdorf Lab\\Documents\\Quaddles\\TexturelessQuaddleRepo\\';
%%itemCND_path = 'C:\\\\Users\\\\Womelsdorf Lab\\\\Documents\\\\BlockDefinitions\\\\Feature_Search_Training\\\\';


%% General Task Parameters
rng(06222019);%for task with context & fam trials

RewardMag = 1;
RewardProb = 1.0;

num_sets_to_generate = 50;
num_blocks = 45;%# of super blocks (FLU + Fam + FS)

add_re_familiarization_block = false;%if true adds a # of refamilaization trials in which only target feature value
%is shown to reinforce that the rule hasn't changed yet
num_fam_trials = 3;

using_contexts = true;%if true, blocks alterante context backgrounds to cue rule change
context_nums = 0:4;
bad_context_colors = [NaN 3 4 6 1];%colors that may be harder to discriminate given the background
%gravel, sand, grass, ice, red brick

%% Main Configurable Parameters for FLU trials

num_FLU_Quaddles_per_trial = 3;%must be at least 2 for 1 Target and 1 distractor
num_feature_values_per_dim = 3; %best to be equal to num_FLU_Quaddles_per_trial
num_feature_dimensions = 3;%best if not more than 4, preferably 2 or 3

max_num_trials = 60;

min_num_FLU_trials = [25 30 35 25 30 35 25 30 35];

number_of_irrelevant_feature_dims = [0 0 0 1 1 1 2 2 2];%should be same length
%as the min_num_FLU_trials. distribution of values determines distribution of #
%of irrelevant features by block.
%% Main Configurable Parameters for FS trials

%for 8 ciruclar positions
num_Distractors = [3 5 7];%does not include target

FS_target_dimensionality = 3;%always 3D targets/2 irrelevant features

num_search_reps = 3;%number of times to do each # distractor
num_search_trials = length(num_Distractors)*num_search_reps;
%% Feature Dimensions and values
shapes = {'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

patterned_colors =   {'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000','C7070240_5000000','C7070286_5000000', 'C7070335_5000000'};
%patternless_colors = {'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070239_6070239','C6070287_6070287', 'C6070335_6070335'};
patternless_colors = {'C7070014_7070014', 'C7070059_7070059', 'C7070106_7070106', 'C7070148_7070148', 'C7070194_7070194','C7070240_7070240','C7070286_7070286', 'C7070335_7070335'};
gray_pattern_color = 'C7000000_5000000';

arms = {'A00_E01', 'A00_E02','A00_E03', 'A01_E00', 'A01_E01', 'A01_E02', 'A01_E03', 'A02_E00', 'A02_E01', 'A02_E02', 'A02_E03'};

neutral_Quaddle = 'S00_P00_C7000000_7000000_T00_A00_E00';
%% Quaddle Spacing-Generate All possible locations
%this is the big difference from original version

y_default = 0.4;

%radius = 5.65;%for 19" Touch screen to be about ~10 dva
radius = 4.7;%for 24" BenQ to be 10 dva
angles = 0:45:365-45;

xzlocs = NaN(2,length(angles));
for an = 1:length(angles)
    xzlocs(1,an) = round(radius*cosd(angles(an)),2);
    xzlocs(2,an) = round(radius*sind(angles(an)),2);
end

%% Generate Block Def Files

file_base_name_str =  ['FLU2FS_v3__' num2str(num_FLU_Quaddles_per_trial) 'AFC__' ...
    num2str(num_feature_values_per_dim) 'x' num2str(num_feature_dimensions) ...
    '__' num2str(min(number_of_irrelevant_feature_dims)) '_' num2str(max(number_of_irrelevant_feature_dims))...
    'irrel'];

if add_re_familiarization_block
    file_base_name_fam_str = ['__Fam' num2str(num_fam_trials)];
else
    file_base_name_fam_str = [];
end

file_base_name = [file_base_name_str file_base_name_fam_str '__Set'];

%%
for set = 1:num_sets_to_generate
    filebase = [file_base_name num2str(set)];
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%---Determine Main Block Specifications---%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %---Pick 2 contexts---%
    these_context_nums = context_nums(randperm(length(context_nums)));
    these_context_nums = these_context_nums(1:2);
    
    %-----Shuffle feature dimensions-----%
    these_dims = 1:4;
    these_dims = these_dims(randperm(4));
    these_dims = these_dims(1:num_feature_dimensions);
    these_dims = sort(these_dims);
    
    %-----Shuffle feature values-----%
    shape_index = randperm(length(shapes));
    shape_index = sort(shape_index(1:num_feature_values_per_dim));
    
    %exclude [patterns 4/5 and 6/7 from being with each other
    all_pattern_index = randperm(length(patterns));
    pattern_index = sort(all_pattern_index(1:num_feature_values_per_dim));
    while (any(pattern_index == 4) && any(pattern_index == 5)) ||...
            (any(pattern_index == 6) && any(pattern_index == 7))
        all_pattern_index = randperm(length(patterns));
        pattern_index = sort(all_pattern_index(1:num_feature_values_per_dim));
    end
    
    if using_contexts
        these_colors = 1:length(patternless_colors);
        %remove "bad" colors
        for con = 1:2
            if ~isnan(bad_context_colors(these_context_nums(con)+1))
                these_colors(these_colors == bad_context_colors(these_context_nums(con)+1)) = [];
            end
        end
        tryagain = true;
        while tryagain
            color_index = these_colors(randperm(length(these_colors)));
            selected_colors = color_index(1:num_feature_values_per_dim);
            if all(pdist(color_index(1:num_feature_values_per_dim)') > 1)
                %color map is circular so check for 1 next to 8
                if ~any(selected_colors == 1 | selected_colors == 8)
                    tryagain = false;
                    color_index = sort(color_index(1:num_feature_values_per_dim));
                elseif (any(selected_colors == 1) && ~any(selected_colors == 8)) || ...
                        (any(selected_colors == 8) && ~any(selected_colors == 1))
                    tryagain = false;
                    color_index = sort(color_index(1:num_feature_values_per_dim));
                end
            end
        end
    else
        color_index = randperm(length(patternless_colors));
        color_index = sort(color_index(1:num_feature_values_per_dim));
    end
    
    arm_index = randperm(length(arms));
    arm_index = sort(arm_index(1:num_feature_dimensions));
    
    all_dimensional_indexes = [shape_index; pattern_index; color_index; arm_index];
    
    %-----Generate Intra/Extra Dimension Patterns & # Irrelevant Dims-----%
    sub_block_len = length(number_of_irrelevant_feature_dims);
    num_sub_blocks = ceil((num_blocks)/sub_block_len);
    all_intra_extra = [];
    all_number_of_irrelevant_feature_dims = [];
    all_number_min_trials = [];
    for sb = 1:num_sub_blocks
        %not a perfect balance if do all blocks but close over shorter # of blocks
        intra_extra = [zeros(1,ceil(sub_block_len/2)) ones(1,ceil(sub_block_len/2))];
        intra_extra = intra_extra(randperm(sub_block_len));
        all_intra_extra = [all_intra_extra intra_extra];
        
        %get shuffled in same order to make sure that there are equal # of
        %min # trials for each # irrel
        rand_ind = randperm(sub_block_len);
        num_irrel = number_of_irrelevant_feature_dims(rand_ind);
        num_min_trials = min_num_FLU_trials(rand_ind);
        all_number_of_irrelevant_feature_dims = [all_number_of_irrelevant_feature_dims num_irrel];
        all_number_min_trials = [all_number_min_trials num_min_trials];
    end
    all_intra_extra = all_intra_extra(1:num_blocks);
    all_number_of_irrelevant_feature_dims = all_number_of_irrelevant_feature_dims(1:num_blocks);
    all_number_min_trials = all_number_min_trials(1:num_blocks);
    
    %%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%---Deterime Quaddle Stimuli----%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    target_dims_vals = NaN(num_blocks,2);
    
    dim = [];
    fv = [];
    for b = 1:num_blocks
        %Target Feature Dimension and Feature value
        dims = these_dims(randperm(num_feature_dimensions));
        fvs = randperm(num_feature_values_per_dim);
        if b == 1 %pick a random stimulus for the first set
            dim = dims(1);
            dims = dims(2:end);
            fv = fvs(1);
        elseif all_intra_extra(b) == 0 %intra dimensional shift
            dim = dim; %same as last time
            dims(dims == dim) = [];
            fvs(fvs == fv) = []; %so can't be the same one
            fv = fvs(1);
        else %extra-dimensional shift
            dims(dims == dim) = [];
            dim = dims(randi(num_feature_dimensions-1));
            
            dims = these_dims;
            dims(dims == dim) = [];
            
            fv = fvs(1);
        end
        
        if length(dims) < max(number_of_irrelevant_feature_dims)
            error('Where did my values go!')
        end
        target_dims_vals(b,1) = dim;%target dimension
        target_dims_vals(b,2) =  fv;%;target value
    end
    
    %---Generate All Feature Combinations---%
    %Determine all possible search targets and distractors
    %note indexing and math here is a little convoluted...sorry
    %pick all target/distractors to be maximum dimensionality
    %integer value is the dimension, and power of 10 is the index of
    %that dimension: e.g. 200 means 3 index in 2nd dimension
    if num_feature_dimensions >= 3
        %all tripple feature combos
        all_combos3 = combnk([these_dims 10*these_dims 100*these_dims],3);
        all_combos3(all_combos3(:,1)*10 == all_combos3(:,2),:) = [];
        all_combos3(all_combos3(:,1)*100 == all_combos3(:,2),:) = [];
        all_combos3(all_combos3(:,2)*10 == all_combos3(:,3),:) = [];
        all_combos3(all_combos3(:,2)*100 == all_combos3(:,3),:) = [];
        all_combos3(all_combos3(:,1)*10 == all_combos3(:,3),:) = [];
        all_combos3(all_combos3(:,1)*100 == all_combos3(:,3),:) = [];
        
        [targets_and_distractors3,target_and_distractors_dimVals3,targets_and_distractors_full_paths3] = ...
            feature_combos2_QuaddleNames(all_combos3,these_dims,stim_path,shapes,patterns,...
            patterned_colors,patternless_colors,arms,all_dimensional_indexes);
        
        %add Neutral Quadddle to list of targets and distractors
        targets_and_distractors3{end+1} = [neutral_Quaddle '.fbx'];
        targets_and_distractors_full_paths3{end+1} = [Determine_Quaddle_Path([neutral_Quaddle '.fbx'],stim_path,false) [neutral_Quaddle '.fbx']];
        target_and_distractors_dimVals3{end+1} = zeros(1,5);
    end
    if num_feature_dimensions >= 2
        %all double feature combos
        all_combos2 = combnk([these_dims 10*these_dims 100*these_dims],2);
        all_combos2(all_combos2(:,1)*10 == all_combos2(:,2),:) = [];
        all_combos2(all_combos2(:,1)*100 == all_combos2(:,2),:) = [];
        
        [targets_and_distractors2,target_and_distractors_dimVals2,targets_and_distractors_full_paths2] = ...
            feature_combos2_QuaddleNames(all_combos2,these_dims,stim_path,shapes,patterns,...
            patterned_colors,patternless_colors,arms,all_dimensional_indexes);
        
        %add Neutral Quadddle to list of targets and distractors
        targets_and_distractors2{end+1} = [neutral_Quaddle '.fbx'];
        targets_and_distractors_full_paths2{end+1} = [Determine_Quaddle_Path([neutral_Quaddle '.fbx'],stim_path,false) [neutral_Quaddle '.fbx']];
        target_and_distractors_dimVals2{end+1} = zeros(1,5);
    end
    %all single feature combos
    all_combos1 = combnk([these_dims 10*these_dims 100*these_dims],1);
    [targets_and_distractors1,target_and_distractors_dimVals1,targets_and_distractors_full_paths1] = ...
        feature_combos2_QuaddleNames(all_combos1,these_dims,stim_path,shapes,patterns,...
        patterned_colors,patternless_colors,arms,all_dimensional_indexes);
    
    %add Neutral Quadddle to list of targets and distractors
    targets_and_distractors1{end+1} = [neutral_Quaddle '.fbx'];
    targets_and_distractors_full_paths1{end+1} = [Determine_Quaddle_Path([neutral_Quaddle '.fbx'],stim_path,false) [neutral_Quaddle '.fbx']];
    target_and_distractors_dimVals1{end+1} = zeros(1,5);
    
    %%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%---Generate the BlockDef----%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    for b = 1:num_blocks
        if using_contexts
            if rem(b,2) == 0%even block
                current_context = these_context_nums(2);
            else
                current_context = these_context_nums(1);
            end
        else
            current_context = -1;
        end
        
        %---BlockDef for FLU trials---%
        
        if b == 1
            FLU_block_id = ['FLU_Set' num2str(set) '_B' num2str(b) '_' ...
                num2str(all_number_of_irrelevant_feature_dims(b)) 'Irrel_First'];
        elseif all_intra_extra(b) == 0
            FLU_block_id = ['FLU_Set' num2str(set) '_B' num2str(b) '_' ...
                num2str(all_number_of_irrelevant_feature_dims(b)) 'Irrel_Intra'];
        elseif all_intra_extra(b) == 1
            FLU_block_id = ['FLU_Set' num2str(set) '_B' num2str(b) '_' ...
                num2str(all_number_of_irrelevant_feature_dims(b)) 'Irrel_Extra'];
        end
        
        %Detemrine number of trials in Block
        num_trials1 = all_number_min_trials(b);
        num_trials2 = max_num_trials;
        
        blockpath = [itemCND_path filebase '\\\\' filebase 'FLU_B' num2str(b)];
        bdstr2 = ['{"BlockID": "blockid.' FLU_block_id '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        bdstr3 = '\n\n';
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        %---BlockDef for Familarization Trials---%
        if add_re_familiarization_block
            Fam_block_id = ['Fam_Set' num2str(set) '_B' num2str(b)];
            
            blockpath = [itemCND_path filebase '\\\\' filebase 'Fam_B' num2str(b)];
            bdstr2 = ['{"BlockID": "blockid.' Fam_block_id '", "TrialRange": [' num2str(num_fam_trials) ',' num2str(num_fam_trials) '], "TrialDefPath": "' ...
                blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
                '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
                '],...' '\n'...
                '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
            
            bdstr3 = '\n\n';
            fprintf(blockfID,bdstr2);
            fprintf(blockfID,bdstr3);
        end
        
        %---BlockDef for FS trials---%
        
        FS_block_id = ['FS_Set' num2str(set) '_B' num2str(b)];
        blockpath = [itemCND_path filebase '\\\\' filebase 'FS_B' num2str(b)];
        bdstr2 = ['{"BlockID": "blockid.' FS_block_id '", "TrialRange": [' num2str(num_search_trials) ',' num2str(num_search_trials) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if b == num_blocks
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
    end
    fclose(blockfID);
    
    %%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%---Generate the ITM/CND files----%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    for b = 1:num_blocks
        if using_contexts
            if rem(b,2) == 0%even block
                current_context = these_context_nums(2);
            else
                current_context = these_context_nums(1);
            end
        else
            current_context = -1;
        end
        
        %% Determine Rewarded Feature
        %same for all blocks
        rewarded_features = zeros(1,5);
        if target_dims_vals(b,1) == 1 %shape
            rewarded_features(1) = shape_index(target_dims_vals(b,2));
        elseif target_dims_vals(b,1) == 2 %pattern
            rewarded_features(2) = pattern_index(target_dims_vals(b,2));
        elseif target_dims_vals(b,1) == 3 %color
            rewarded_features(3) = color_index(target_dims_vals(b,2));
        elseif target_dims_vals(b,1) == 4 %arms
            rewarded_features(5) = arm_index(target_dims_vals(b,2));
        else
            error('Stimulus not recognized')
        end
        
        %% ITM/CND for FLU Blocks
        %for distractor(s) dimensions
        distractor_dims = these_dims;
        distractor_dims(target_dims_vals(b,1) == distractor_dims) = [];
        distractor_dims = distractor_dims(randperm(length(distractor_dims)));
        dont_use_these_dims = distractor_dims(all_number_of_irrelevant_feature_dims(b)+1:end);
        
        if all_number_of_irrelevant_feature_dims(b) == 0
            these_targets_and_distractors = targets_and_distractors1;
            these_targets_and_distractors_full_paths = targets_and_distractors_full_paths1;
            these_target_and_distractors_dimVals = target_and_distractors_dimVals1;
            
            %remove Quaddles with non-neutral feature values in unwanted feature dimensions
            all_these_dim_vals = cell2mat(these_target_and_distractors_dimVals');
            rows_to_rmv = [];
            for dud = 1:length(dont_use_these_dims)
                if dont_use_these_dims(dud) == 4
                    rows_to_rmv = [rows_to_rmv; find(all_these_dim_vals(:,5) ~= 0)];
                else
                    rows_to_rmv = [rows_to_rmv; find(all_these_dim_vals(:,dont_use_these_dims(dud)) ~= 0)];
                end
            end
            rows_to_rmv = unique(rows_to_rmv);
            if length(rows_to_rmv) ~= 6
                error('Why are so many rows removed')
            end
            
            these_targets_and_distractors(rows_to_rmv) = [];
            these_targets_and_distractors_full_paths(rows_to_rmv) = [];
            these_target_and_distractors_dimVals(rows_to_rmv) = [];
            
        elseif all_number_of_irrelevant_feature_dims(b) == 1
            these_targets_and_distractors = targets_and_distractors2;
            these_targets_and_distractors_full_paths = targets_and_distractors_full_paths2;
            these_target_and_distractors_dimVals = target_and_distractors_dimVals2;
            
            %remove Quaddles with non-neutral feature values in unwanted feature dimensions
            all_these_dim_vals = cell2mat(these_target_and_distractors_dimVals');
            rows_to_rmv = [];
            for dud = 1:length(dont_use_these_dims)
                if dont_use_these_dims(dud) == 4
                    rows_to_rmv = [rows_to_rmv; find(all_these_dim_vals(:,5) ~= 0)];
                else
                    rows_to_rmv = [rows_to_rmv; find(all_these_dim_vals(:,dont_use_these_dims(dud)) ~= 0)];
                end
            end
            rows_to_rmv = unique(rows_to_rmv);
            if length(rows_to_rmv) ~= 18
                error('Why are so many rows removed')
            end
            
            these_targets_and_distractors(rows_to_rmv) = [];
            these_targets_and_distractors_full_paths(rows_to_rmv) = [];
            these_target_and_distractors_dimVals(rows_to_rmv) = [];
            
        elseif all_number_of_irrelevant_feature_dims(b) == 2
            these_targets_and_distractors = targets_and_distractors3;
            these_targets_and_distractors_full_paths = targets_and_distractors_full_paths3;
            these_target_and_distractors_dimVals = target_and_distractors_dimVals3;
        else
            error('Unknown # of Irrelevant Dimensions')
        end
        
        num_trial_distractors = num_FLU_Quaddles_per_trial-1;
        num_neutral = length(xzlocs)-num_trial_distractors-1;
        ITMCND_name = [filebase 'FLU_B' num2str(b)];
        task_type = 'FLU';
        
        GenerateITM_CND_files_from_target_distractor_list(ITMCND_name,current_context,...
            these_targets_and_distractors,these_targets_and_distractors_full_paths,...
            these_target_and_distractors_dimVals,xzlocs,y_default,rewarded_features,...
            num_trial_distractors,num_neutral,max_num_trials,RewardProb,RewardMag,task_type)
        
        %% ITM/CND for Fam Blocks
        
        if add_re_familiarization_block
            neutral_row = length(target_and_distractors_dimVals1);
            target_dim_row = find(all(cell2mat(target_and_distractors_dimVals1') == rewarded_features,2));
            these_targets_and_distractors = targets_and_distractors1([target_dim_row,neutral_row]);
            these_targets_and_distractors_full_paths = targets_and_distractors_full_paths1([target_dim_row,neutral_row]);
            these_target_and_distractors_dimVals = target_and_distractors_dimVals1([target_dim_row,neutral_row]);
            
            num_trial_distractors = 0;
            num_neutral = length(xzlocs)-1;
            ITMCND_name = [filebase 'Fam_B' num2str(b)];
            task_type = 'FS';%yes I know it's Fam block but essential it's 1D vs 0D FS
            
            GenerateITM_CND_files_from_target_distractor_list(ITMCND_name,current_context,...
                these_targets_and_distractors,these_targets_and_distractors_full_paths,...
                these_target_and_distractors_dimVals,xzlocs,y_default,rewarded_features,...
                num_trial_distractors,num_neutral,num_fam_trials,RewardProb,RewardMag,task_type)
        end
        
        %% ITM/CND for FLU Blocks
        if FS_target_dimensionality == 1
            these_targets_and_distractors = targets_and_distractors1;
            these_targets_and_distractors_full_paths = targets_and_distractors_full_paths1;
            these_target_and_distractors_dimVals = target_and_distractors_dimVals1;
        elseif FS_target_dimensionality == 2
            these_targets_and_distractors = targets_and_distractors2;
            these_targets_and_distractors_full_paths = targets_and_distractors_full_paths2;
            these_target_and_distractors_dimVals = target_and_distractors_dimVals2;
        elseif FS_target_dimensionality == 3
            these_targets_and_distractors = targets_and_distractors3;
            these_targets_and_distractors_full_paths = targets_and_distractors_full_paths3;
            these_target_and_distractors_dimVals = target_and_distractors_dimVals3;
        else
            error('Unknown # of Quaddle Dimensions')
        end
        
        num_trial_distractors = num_Distractors;
        num_neutral = NaN;
        ITMCND_name = [filebase 'FS_B' num2str(b)];
        task_type = 'FS';
        
        GenerateITM_CND_files_from_target_distractor_list(ITMCND_name,current_context,...
            these_targets_and_distractors,these_targets_and_distractors_full_paths,...
            these_target_and_distractors_dimVals,xzlocs,y_default,rewarded_features,...
            num_trial_distractors,num_neutral,num_search_trials,RewardProb,RewardMag,task_type)
        %%
    end
end
fclose all;
cd(current_dir)