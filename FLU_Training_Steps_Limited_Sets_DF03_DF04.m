%writen 2/13/19 Seth Koenig
%Scripts generate sets for FLU training partiuclalry for intra- and extra-
%dimensional shifts. All major parameters are configurable
%SDK 4/6/19 originally didn't have C7070240_5000000 included,so regenrated

clear,clc,close all

%rng won't work so well here because too many options
%rng(20190213); %seed random number generator, do not change EVER &...
%always run file from the start and generate all files

current_dir = pwd;

%Set random # generators so can regenerate stets
%rng(02132019);%for 0 irrelevant
% rng(04062019); %for 1 irrelevant
%rng(02252019); %for 1-2 irrelevant
%rng(06062019);%for 2 irrelevant
%rng(08082019);%for 0-2 irrelevant

% rng(10112019);%0 irrel probabilistic reward
% rng(10272019);%1 irrelevant probabilistic reward
rng(11042019)%0-1 irrelevant probabilistic reward
%% Block and Trial Cound parmaeters

num_sets_to_generate = 50;
num_blocks = 30;%# of shifts is this minus 1
context_nums = -1; %neutral background = -1

RewardMag = 3;
RewardProb = 0.85;

min_num_trials = [30 35 40 45];
max_num_trials = 60;

%% Main Configurable Parameters for Feature and Dimensions

num_Quaddles_per_trial = 3;%must be at least 2 for 1 Target and 1 distractor
num_feature_values_per_dim = 3; %best to be equal to num_Quaddles_per_trial
num_feature_dimensions = 3;%best if not more than 4, preferably 2 or 3

number_of_irrelevant_feature_dims = [0 0 0 1 1 1];%must be even number, distribution
%of values determines distribution of # of irrelevant features by block
%e.g. if [1 2 1 2 1 2] half trials with have 1 irrelevant feature dim and
%half of trials with have 2 irrelevant features dims

%% Feature Dimensions and values
shapes = {'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

patternless_colors = {'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070287_6070287', 'C6070335_6070335','C6070240_6070240'};
gray_pattern_color = 'C7000000_5000000';

arms = {'A00_E01', 'A00_E02','A00_E03', 'A01_E00', 'A01_E01', 'A01_E02', 'A01_E03', 'A02_E00', 'A02_E01', 'A02_E02', 'A02_E03'};
%% Generate Block Def Files

file_base_name = ['FLU_DF03_limited__' num2str(num_Quaddles_per_trial) 'AFC__' ...
    num2str(num_feature_values_per_dim) 'x' num2str(num_feature_dimensions) ...
    '__' num2str(min(number_of_irrelevant_feature_dims)) '_' num2str(max(number_of_irrelevant_feature_dims))...
    'irrel__Set'];
for set = 1:num_sets_to_generate
       
    %-----Shuffle feature dimensions-----%
    these_dims = 1:4;
    these_dims = these_dims(randperm(4));
    these_dims = these_dims(1:num_feature_dimensions);
    
    %-----Shuffle feature values-----%
    shape_index = randperm(length(shapes));
    shape_index = sort(shape_index(1:num_feature_values_per_dim));
    
    %exclude [atterns 4/5 and 6/7 from being with each other
    all_pattern_index = randperm(length(patterns));
    pattern_index = sort(all_pattern_index(1:num_feature_values_per_dim));
    while (any(pattern_index == 4) && any(pattern_index == 5)) ||...
            (any(pattern_index == 6) && any(pattern_index == 7))
        all_pattern_index = randperm(length(patterns));
        pattern_index = sort(all_pattern_index(1:num_feature_values_per_dim));
    end
    
    color_index = randperm(length(patternless_colors));
    %     if any(color_index == length(patternless_colors))
    %         disp('now')
    %     end
    color_index = sort(color_index(1:num_feature_values_per_dim));
    
    arm_index = randperm(length(arms));
    arm_index = sort(arm_index(1:num_feature_dimensions));
    
    %-----Generate Intra/Extra Dimension Patterns & Irrelevant-----%
    sub_block_len = length(number_of_irrelevant_feature_dims);
    num_sub_blocks = ceil((num_blocks)/sub_block_len);
    all_intra_extra = [];
    all_number_of_irrelevant_feature_dims = [];
    for sb = 1:num_sub_blocks
        intra_extra = [zeros(1,sub_block_len/2) ones(1,sub_block_len/2)];
        intra_extra = intra_extra(randperm(sub_block_len));
        all_intra_extra = [all_intra_extra intra_extra];
        
        num_irrel = number_of_irrelevant_feature_dims;
        num_irrel = num_irrel(randperm(length(num_irrel)));
        
        all_number_of_irrelevant_feature_dims = [all_number_of_irrelevant_feature_dims num_irrel];
    end
    all_intra_extra = all_intra_extra(1:num_blocks);
    all_number_of_irrelevant_feature_dims = all_number_of_irrelevant_feature_dims(1:num_blocks);
    
    %---Generate the BlockDef----%
    filebase = [file_base_name num2str(set)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    dim = [];
    fv = [];
    for b = 1:num_blocks
        %Target Feature Dimension and Feature value
        dims = these_dims(randperm(num_feature_dimensions));
        fvs = randperm(num_feature_values_per_dim);
        if b == 1 %pick a random stimulus for the first set
            dim = dims(1);
            dims = dims(2:end);
            fv = fvs(1);
        elseif all_intra_extra(b) == 0 %intra dimensional shift
            dim = dim; %same as last time
            dims(dims == dim) = [];
            fvs(fvs == fv) = []; %so can't be the same one
            fv = fvs(1);
        else %extra-dimensional shift
            dims(dims == dim) = [];
            dim = dims(randi(num_feature_dimensions-1));
           
            dims = these_dims;
            dims(dims == dim) = [];
            
            fv = fvs(1);
        end

        if length(dims) < max(number_of_irrelevant_feature_dims)
            error('Where did my values go!')
        end
        %---Make Active Array---%
        shape_str = [];
        pattern_str = [];
        color_str = [];
        arm_str = [];
        
        %for target dimension
        if  dim == 1 %shape
            shape_str = ['[' strjoin(arrayfun(@(x) num2str(x),shape_index,'UniformOutput',false),',') ']'];
        elseif dim == 2 %pattern
            pattern_str = ['[' strjoin(arrayfun(@(x) num2str(x),pattern_index,'UniformOutput',false),',') ']'];
        elseif dim == 3 %color
            color_str = ['[' strjoin(arrayfun(@(x) num2str(x),color_index,'UniformOutput',false),',') ']'];
        elseif dim == 4 %arms
            arm_str = ['[' strjoin(arrayfun(@(x) num2str(x),arm_index,'UniformOutput',false),',') ']'];
        else
            error('Stimulus not recognized')
        end
        
        %for distractor(s) dimensions
        for dd = 1:all_number_of_irrelevant_feature_dims(b)
            if  dims(dd) == 1 %shape
                shape_str = ['[' strjoin(arrayfun(@(x) num2str(x),shape_index,'UniformOutput',false),',') ']'];
            elseif dims(dd) == 2 %pattern
                pattern_str = ['[' strjoin(arrayfun(@(x) num2str(x),pattern_index,'UniformOutput',false),',') ']'];
            elseif dims(dd) == 3 %color
                color_str = ['[' strjoin(arrayfun(@(x) num2str(x),color_index,'UniformOutput',false),',') ']'];
            elseif dims(dd) == 4 %arms
                arm_str = ['[' strjoin(arrayfun(@(x) num2str(x),arm_index,'UniformOutput',false),',') ']'];
            else
                error('Stimulus not recognized')
            end
        end
        
        if isempty(shape_str)
            shape_str = '[0]';
        end
        if isempty(pattern_str)
            pattern_str = '[0]';
        end
        if isempty(color_str)
            color_str = '[0]';
        end
        if isempty(arm_str)
            arm_str = '[0]';
        end
        
        active_array = [ '[' shape_str ',' pattern_str ',' color_str ',[0],' arm_str ']' ];
        
        %---Make Rule Array---%
        %since stimuli are chosen at random jsut set 1st one as rewarded
        if dim == 1 %shape
            rule_array = ['[' num2str(shape_index(fv)) '],[-1],[-1],[-1],[-1]'];%since stimuli are chosen at random jsut set 1st one as rewarded
        elseif dim == 2 %pattern
            rule_array = ['[-1],[' num2str(pattern_index(fv)) '],[-1],[-1],[-1]'];%since stimuli are chosen at random jsut set 1st one as rewarded
        elseif dim == 3 %color
            rule_array = ['[-1],[-1],[' num2str(color_index(fv)) '],[-1],[-1]'];
        elseif dim == 4 %amrs
            rule_array = ['[-1],[-1],[-1],[-1],[' num2str(arm_index(fv)) ']'];
        else
            error('Stimulus not recognized')
        end
        
            
        %Detemrine number of trials in Block
        num_trials1 = min_num_trials(randi(length(min_num_trials)));
        num_trials2 = max_num_trials;
        
        %Determine Block name...useful for post processing
        if b == 1
            block_name = [filebase(1:12) '_B' num2str(b) '_Set' num2str(set) '_First_' ...
                num2str(all_number_of_irrelevant_feature_dims(b)) '_irrel'];
        else
            if all_intra_extra(b) == 1 %extra dimensional shift
                block_name = [filebase(1:12) '_B' num2str(b) '_Extra_' ...
                num2str(all_number_of_irrelevant_feature_dims(b)) '_irrel'];
            else %intra-dimensional shift
                block_name = [filebase(1:12) '_B' num2str(b) '_Intra_' ...
                num2str(all_number_of_irrelevant_feature_dims(b)) '_irrel'];
            end
        end
              
        
        bdstr2 = ['{"BlockID": "blockid.' block_name '",' ...
            ' "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '],' ...
            ' "ActiveFeatureTemplate": ' active_array ',' ...
            ' "ContextNums": [' num2str(context_nums) '],' ...
            ' "NumRelevantObjectsPerTrial": [' num2str(num_Quaddles_per_trial) ',' num2str(num_Quaddles_per_trial) '], ' ...
            '"NumIrrelevantObjectsPerTrial": [0,0] ,'...
            ' "NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [' rule_array '], "ContextNums": [' num2str(context_nums) '], ' ...
            '"RewardProb": ' num2str(RewardProb) ', "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": ' num2str(1-RewardProb) ', "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if b == num_blocks
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
    end
    fclose(blockfID);
end
fclose all
cd(current_dir)