%Written by Seth Koenig February 25, 2019
%SDK 4/6/19 originally didn't have C7070240_5000000 included,added but didn't re-generate
clear,clc,close all

%seed random number generator, do not change EVER &...
%always run file from the start and generate all files
%rng(20190225); %for 1D, aka DF02
%rng(20190226); %for 2D
rng(20190227); %for 3D

current_dir = pwd;
%% Stim Path Parameters

%Example Paths for Dual-Drive Computer
stim_path = 'D:\\QuaddleRepository_20180515\\';
itemCND_path = 'D:\\\\BlockDefnitions\\\\Feature_Search_Training\\\\';

%Example Paths for Kiosk Computers with 1 drive
%%stim_path = 'C:\\Users\\Womelsdorf Lab\\Documents\\Quaddles\\TexturelessQuaddleRepo\\';
%%itemCND_path = 'C:\\\\Users\\\\Womelsdorf Lab\\\\Documents\\\\BlockDefinitions\\\\Feature_Search_Training\\\\';


%% Block and Trial Cound parmaeters
number_of_dimensions = 3;

num_sets_to_generate = 10;
context_nums = 0:4;

RewardMag = 2;
RewardProb = 1.0;

num_distractor_Quaddles = [0 1 3 5 9 12];

%incraesed minimums for more practice & familiarization
fam_num_trials = 20;
min_num_trials = 60;
max_num_trials = 100;

psuedo_random_block_size = 5; %for context balancing
%% Quaddle Spacing-Generate All possible locations

spatial_scale = 0.05;
y_default = 0.4;
x_spacing = 4;%also exlucde within 2 units of center, arms make us need more spacing on x-axis
z_spacing = 3;%also exlucde within 1.5 units of center
x_max = 8;%magnitude
z_max = 4.5;%magnitude

xzlocs = [];
count = 0;
for x = -x_max:x_spacing:x_max
    for z = -z_max:z_spacing:z_max
        if x == 0 && z == 0
            continue
        else
            count = count+1;
            xzlocs(1,count) = x;
            xzlocs(2,count) = z;
        end
    end
end

%% Feature Dimensions and values
shapes = {'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

patternless_colors = {'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070287_6070287', 'C6070335_6070335','C6070240_6070240'};
patterned_colors =   {'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000', 'C7070286_5000000', 'C7070335_5000000','C7070240_5000000'};
gray_pattern_color = 'C7000000_5000000';

arms = {'A00_E01', 'A00_E02','A00_E03', 'A01_E00', 'A02_E00'}; %limit because background makes it tougher to discriminate 
%may want to add more back in when monkeys get better at the task

%% Generate ITM and CND Headers which are always the same for all files

itm_header=['StimCode' '\t' 'StimName' '\t' 'StimPath' '\t' ...
    'StimDimVals1' '\t' 'StimDimVals2' '\t' 'StimDimVals3' '\t' 'StimDimVals4' '\t' 'StimDimVals5' '\t' ...
    'StimLocationX' '\t' 'StimLocationY' '\t' 'StimLocationZ' '\t' ...
    'StimRotationX' '\t' 'StimRotationY' '\t' 'StimRotationZ' '\t' ...
    'StimTrialRewardProb' '\t' 'StimTrialRewardMag' '\t' 'TimesUsedInBlock' ...
    '\t' 'isRelevant' '\t' 'SetLocation' '\t' 'SetRotation' '\n'];

cnd_header=['TrialName' '\t' 'TrialCode' '\t' 'ContextNum' '\t' 'ContextName' '\t' ...
    'Stim1' '\t'  'Stim2' '\t'  'Stim3' '\t'  'Stim4' '\t'  'Stim5' '\t'  'Stim6' '\t' ...
    'Stim7' '\t'  'Stim8' '\t'  'Stim9' '\t'  'Stim10' '\t'  'Stim11' '\t'  'Stim12' '\t' ...
    'Stim13' '\t'  'Stim14' '\n'];

%% Generate Context Sets

neutral = 'S00_P00_C6000000_6000000_T00_A00_E00.fbx';
neutral_full_path = [Determine_Quaddle_Path(neutral,stim_path,false) neutral];

%DF is for the diffculty mode, D stands for number of dimensions
if number_of_dimensions == 1
    file_base_name = ['CFS_DF02_' num2str(number_of_dimensions) 'D_Set'];
else
    file_base_name = ['CFS_DF03_' num2str(number_of_dimensions) 'D_Set'];
end
for file = 1:num_sets_to_generate
    
    %---Pick 2 contexts---%
    these_context_nums = context_nums(randperm(length(context_nums)));
    these_context_nums = these_context_nums(1:2);
    
    %---Pick N-Dimensions---%
    these_dims = 1:4;
    these_dims = these_dims(randperm(4));
    these_dims = sort(these_dims(1:number_of_dimensions));

    %-----Shuffle feature values-----%
    shape_index = randperm(length(shapes));
    shape_index = sort(shape_index(1:2));
    
    %exclude [atterns 4/5 and 6/7 from being with each other
    all_pattern_index = randperm(length(patterns));
    pattern_index = sort(all_pattern_index(1:2));
    while (any(pattern_index == 4) && any(pattern_index == 5)) ||...
            (any(pattern_index == 6) && any(pattern_index == 7))
        all_pattern_index = randperm(length(patterns));
        pattern_index = sort(all_pattern_index(1:2));
    end
    
    %exclude colors right next to each other because background interfers a bit in color discrimnation
    color_index = randperm(length(patternless_colors));
    while abs(color_index(1)-color_index(2)) == 1 || abs(color_index(1)-color_index(2)) == length(color_index)-1
        color_index = randperm(length(patternless_colors));
    end
    color_index = sort(color_index(1:2));
    
    
    arm_index = randperm(length(arms));
    arm_index = sort(arm_index(1:2));
    
    %---Generate All Feature Combinations---%
    %note indexing and math here is a little convoluted...sorry
    if number_of_dimensions == 3
        %all single feature combos
        C1 = combnk([these_dims 10*these_dims],1);
        
        %all double feature combos
        C2 = combnk([these_dims 10*these_dims],2);
        C2(C2(:,1)*10 == C2(:,2),:) = [];
        
        %all tripple feature combos
        C3 = combnk([these_dims 10*these_dims],3);
        C3(C3(:,1)*10 == C3(:,2),:) = [];
        C3(C3(:,2)*10 == C3(:,3),:) = [];
        C3(C3(:,1)*10 == C3(:,3),:) = [];
    elseif number_of_dimensions == 2
        %all single feature combos
        C1 = combnk([these_dims 10*these_dims],1);
        
        %all double feature combos
        C2 = combnk([these_dims 10*these_dims],2);
        C2(C2(:,1)*10 == C2(:,2),:) = [];
        
        C3 = [];
    elseif number_of_dimensions == 1
        C1 = combnk([these_dims -these_dims],1); %pontless but just to keep them together
        C2 = [];
        C3 = [];
    end
    
    %---Get Quaddle Names, Paths, and Feature Dimensions---%
    max_num = max([length(C1) length(C2) length(C3)]);
    targets_and_distractors = cell(3,max_num);
    targets_and_distractors_full_paths =  cell(3,max_num);
    target_and_distractors_dimVals = cell(3,max_num);
    for c = 1:size(C3,1)
        dim_vals = NaN(1,4);
        for v = 1:size(C3,2)
            cdim = C3(c,v);
            if cdim >= 10
                cdim = cdim/10;
            end
            if cdim == min(these_dims)
                current_dim = 1;
            elseif cdim == max(these_dims)
                current_dim = 3;
            else
                current_dim = 2;
            end
            if C3(c,v) >= 10 %dimensional index
                dim_index = 2;
            else
                dim_index = 1;
            end
            dim_vals(these_dims(current_dim)) = dim_index;
        end
        if ~isnan(dim_vals(1)) %shapes
            Quaddle_name = [shapes{shape_index(dim_vals(1))} '_'];
        else
            Quaddle_name = 'S00_';
        end
        if ~isnan(dim_vals(2)) %patterns
            Quaddle_name = [Quaddle_name patterns{pattern_index(dim_vals(2))} '_'];
        else
            Quaddle_name = [Quaddle_name 'P00_'];
        end
        if ~isnan(dim_vals(3)) %colors
            if isnan(dim_vals(2)) %patternless
                Quaddle_name = [Quaddle_name patternless_colors{color_index(dim_vals(3))} '_T00_'];
            else
                Quaddle_name = [Quaddle_name  patterned_colors{color_index(dim_vals(3))} '_T00_'];
            end
        else
            if isnan(dim_vals(2)) %patternless
                Quaddle_name = [Quaddle_name 'C6000000_6000000_T00_'];
            else
                Quaddle_name = [Quaddle_name 'C7000000_5000000_T00_'];
            end
        end
        if ~isnan(dim_vals(4))
            Quaddle_name = [Quaddle_name arms{arm_index(dim_vals(4))} '.fbx'];
        else
            Quaddle_name = [Quaddle_name 'A00_E00.fbx'];
        end
        targets_and_distractors{3,c} = Quaddle_name;
        targets_and_distractors_full_paths{3,c} = [Determine_Quaddle_Path(Quaddle_name,stim_path,false) Quaddle_name];
        [target_and_distractors_dimVals{3,c},~] = ParseQuaddleName(Quaddle_name,patternless_colors,arms);
        if  target_and_distractors_dimVals{3,c}(2) ~= 0
            [target_and_distractors_dimVals{3,c},~] = ParseQuaddleName(Quaddle_name,patterned_colors,arms);
        end
    end
    for c = 1:size(C2,1)
        dim_vals = NaN(1,4);
        for v = 1:size(C2,2)
            cdim = C2(c,v);
            if cdim >= 10
                cdim = cdim/10;
            end
            if cdim == min(these_dims)
                current_dim = 1;
            elseif cdim == max(these_dims)
                current_dim = length(these_dims);
            else
                current_dim = 2;
            end
            if C2(c,v) >= 10 %dimensional index
                dim_index = 2;
            else
                dim_index = 1;
            end
            dim_vals(these_dims(current_dim)) = dim_index;
        end
        if ~isnan(dim_vals(1)) %shapes
            Quaddle_name = [shapes{shape_index(dim_vals(1))} '_'];
        else
            Quaddle_name = 'S00_';
        end
        if ~isnan(dim_vals(2)) %patterns
            Quaddle_name = [Quaddle_name patterns{pattern_index(dim_vals(2))} '_'];
        else
            Quaddle_name = [Quaddle_name 'P00_'];
        end
        if ~isnan(dim_vals(3)) %colors
            if isnan(dim_vals(2)) %patternless
                Quaddle_name = [Quaddle_name patternless_colors{color_index(dim_vals(3))} '_T00_'];
            else
                Quaddle_name = [Quaddle_name  patterned_colors{color_index(dim_vals(3))} '_T00_'];
            end
        else
            if isnan(dim_vals(2)) %patternless
                Quaddle_name = [Quaddle_name 'C6000000_6000000_T00_'];
            else
                Quaddle_name = [Quaddle_name 'C7000000_5000000_T00_'];
            end
        end
        if ~isnan(dim_vals(4))
            Quaddle_name = [Quaddle_name arms{arm_index(dim_vals(4))} '.fbx'];
        else
            Quaddle_name = [Quaddle_name 'A00_E00.fbx'];
        end
        targets_and_distractors{2,c} = Quaddle_name;
        targets_and_distractors_full_paths{2,c} = [Determine_Quaddle_Path(Quaddle_name,stim_path,false) Quaddle_name];
        [ target_and_distractors_dimVals{2,c},~] = ParseQuaddleName(Quaddle_name,patternless_colors,arms);
        if  target_and_distractors_dimVals{2,c}(2) ~= 0
            [target_and_distractors_dimVals{2,c},~] = ParseQuaddleName(Quaddle_name,patterned_colors,arms);
        end
    end
    for c = 1:size(C1,1)
        dim_vals = NaN(1,4);
        v = 1;
        cdim = C1(c,v);
        if cdim >= 10
            cdim = cdim/10; 
        elseif cdim < 0
           cdim = abs(cdim);
        end
        if cdim == min(these_dims)
            current_dim = 1;
        elseif cdim == max(these_dims)
            current_dim = length(these_dims);
        else
            current_dim = 2;
        end
        if C1(c,v) >= 10 %dimensional index
            dim_index = 2;
        elseif C1(c,v) < 0 %will only happen in single dimension case
            dim_index = 2;
        else
            dim_index = 1;
        end
        dim_vals(these_dims(current_dim)) = dim_index;
        if ~isnan(dim_vals(1)) %shapes
            Quaddle_name = [shapes{shape_index(dim_vals(1))} '_'];
        else
            Quaddle_name = 'S00_';
        end
        if ~isnan(dim_vals(2)) %patterns
            Quaddle_name = [Quaddle_name patterns{pattern_index(dim_vals(2))} '_'];
        else
            Quaddle_name = [Quaddle_name 'P00_'];
        end
        if ~isnan(dim_vals(3)) %colors
            if isnan(dim_vals(2)) %patternless
                Quaddle_name = [Quaddle_name patternless_colors{color_index(dim_vals(3))} '_T00_'];
            else
                Quaddle_name = [Quaddle_name  patterned_colors{color_index(dim_vals(3))} '_T00_'];
            end
        else
            if isnan(dim_vals(2)) %patternless
                Quaddle_name = [Quaddle_name 'C6000000_6000000_T00_'];
            else
                Quaddle_name = [Quaddle_name 'C7000000_5000000_T00_'];
            end
        end
        if ~isnan(dim_vals(4))
            Quaddle_name = [Quaddle_name arms{arm_index(dim_vals(4))} '.fbx'];
        else
            Quaddle_name = [Quaddle_name 'A00_E00.fbx'];
        end
        targets_and_distractors{1,c} = Quaddle_name;
        targets_and_distractors_full_paths{1,c} = [Determine_Quaddle_Path(Quaddle_name,stim_path,false) Quaddle_name];
        [ target_and_distractors_dimVals{1,c},~] = ParseQuaddleName(Quaddle_name,patternless_colors,arms);
        if  target_and_distractors_dimVals{1,c}(2) ~= 0
            [target_and_distractors_dimVals{1,c},~] = ParseQuaddleName(Quaddle_name,patterned_colors,arms);
        end
    end
    
    %1 for context 1 and 2 for context!
    rewarded = zeros(size(targets_and_distractors));
    if number_of_dimensions == 3
        rewarded(3,1) = 1;
        rewarded(3,8) = 2;
    elseif  number_of_dimensions == 2
        rewarded(2,2) = 1;
        rewarded(2,3) = 2;
    elseif number_of_dimensions == 1
        rewarded(1,1) = 1;
        rewarded(1,2) = 2;
    end
    
    %---stretch out into linear arrays---%
    targets_and_distractors = targets_and_distractors(:);
    targets_and_distractors_full_paths = targets_and_distractors_full_paths(:);
    target_and_distractors_dimVals = target_and_distractors_dimVals(:);
    rewarded = rewarded(:);
    
    %---Add Neutral Quaddle to list of possible distractors---%
    targets_and_distractors = [targets_and_distractors; {neutral}];
    targets_and_distractors_full_paths = [targets_and_distractors_full_paths; {neutral_full_path}];
    target_and_distractors_dimVals = [target_and_distractors_dimVals; {zeros(1,5)}];
    rewarded = [rewarded; 0];
    
    %---First Generate the BlockDef----%
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    %---Second Generate the Item and Condition files for each Block----%
    for numD = 1:length(num_distractor_Quaddles)
        
        if numD == 1%for fam block
            num_trials1 = fam_num_trials;
            num_trials2 = min_num_trials;
        elseif numD  == length(num_distractor_Quaddles)%for last block do lots of trials so won't finish block before stop working
            num_trials1 = 4*max_num_trials;
            num_trials2 = 4*max_num_trials;
        else
            num_trials1 = min_num_trials;
            num_trials2 = max_num_trials;
        end
        
        blockpath = [itemCND_path filebase '\\\\' filebase '_D' num2str(num_distractor_Quaddles(numD))];
        bdstr2 = ['{"BlockID": "blockid.' [filebase '_D' num2str(num_distractor_Quaddles(numD))] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if numD == length(num_distractor_Quaddles)
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        
        %-----Generate Item File---%
        fileIDitm = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_Itm.txt'],'w');
        fprintf(fileIDitm,itm_header);
        item_num = 0;
        item_locc_index = NaN(length(targets_and_distractors),length(xzlocs),2);
        rewarded_item_locc_index = NaN(length(targets_and_distractors),length(xzlocs),2);
        for contxt = 1:2
            for Q = 1:length(targets_and_distractors)
                if isempty(targets_and_distractors{Q})
                    continue
                end
                for locs = 1:length(xzlocs)
                    item_num = item_num+1;
                    item_locc_index(Q,locs,contxt) = item_num;
                    if contxt == 1 && rewarded(Q) == 1 || contxt == 2 && rewarded(Q) == 2
                        rewarded_item_locc_index(Q,locs,contxt) = contxt;
                        str = [num2str(item_num) '\t' targets_and_distractors{Q} '\t' targets_and_distractors_full_paths{Q} '\t' ...
                            num2str(target_and_distractors_dimVals{Q}(1)) '\t' num2str(target_and_distractors_dimVals{Q}(2)) '\t' ...
                            num2str(target_and_distractors_dimVals{Q}(3)) '\t' num2str(target_and_distractors_dimVals{Q}(4)) '\t' ...
                            num2str(target_and_distractors_dimVals{Q}(5)) '\t' ...
                            num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                            '80' '\t' '10' '\t' '10' '\t' ...
                            num2str(RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                            'true' '\t' 'true' '\t' 'true' '\n'];
                    else
                        rewarded_item_locc_index(Q,locs,contxt) = 0;
                        str = [num2str(item_num) '\t' targets_and_distractors{Q} '\t' targets_and_distractors_full_paths{Q} '\t' ...
                            num2str(target_and_distractors_dimVals{Q}(1)) '\t' num2str(target_and_distractors_dimVals{Q}(2)) '\t' ...
                            num2str(target_and_distractors_dimVals{Q}(3)) '\t' num2str(target_and_distractors_dimVals{Q}(4)) '\t' ...
                            num2str(target_and_distractors_dimVals{Q}(5)) '\t' ...
                            num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                            '80' '\t' '10' '\t' '10' '\t' ...
                            num2str(1-RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                            'true' '\t' 'true' '\t' 'true' '\n'];
                    end
                    fprintf(fileIDitm,str);
                end
            end
        end
        fclose(fileIDitm);
        
        %----Generate Condition file-----%
        fileIDcnd = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_CND.txt'],'w');
        fprintf(fileIDcnd,cnd_header);
        
        num_p_blocks = ceil(num_trials2/psuedo_random_block_size/2);%pseudorandomize doesn't have to be perfect but should be good
        context_dist = [];
        for npb = 1:num_p_blocks
            this_dist = [ones(1,psuedo_random_block_size) 2*ones(1,psuedo_random_block_size)];
            this_dist = this_dist(randperm(2*psuedo_random_block_size));
            context_dist = [context_dist this_dist];
        end
        
        for t = 1:num_trials2
            current_context = context_dist(t);
            item_index = randperm(length(xzlocs));%randomize location index
            
            if current_context == 1
                target_row = find(rewarded_item_locc_index(:,1,1) == current_context);
            elseif current_context == 2
                target_row = find(rewarded_item_locc_index(:,1,2) == current_context);
            end
            distractors = 1:size(rewarded_item_locc_index,1);
            distractors(isnan(rewarded_item_locc_index(:,1,current_context))) = [];
            distractors(distractors == target_row) = [];
            
            %make more distractors than have if it's too little
            all_distractors = [];
            for di = 1:ceil(num_distractor_Quaddles(numD)/length(distractors))
                all_distractors = [all_distractors distractors];
            end
            all_distractors = all_distractors(randperm(length(all_distractors)));
            
            %make trial and context strings
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(these_context_nums(current_context)) '\t'  num2str(these_context_nums(current_context))  '\t'];
            
            %make target string
            str3 = num2str(item_locc_index(target_row,item_index(1),current_context));
            if num_distractor_Quaddles(numD) > 0
                str3 = [str3 '\t'];
            end
            
            %make distractor string
            str4 = [];
            for stim = 1:num_distractor_Quaddles(numD)
                str4= [str4  num2str(item_locc_index(all_distractors(stim),item_index(stim+1),current_context))];
                if stim ~= num_distractor_Quaddles(numD)
                    str4 = [str4 '\t'];
                end
            end
            fprintf(fileIDcnd,[str1 str2 str3  str4 '\n']);
            
        end
        fclose(fileIDcnd);
    end
    fclose(blockfID);
end

fclose all