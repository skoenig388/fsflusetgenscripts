function [all_Quaddle_names,all_Quaddle_dimVals,all_Quaddle_fullPath] = ...
    feature_combos2_QuaddleNames(all_combos,active_feature_dims,Quaddle_path,shapes,patterns,...
    patterned_colors,patternless_colors,arms,dimensional_indexes)
%Seth Koenig 6/21/19
%Commonly to create all possible combinations of features, I use the same
%few lines of code across multiple tasks, so useful to convert them back
%into Quaddle names.

%---All_Combos_Key---%
%integer value is the dimension, and power of 10 is the index of
%that dimension: e.g. 200 means 3 index in 2nd dimension

%---decrompess chosen dimensional indexes---%
shape_index = dimensional_indexes(1,:);
pattern_index = dimensional_indexes(2,:);
color_index = dimensional_indexes(3,:);
arm_index = dimensional_indexes(4,:);

all_Quaddle_names = cell(1,size(all_combos,1));%file name of Quaddle
all_Quaddle_fullPath =  cell(1,size(all_combos,1));%file name + path of Quaddle
all_Quaddle_dimVals = cell(1,size(all_combos,1));%Dimension values, 0 is neutral

for c = 1:size(all_combos,1)
    dim_vals = NaN(1,4);
    for v = 1:size(all_combos,2)
        cdim = all_combos(c,v);
        if cdim >= 100
            cdim = cdim/100;
        elseif cdim >= 10
            cdim = cdim/10;
        end
        if cdim == min(active_feature_dims)
            current_dim = 1;
        elseif cdim == max(active_feature_dims)
            current_dim = 3;
        else
            current_dim = 2;
        end
        if all_combos(c,v) >= 100  %dimensional index
            dim_index = 3;
        elseif all_combos(c,v) >= 10
            dim_index = 2;
        else
            dim_index = 1;
        end
        dim_vals(active_feature_dims(current_dim)) = dim_index;
    end
    if ~isnan(dim_vals(1)) %shapes
        Quaddle_name = [shapes{shape_index(dim_vals(1))} '_'];
    else
        Quaddle_name = 'S00_';
    end
    if ~isnan(dim_vals(2)) %patterns
        Quaddle_name = [Quaddle_name patterns{pattern_index(dim_vals(2))} '_'];
    else
        Quaddle_name = [Quaddle_name 'P00_'];
    end
    if ~isnan(dim_vals(3)) %colors
        if isnan(dim_vals(2)) %patternless
            Quaddle_name = [Quaddle_name patternless_colors{color_index(dim_vals(3))} '_T00_'];
        else
            Quaddle_name = [Quaddle_name  patterned_colors{color_index(dim_vals(3))} '_T00_'];
        end
    else
        if isnan(dim_vals(2)) %patternless
            Quaddle_name = [Quaddle_name 'C7000000_7000000_T00_'];
        else
            Quaddle_name = [Quaddle_name 'C7000000_5000000_T00_'];
        end
    end
    if ~isnan(dim_vals(4))
        Quaddle_name = [Quaddle_name arms{arm_index(dim_vals(4))} '.fbx'];
    else
        Quaddle_name = [Quaddle_name 'A00_E00.fbx'];
    end
    all_Quaddle_names{c} = Quaddle_name;
    all_Quaddle_fullPath{c} = [Determine_Quaddle_Path(Quaddle_name,Quaddle_path,false) Quaddle_name];
    [all_Quaddle_dimVals{c},~] = ParseQuaddleName(Quaddle_name,patternless_colors,arms);
    if  all_Quaddle_dimVals{c}(2) ~= 0
        [all_Quaddle_dimVals{c},~] = ParseQuaddleName(Quaddle_name,patterned_colors,arms);
    end
end

end