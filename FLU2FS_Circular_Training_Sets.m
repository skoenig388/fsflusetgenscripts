%Modified from FLU2FS_Training_Sets on 6/19/19 to implement non-grid search
%with all of the search locations on a concentric circle; "unused"  locations
%are filled with neutral Quaddles as space fillers. All other code is nearly
%identical.

%Code generates super blocks: 1 block of FLU task followed by 1 familiarization
%block followed by 1 block of FS; all trials within a super block have the
% same target feature values.
%%
clear,clc,close all

current_dir = pwd;
%% Stim Path Parameters

%Example Paths for Dual-Drive Computer
stim_path = 'D:\\TextureLessQuaddleRepo_05032019\\';
itemCND_path = 'D:\\\\BlockDefnitions\\\\FLU2FS_Training\\\\';

%Example Paths for Kiosk Computers with 1 drive
%%stim_path = 'C:\\Users\\Womelsdorf Lab\\Documents\\Quaddles\\TexturelessQuaddleRepo\\';
%%itemCND_path = 'C:\\\\Users\\\\Womelsdorf Lab\\\\Documents\\\\BlockDefinitions\\\\Feature_Search_Training\\\\';


%% General Task Parameters
rng(06192019);%for task with context & fam trials

RewardMag = 2;
RewardProb = 1.0;

num_sets_to_generate = 50;
num_blocks = 30;%# of super blocks (FLU + Fam + FS)

add_re_familiarization_block = true;%if true adds a # of refamilaization trials in which only target feature value
%is shown to reinforce that the rule hasn't changed yet
num_fam_trials = 3;

using_contexts = true;%if true, blocks alterante context backgrounds to cue rule change
context_nums = 0:4;
bad_context_colors = [NaN 3 4 8 1];%colors that may be harder to discriminate given the background
%gravel, sand, grass, ice, brick

%% Main Configurable Parameters for FLU trials

num_Quaddles_per_trial = 3;%must be at least 2 for 1 Target and 1 distractor
num_feature_values_per_dim = 3; %best to be equal to num_Quaddles_per_trial
num_feature_dimensions = 3;%best if not more than 4, preferably 2 or 3

max_num_trials = 60;

min_num_trials = [25 30 35 25 30 35 25 30 35];

number_of_irrelevant_feature_dims = [0 0 0 1 1 1 2 2 2];%should be same length
%as the min_num_trials. distribution of values determines distribution of #
%of irrelevant features by block.
%% Main Configurable Parameters for FS trials

%for 8 ciruclar positions
num_Distractors = [3 5 7];%does not include target

num_search_reps = 3;%number of times to do each # distractor
num_search_trials = length(num_Distractors)*num_search_reps;
%% Feature Dimensions and values
shapes = {'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

patterned_colors =   {'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000', 'C7070286_5000000', 'C7070335_5000000','C7070240_5000000'};
%patternless_colors = {'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070287_6070287', 'C6070335_6070335','C6070239_6070239'};
patternless_colors = {'C7070014_7070014', 'C7070059_7070059', 'C7070106_7070106', 'C7070148_7070148', 'C7070194_7070194', 'C7070286_7070286', 'C7070335_7070335','C7070240_7070240'};
gray_pattern_color = 'C7000000_5000000';

arms = {'A00_E01', 'A00_E02','A00_E03', 'A01_E00', 'A01_E01', 'A01_E02', 'A01_E03', 'A02_E00', 'A02_E01', 'A02_E02', 'A02_E03'};

neutral_Quaddle = 'S00_P00_C7000000_7000000_T00_A00_E00';
%% Quaddle Spacing-Generate All possible locations
%this is the big difference from original version

y_default = 0.4;

radius = 5.65;
angles = 0:45:365-45;

xzlocs = NaN(2,length(angles));
for an = 1:length(angles)
    xzlocs(1,an) = round(radius*cosd(angles(an)),2);
    xzlocs(2,an) = round(radius*sind(angles(an)),2);
end
%% Generate ITM and CND Headers which are always the same for all files

itm_header=['StimCode' '\t' 'StimName' '\t' 'StimPath' '\t' ...
    'StimDimVals1' '\t' 'StimDimVals2' '\t' 'StimDimVals3' '\t' 'StimDimVals4' '\t' 'StimDimVals5' '\t' ...
    'StimLocationX' '\t' 'StimLocationY' '\t' 'StimLocationZ' '\t' ...
    'StimRotationX' '\t' 'StimRotationY' '\t' 'StimRotationZ' '\t' ...
    'StimTrialRewardProb' '\t' 'StimTrialRewardMag' '\t' 'TimesUsedInBlock' ...
    '\t' 'isRelevant' '\t' 'SetLocation' '\t' 'SetRotation' '\n'];

cnd_header=['TrialName' '\t' 'TrialCode' '\t' 'ContextNum' '\t' 'ContextName' '\t' ...
    'Stim1' '\t'  'Stim2' '\t'  'Stim3' '\t'  'Stim4' '\t'  'Stim5' '\t'  'Stim6' '\t' ...
    'Stim7' '\t'  'Stim8' '\t'  'Stim9' '\t'  'Stim10' '\t'  'Stim11' '\t'  'Stim12' '\t' ...
    'Stim13' '\t'  'Stim14' '\n'];

%% Generate Block Def Files

file_base_name_str =  ['FLU2FS_v2__' num2str(num_Quaddles_per_trial) 'AFC__' ...
    num2str(num_feature_values_per_dim) 'x' num2str(num_feature_dimensions) ...
    '__' num2str(min(number_of_irrelevant_feature_dims)) '_' num2str(max(number_of_irrelevant_feature_dims))...
    'irrel'];

if add_re_familiarization_block
    file_base_name_fam_str = ['__Fam' num2str(num_fam_trials)];
else
    file_base_name_fam_str = [];
end

file_base_name = [file_base_name_str file_base_name_fam_str '__Set'];

%%
for set = 1:num_sets_to_generate
    filebase = [file_base_name num2str(set)];
    
    %---Pick 2 contexts---%
    these_context_nums = context_nums(randperm(length(context_nums)));
    these_context_nums = these_context_nums(1:2);
    
    %-----Shuffle feature dimensions-----%
    these_dims = 1:4;
    these_dims = these_dims(randperm(4));
    these_dims = these_dims(1:num_feature_dimensions);
    these_dims = sort(these_dims);
    
    %-----Shuffle feature values-----%
    shape_index = randperm(length(shapes));
    shape_index = sort(shape_index(1:num_feature_values_per_dim));
    
    %exclude [patterns 4/5 and 6/7 from being with each other
    all_pattern_index = randperm(length(patterns));
    pattern_index = sort(all_pattern_index(1:num_feature_values_per_dim));
    while (any(pattern_index == 4) && any(pattern_index == 5)) ||...
            (any(pattern_index == 6) && any(pattern_index == 7))
        all_pattern_index = randperm(length(patterns));
        pattern_index = sort(all_pattern_index(1:num_feature_values_per_dim));
    end
    
    if using_contexts
        these_colors = 1:length(patternless_colors);
        %remove "bad" colors
        for con = 1:2
            if ~isnan(bad_context_colors(these_context_nums(con)+1))
                these_colors(these_colors == bad_context_colors(these_context_nums(con))) = [];
            end
        end
        tryagain = true;
        while tryagain
            color_index = randperm(length(patternless_colors));
            selected_colors = color_index(1:num_feature_values_per_dim);
            if all(pdist(color_index(1:num_feature_values_per_dim)') > 1)
                %color map is circular so check for 1 next to 8
                if ~any(selected_colors == 1 | selected_colors == 8)
                    tryagain = false;
                    color_index = sort(color_index(1:num_feature_values_per_dim));
                elseif (any(selected_colors == 1) && ~any(selected_colors == 8)) || ...
                        (any(selected_colors == 8) && ~any(selected_colors == 1))
                    tryagain = false;
                    color_index = sort(color_index(1:num_feature_values_per_dim));
                end
            end
        end
    else
        color_index = randperm(length(patternless_colors));
        color_index = sort(color_index(1:num_feature_values_per_dim));
    end
    
    arm_index = randperm(length(arms));
    arm_index = sort(arm_index(1:num_feature_dimensions));
    
    %-----Generate Intra/Extra Dimension Patterns & # Irrelevant Dims-----%
    sub_block_len = length(number_of_irrelevant_feature_dims);
    num_sub_blocks = ceil((num_blocks)/sub_block_len);
    all_intra_extra = [];
    all_number_of_irrelevant_feature_dims = [];
    all_number_min_trials = [];
    for sb = 1:num_sub_blocks
        %not a perfect balance if do all blocks but close over shorter # of blocks
        intra_extra = [zeros(1,ceil(sub_block_len/2)) ones(1,ceil(sub_block_len/2))];
        intra_extra = intra_extra(randperm(sub_block_len));
        all_intra_extra = [all_intra_extra intra_extra];
        
        %get shuffled in same order to make sure that there are equal # of
        %min # trials for each # irrel
        rand_ind = randperm(sub_block_len);
        num_irrel = number_of_irrelevant_feature_dims(rand_ind);
        num_min_trials = min_num_trials(rand_ind);
        all_number_of_irrelevant_feature_dims = [all_number_of_irrelevant_feature_dims num_irrel];
        all_number_min_trials = [all_number_min_trials num_min_trials];
    end
    all_intra_extra = all_intra_extra(1:num_blocks);
    all_number_of_irrelevant_feature_dims = all_number_of_irrelevant_feature_dims(1:num_blocks);
    all_number_min_trials = all_number_min_trials(1:num_blocks);
    
    %---Deterime Target Dimension and Value----%
    target_dims_vals = NaN(num_blocks,2);
    for b = 1:num_blocks
        
        dim = [];
        fv = [];
        for b = 1:num_blocks
            %Target Feature Dimension and Feature value
            dims = these_dims(randperm(num_feature_dimensions));
            fvs = randperm(num_feature_values_per_dim);
            if b == 1 %pick a random stimulus for the first set
                dim = dims(1);
                dims = dims(2:end);
                fv = fvs(1);
            elseif all_intra_extra(b) == 0 %intra dimensional shift
                dim = dim; %same as last time
                dims(dims == dim) = [];
                fvs(fvs == fv) = []; %so can't be the same one
                fv = fvs(1);
            else %extra-dimensional shift
                dims(dims == dim) = [];
                dim = dims(randi(num_feature_dimensions-1));
                
                dims = these_dims;
                dims(dims == dim) = [];
                
                fv = fvs(1);
            end
            
            if length(dims) < max(number_of_irrelevant_feature_dims)
                error('Where did my values go!')
            end
            target_dims_vals(b,1) = dim;%target dimension
            target_dims_vals(b,2) =  fv;%;target value
        end
    end
    
    %---Generate All Feature Combinations---%
    %Determine all possible search targets and distractors
    %note indexing and math here is a little convoluted...sorry
    %pick all target/distractors to be maximum dimensionality
    if num_feature_dimensions == 3
        %all tripple feature combos
        all_combos = combnk([these_dims 10*these_dims 100*these_dims],3);
        all_combos(all_combos(:,1)*10 == all_combos(:,2),:) = [];
        all_combos(all_combos(:,1)*100 == all_combos(:,2),:) = [];
        all_combos(all_combos(:,2)*10 == all_combos(:,3),:) = [];
        all_combos(all_combos(:,2)*100 == all_combos(:,3),:) = [];
        all_combos(all_combos(:,1)*10 == all_combos(:,3),:) = [];
        all_combos(all_combos(:,1)*100 == all_combos(:,3),:) = [];
    elseif num_feature_dimensions == 2
        %all double feature combos
        all_combos = combnk([these_dims 10*these_dims 100*these_dims],2);
        all_combos(all_combos(:,1)*10 == all_combos(:,2),:) = [];
        all_combos(all_combos(:,1)*100 == all_combos(:,2),:) = [];
    elseif num_feature_dimensions == 1
        %all single feature combos
        all_combos = combnk([these_dims 10*these_dims 100*these_dims],1);
    end
    
    %---Get Quaddle Names, Paths, and Feature Dimensions---%
    targets_and_distractors = cell(1,size(all_combos,1));
    targets_and_distractors_full_paths =  cell(1,size(all_combos,1));
    target_and_distractors_dimVals = cell(1,size(all_combos,1));
    for c = 1:size(all_combos,1)
        dim_vals = NaN(1,4);
        for v = 1:size(all_combos,2)
            cdim = all_combos(c,v);
            if cdim >= 100
                cdim = cdim/100;
            elseif cdim >= 10
                cdim = cdim/10;
            end
            if cdim == min(these_dims)
                current_dim = 1;
            elseif cdim == max(these_dims)
                current_dim = 3;
            else
                current_dim = 2;
            end
            if all_combos(c,v) >= 100  %dimensional index
                dim_index = 3;
            elseif all_combos(c,v) >= 10
                dim_index = 2;
            else
                dim_index = 1;
            end
            dim_vals(these_dims(current_dim)) = dim_index;
        end
        if ~isnan(dim_vals(1)) %shapes
            Quaddle_name = [shapes{shape_index(dim_vals(1))} '_'];
        else
            Quaddle_name = 'S00_';
        end
        if ~isnan(dim_vals(2)) %patterns
            Quaddle_name = [Quaddle_name patterns{pattern_index(dim_vals(2))} '_'];
        else
            Quaddle_name = [Quaddle_name 'P00_'];
        end
        if ~isnan(dim_vals(3)) %colors
            if isnan(dim_vals(2)) %patternless
                Quaddle_name = [Quaddle_name patternless_colors{color_index(dim_vals(3))} '_T00_'];
            else
                Quaddle_name = [Quaddle_name  patterned_colors{color_index(dim_vals(3))} '_T00_'];
            end
        else
            if isnan(dim_vals(2)) %patternless
                Quaddle_name = [Quaddle_name 'C6000000_6000000_T00_'];
            else
                Quaddle_name = [Quaddle_name 'C7000000_5000000_T00_'];
            end
        end
        if ~isnan(dim_vals(4))
            Quaddle_name = [Quaddle_name arms{arm_index(dim_vals(4))} '.fbx'];
        else
            Quaddle_name = [Quaddle_name 'A00_E00.fbx'];
        end
        targets_and_distractors{c} = Quaddle_name;
        targets_and_distractors_full_paths{c} = [Determine_Quaddle_Path(Quaddle_name,stim_path,false) Quaddle_name];
        [target_and_distractors_dimVals{c},~] = ParseQuaddleName(Quaddle_name,patternless_colors,arms);
        if  target_and_distractors_dimVals{c}(2) ~= 0
            [target_and_distractors_dimVals{c},~] = ParseQuaddleName(Quaddle_name,patterned_colors,arms);
        end
    end
    
    %add Neutral Quadddle to list of targets and distractors
    targets_and_distractors{c+1} = [neutral_Quaddle '.fbx'];
    targets_and_distractors_full_paths{c+1} = [Determine_Quaddle_Path([neutral_Quaddle '.fbx'],stim_path,false) [neutral_Quaddle '.fbx']];
    target_and_distractors_dimVals{c+1} = zeros(1,5);
    
    %---Generate the BlockDef----%
    filebase = [file_base_name num2str(set)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    for b = 1:num_blocks
        if using_contexts
            if rem(b,2) == 0%even block
                current_context = these_context_nums(2);
            else
                current_context = these_context_nums(1);
            end
        else
            current_context = -1;
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%---BlockDef for FLU trials---%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %---Make Active Array---%
        shape_str = [];
        pattern_str = [];
        color_str = [];
        arm_str = [];
        
        %for target dimension
        if target_dims_vals(b,1) == 1 %shape
            shape_str = ['[' strjoin(arrayfun(@(x) num2str(x),shape_index,'UniformOutput',false),',') ']'];
        elseif target_dims_vals(b,1) == 2 %pattern
            pattern_str = ['[' strjoin(arrayfun(@(x) num2str(x),pattern_index,'UniformOutput',false),',') ']'];
        elseif target_dims_vals(b,1) == 3 %color
            color_str = ['[' strjoin(arrayfun(@(x) num2str(x),color_index,'UniformOutput',false),',') ']'];
        elseif target_dims_vals(b,1) == 4 %arms
            arm_str = ['[' strjoin(arrayfun(@(x) num2str(x),arm_index,'UniformOutput',false),',') ']'];
        else
            error('Stimulus not recognized')
        end
        
        %for distractor(s) dimensions
        distractor_dims = these_dims;
        distractor_dims(target_dims_vals(b,1) == distractor_dims) = [];
        distractor_dims = distractor_dims(randperm(length(distractor_dims)));
        for dd = 1:all_number_of_irrelevant_feature_dims(b)
            if distractor_dims(dd) == 1 %shape
                shape_str = ['[' strjoin(arrayfun(@(x) num2str(x),shape_index,'UniformOutput',false),',') ']'];
            elseif distractor_dims(dd) == 2 %pattern
                pattern_str = ['[' strjoin(arrayfun(@(x) num2str(x),pattern_index,'UniformOutput',false),',') ']'];
            elseif distractor_dims(dd) == 3 %color
                color_str = ['[' strjoin(arrayfun(@(x) num2str(x),color_index,'UniformOutput',false),',') ']'];
            elseif distractor_dims(dd) == 4 %arms
                arm_str = ['[' strjoin(arrayfun(@(x) num2str(x),arm_index,'UniformOutput',false),',') ']'];
            else
                error('Stimulus not recognized')
            end
        end
        
        if isempty(shape_str)
            shape_str = '[0]';
        end
        if isempty(pattern_str)
            pattern_str = '[0]';
        end
        if isempty(color_str)
            color_str = '[0]';
        end
        if isempty(arm_str)
            arm_str = '[0]';
        end
        
        active_array = [ '[' shape_str ',' pattern_str ',' color_str ',[0],' arm_str ']' ];
        
        %---Make Rule Array---%
        %since stimuli are chosen at random jsut set 1st one as rewarded
        if target_dims_vals(b,1) == 1 %shape
            rule_array = ['[' num2str(shape_index(target_dims_vals(b,2))) '],[-1],[-1],[-1],[-1]'];%since stimuli are chosen at random jsut set 1st one as rewarded
        elseif target_dims_vals(b,1) == 2 %pattern
            rule_array = ['[-1],[' num2str(pattern_index(target_dims_vals(b,2))) '],[-1],[-1],[-1]'];%since stimuli are chosen at random jsut set 1st one as rewarded
        elseif target_dims_vals(b,1) == 3 %color
            rule_array = ['[-1],[-1],[' num2str(color_index(target_dims_vals(b,2))) '],[-1],[-1]'];
        elseif target_dims_vals(b,1) == 4 %amrs
            rule_array = ['[-1],[-1],[-1],[-1],[' num2str(arm_index(target_dims_vals(b,2))) ']'];
        else
            error('Stimulus not recognized')
        end
        
        %Detemrine number of trials in Block
        num_trials1 = all_number_min_trials(b);
        num_trials2 = max_num_trials;
        
        %Determine Block name...useful for post processing
        if b == 1
            FLU_block_id = ['FLU_Set' num2str(set) '_B' num2str(b) '_' ...
                num2str(all_number_of_irrelevant_feature_dims(b)) 'Irrel_First'];
        elseif all_intra_extra(b) == 0
            FLU_block_id = ['FLU_Set' num2str(set) '_B' num2str(b) '_' ...
                num2str(all_number_of_irrelevant_feature_dims(b)) 'Irrel_Intra'];
        elseif all_intra_extra(b) == 1
            FLU_block_id = ['FLU_Set' num2str(set) '_B' num2str(b) '_' ...
                num2str(all_number_of_irrelevant_feature_dims(b)) 'Irrel_Extra'];
        end
        bdstr2 = ['{"BlockID": "blockid.' FLU_block_id '",' ...
            ' "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '],' ...
            ' "ActiveFeatureTemplate": ' active_array ',' ...
            ' "ContextNums": [' num2str(current_context) '],' ...
            ' "NumRelevantObjectsPerTrial": [' num2str(num_Quaddles_per_trial) ',' num2str(num_Quaddles_per_trial) '], ' ...
            '"NumIrrelevantObjectsPerTrial": [0,0] ,'...
            ' "NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [' rule_array '], "ContextNums": [' num2str(current_context) '], ' ...
            '"RewardProb": ' num2str(RewardProb) ', "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        bdstr3 = '\n\n';
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%---BlockDef for ReFamilization Trials---%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %essential VDS/VDM trials
        if add_re_familiarization_block
            Fam_block_id = ['Fam_Set' num2str(set) '_B' num2str(b)];
            
            blockpath = [itemCND_path filebase '\\\\' filebase 'Fam_B' num2str(b)];
            bdstr2 = ['{"BlockID": "blockid.' Fam_block_id '", "TrialRange": [' num2str(num_fam_trials) ',' num2str(num_fam_trials) '], "TrialDefPath": "' ...
                blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
                '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
                '],...' '\n'...
                '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
            
            bdstr3 = '\n\n';
            fprintf(blockfID,bdstr2);
            fprintf(blockfID,bdstr3);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%---ITM/CND for Fam Blocks---%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            fam_block_target_and_distractors = cell(1,2);
            fam_block_target_and_distractors_full_path = cell(1,2);
            fam_block_target_and_distractors_dimVals = cell(1,2);
            
            %---1D Quaddle Targets---%
            if target_dims_vals(b,1) == 1 %shape
                fam_block_target_and_distractors{1} = [shapes{shape_index(target_dims_vals(b,2))} '_P00_C7000000_7000000_T00_A00_E00.fbx'];
            elseif target_dims_vals(b,1) == 2 %pattern
                fam_block_target_and_distractors{1} = ['S00_' patterns{pattern_index(target_dims_vals(b,2))} '_C7000000_5000000_T00_A00_E00.fbx'];
            elseif target_dims_vals(b,1) == 3 %color
                fam_block_target_and_distractors{1} = ['S00_P00_' patternless_colors{color_index(target_dims_vals(b,2))} '_T00_A00_E00.fbx'];
            elseif target_dims_vals(b,1) == 4 %arms
                fam_block_target_and_distractors{1} = ['S00_P00_C7000000_7000000_T00_' arms{arm_index(target_dims_vals(b,2))} '.fbx'];
            else
                error('Stimulus not recognized')
            end
            fam_block_target_and_distractors{2} = [neutral_Quaddle '.fbx'];
            
            for stim = 1:2
                fam_block_target_and_distractors_full_path{stim} = [Determine_Quaddle_Path(fam_block_target_and_distractors{stim},...
                    stim_path,false) fam_block_target_and_distractors{stim}];
                [fam_block_target_and_distractors_dimVals{stim},~] = ParseQuaddleName(fam_block_target_and_distractors{stim},patternless_colors,arms);
            end
            
            
            %-----Generate Item File---%
            fileIDitm = fopen([filebase 'Fam_B' num2str(b) '_Itm.txt'],'w');
            fprintf(fileIDitm,itm_header);
            item_num = 0;
            fam_item_locc_index = NaN(length(fam_block_target_and_distractors),length(xzlocs));
            fam_rewarded_item_locc_index = NaN(length(fam_block_target_and_distractors),length(xzlocs));
            for Q = 1:length(fam_block_target_and_distractors)
                for locs = 1:length(xzlocs)
                    item_num = item_num+1;
                    fam_item_locc_index(Q,locs) = item_num;
                    if Q == 1 %rewarded target
                        fam_rewarded_item_locc_index(Q,locs) = 1;
                        str = [num2str(item_num) '\t' fam_block_target_and_distractors{Q} '\t' fam_block_target_and_distractors_full_path{Q} '\t' ...
                            num2str(fam_block_target_and_distractors_dimVals{Q}(1)) '\t' num2str(fam_block_target_and_distractors_dimVals{Q}(2)) '\t' ...
                            num2str(fam_block_target_and_distractors_dimVals{Q}(3)) '\t' num2str(fam_block_target_and_distractors_dimVals{Q}(4)) '\t' ...
                            num2str(fam_block_target_and_distractors_dimVals{Q}(5)) '\t' ...
                            num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                            '80' '\t' '10' '\t' '10' '\t' ...
                            num2str(RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                            'true' '\t' 'true' '\t' 'true' '\n'];
                    else % neutral Quaddle
                        fam_rewarded_item_locc_index(Q,locs) = 0;
                        str = [num2str(item_num) '\t' fam_block_target_and_distractors{Q} '\t' fam_block_target_and_distractors_full_path{Q} '\t' ...
                            num2str(fam_block_target_and_distractors_dimVals{Q}(1)) '\t' num2str(fam_block_target_and_distractors_dimVals{Q}(2)) '\t' ...
                            num2str(fam_block_target_and_distractors_dimVals{Q}(3)) '\t' num2str(fam_block_target_and_distractors_dimVals{Q}(4)) '\t' ...
                            num2str(fam_block_target_and_distractors_dimVals{Q}(5)) '\t' ...
                            num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                            '80' '\t' '10' '\t' '10' '\t' ...
                            num2str(1-RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                            'true' '\t' 'true' '\t' 'true' '\n'];
                    end
                    fprintf(fileIDitm,str);
                end
            end
            fclose(fileIDitm);
            
            %----Generate Condition file-----%
            fileIDcnd = fopen([filebase 'Fam_B' num2str(b)  '_CND.txt'],'w');
            fprintf(fileIDcnd,cnd_header);
            
            fam_rewarded_items = find(fam_rewarded_item_locc_index == 1);
            fam_rewarded_items = fam_rewarded_items(randperm(length(fam_rewarded_items)));%balance so each target is unique-ish
            
            for t = 1:num_fam_trials
                numD = 1;
                location_index = randperm(length(xzlocs));%randomize location index
                target_row = 1;
                
                %make trial and context strings
                str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
                str2 = ['ContextNum' num2str(current_context) '\t'  num2str(current_context)  '\t'];
                
                %make target string
                str3 = [num2str(fam_item_locc_index(1,location_index(1))) '\t'];
                
                %make distractor string
                str4 = [];
                for stim = 1:length(xzlocs)-1
                    str4= [str4  num2str(fam_item_locc_index(2,location_index(stim+1)))];
                    if stim ~= length(xzlocs)-1
                        str4 = [str4 '\t'];
                    end
                end
                fprintf(fileIDcnd,[str1 str2 str3  str4 '\n']);
                
            end
            fclose(fileIDcnd);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%---BlockDef for FS trials---%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        FS_block_id = ['FS_Set' num2str(set) '_B' num2str(b)];
        blockpath = [itemCND_path filebase '\\\\' filebase 'FS_B' num2str(b)];
        bdstr2 = ['{"BlockID": "blockid.' FS_block_id '", "TrialRange": [' num2str(num_search_trials) ',' num2str(num_search_trials) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if b == num_blocks
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%---ITM/CND for FS Blocks---%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %---identify distractors with rewarded feature value---%
        contains_target_feature_value = NaN(1,length(target_and_distractors_dimVals));
        for td = 1:length(target_and_distractors_dimVals)
            if target_dims_vals(b,1) == 1%shapes
                if target_and_distractors_dimVals{td}(1) == shape_index(target_dims_vals(b,2))
                    contains_target_feature_value(td) = 1;
                else
                    contains_target_feature_value(td) = 0;
                end
            elseif target_dims_vals(b,1) == 2%patterns
                if target_and_distractors_dimVals{td}(2) == pattern_index(target_dims_vals(b,2))
                    contains_target_feature_value(td) = 1;
                else
                    contains_target_feature_value(td) = 0;
                end
            elseif target_dims_vals(b,1) == 3%colors
                if target_and_distractors_dimVals{td}(3) == color_index(target_dims_vals(b,2))
                    contains_target_feature_value(td) = 1;
                else
                    contains_target_feature_value(td) = 0;
                end
            elseif target_dims_vals(b,1) == 4%arms
                if target_and_distractors_dimVals{td}(5) == arm_index(target_dims_vals(b,2))
                    contains_target_feature_value(td) = 1;
                else
                    contains_target_feature_value(td) = 0;
                end
            else
                error('Target Dimensions not recognized')
            end
        end
        if sum(contains_target_feature_value) ~= num_feature_values_per_dim*num_feature_dimensions
            %I think this should work for all # features dims and values
            %but not 100% sure, definitely works for 3dx3f, if it doesnt
            %then it should cauze an error :)
            error('Not enough targets found for FS blocks')
        end
        
        %-----Generate Item File---%
        fileIDitm = fopen([filebase 'FS_B' num2str(b) '_Itm.txt'],'w');
        fprintf(fileIDitm,itm_header);
        item_num = 0;
        item_locc_index = NaN(length(targets_and_distractors),length(xzlocs));
        rewarded_item_locc_index = NaN(length(targets_and_distractors),length(xzlocs));
        for Q = 1:length(targets_and_distractors)
            for locs = 1:length(xzlocs)
                item_num = item_num+1;
                item_locc_index(Q,locs) = item_num;
                if contains_target_feature_value(Q) %rewarded feature
                    rewarded_item_locc_index(Q,locs) = 1;
                    str = [num2str(item_num) '\t' targets_and_distractors{Q} '\t' targets_and_distractors_full_paths{Q} '\t' ...
                        num2str(target_and_distractors_dimVals{Q}(1)) '\t' num2str(target_and_distractors_dimVals{Q}(2)) '\t' ...
                        num2str(target_and_distractors_dimVals{Q}(3)) '\t' num2str(target_and_distractors_dimVals{Q}(4)) '\t' ...
                        num2str(target_and_distractors_dimVals{Q}(5)) '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        num2str(RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                else
                    rewarded_item_locc_index(Q,locs) = 0;
                    str = [num2str(item_num) '\t' targets_and_distractors{Q} '\t' targets_and_distractors_full_paths{Q} '\t' ...
                        num2str(target_and_distractors_dimVals{Q}(1)) '\t' num2str(target_and_distractors_dimVals{Q}(2)) '\t' ...
                        num2str(target_and_distractors_dimVals{Q}(3)) '\t' num2str(target_and_distractors_dimVals{Q}(4)) '\t' ...
                        num2str(target_and_distractors_dimVals{Q}(5)) '\t' ...
                        num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                        '80' '\t' '10' '\t' '10' '\t' ...
                        num2str(1-RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                        'true' '\t' 'true' '\t' 'true' '\n'];
                end
                fprintf(fileIDitm,str);
            end
        end
        fclose(fileIDitm);
        
        %----Generate Condition file-----%
        fileIDcnd = fopen([filebase 'FS_B' num2str(b)  '_CND.txt'],'w');
        fprintf(fileIDcnd,cnd_header);
        
        if num_search_trials <=  sum(contains_target_feature_value)
            rewarded_items = find(contains_target_feature_value == 1);
            rewarded_items = rewarded_items(randperm(length(rewarded_items)));%balance so each target is unique-ish
        else
            error('Need to add code here to randomize better');
        end
        
        %randomize number of distractors by trial
        all_num_Distractors = [];
        for reps = 1:num_search_reps
            all_num_Distractors = [all_num_Distractors num_Distractors];
        end
        all_num_Distractors = all_num_Distractors(randperm(length(all_num_Distractors)));
        
        for t = 1:num_search_trials
            numD = all_num_Distractors(t);
            location_index = randperm(length(xzlocs));%randomize location index
            target_row = rewarded_items(t);
            
            distractor_rows = 1:size(rewarded_item_locc_index,1)-1;%exclude neutral from this
            distractor_rows(rewarded_items) = [];
            distractor_rows = distractor_rows(randperm(length(distractor_rows)));
            
            %make trial and context strings
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(current_context) '\t'  num2str(current_context)  '\t'];
            
            %make target string
            str3 = [num2str(item_locc_index(target_row,location_index(1))) '\t'];
            
            %make distractor string
            str4 = [];
            for stim = 1:length(xzlocs)-1
                if stim <= numD %use distractors
                    str4= [str4  num2str(item_locc_index(distractor_rows(stim),location_index(stim+1)))];
                    if stim ~= length(xzlocs)-1
                        str4 = [str4 '\t'];
                    end
                else %use neutral Quaddles to fill the rest of the locations
                    str4= [str4  num2str(item_locc_index(end,location_index(stim+1)))];
                    if stim ~= length(xzlocs)-1
                        str4 = [str4 '\t'];
                    end
                end
            end
            fprintf(fileIDcnd,[str1 str2 str3  str4 '\n']);

        end
        fclose(fileIDcnd);
    end
    fclose(blockfID);
end
fclose all;
cd(current_dir)