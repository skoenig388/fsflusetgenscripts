function GenerateITM_CND_files_from_target_distractor_list(ITMCND_name,context,...
    target_distractors,target_distractors_full_paths,target_distractors_dimVals,...
    stim_locs,y_default,rewarded_features,num_distractors,num_neutral,num_trials,...
    RewardProb,RewardMag,task_type)

%Seth Koenig 6/21/19
%Methd for generating ITM and Conditions files is essential the same across
%all tasks if you know the rewarded feature(s) and locations

rewarded_feature_ind = find(rewarded_features~=0);
num_locations = size(stim_locs,2);

%---Generate ITM and CND Headers---%
itm_header=['StimCode' '\t' 'StimName' '\t' 'StimPath' '\t' ...
    'StimDimVals1' '\t' 'StimDimVals2' '\t' 'StimDimVals3' '\t' 'StimDimVals4' '\t' 'StimDimVals5' '\t' ...
    'StimLocationX' '\t' 'StimLocationY' '\t' 'StimLocationZ' '\t' ...
    'StimRotationX' '\t' 'StimRotationY' '\t' 'StimRotationZ' '\t' ...
    'StimTrialRewardProb' '\t' 'StimTrialRewardMag' '\t' 'TimesUsedInBlock' ...
    '\t' 'isRelevant' '\t' 'SetLocation' '\t' 'SetRotation' '\n'];

cnd_header=['TrialName' '\t' 'TrialCode' '\t' 'contextNum' '\t' 'contextName' '\t' ...
    'Stim1' '\t'  'Stim2' '\t'  'Stim3' '\t'  'Stim4' '\t'  'Stim5' '\t'  'Stim6' '\t' ...
    'Stim7' '\t'  'Stim8' '\t'  'Stim9' '\t'  'Stim10' '\t'  'Stim11' '\t'  'Stim12' '\t' ...
    'Stim13' '\t'  'Stim14' '\n'];



%---Generate Item File---%
fileIDitm = fopen([ITMCND_name '_Itm.txt'],'w');
fprintf(fileIDitm,itm_header);
item_num = 0;
item_locc_index = NaN(length(target_distractors),num_locations);
rewarded_item_locc_index = NaN(length(target_distractors),num_locations);
is_neutral = NaN(length(target_distractors),num_locations);
for Q = 1:length(target_distractors)
    for locs = 1:num_locations
        item_num = item_num+1;
        item_locc_index(Q,locs) = item_num;
        
        if all(target_distractors_dimVals{Q} == 0)
            is_neutral(Q,locs) = 1;
        else
            is_neutral(Q,locs) = 0;
        end
        
        if sum(target_distractors_dimVals{Q}(rewarded_feature_ind) == rewarded_features(rewarded_feature_ind))
            rewarded_item_locc_index(Q,locs) = 1;
            this_RewardProb = RewardProb;
        else
            rewarded_item_locc_index(Q,locs) = 0;
            this_RewardProb = 1-RewardProb;
        end
        
        itm_str = [num2str(item_num) '\t' target_distractors{Q} '\t' target_distractors_full_paths{Q} '\t' ...
            num2str(target_distractors_dimVals{Q}(1)) '\t' num2str(target_distractors_dimVals{Q}(2)) '\t' ...
            num2str(target_distractors_dimVals{Q}(3)) '\t' num2str(target_distractors_dimVals{Q}(4)) '\t' ...
            num2str(target_distractors_dimVals{Q}(5)) '\t' ...
            num2str(stim_locs(1,locs)) '\t' num2str(y_default) '\t'  num2str(stim_locs(2,locs)) '\t' ...
            '80' '\t' '10' '\t' '10' '\t' ...
            num2str(this_RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
            'true' '\t' 'true' '\t' 'true' '\n'];
        
        fprintf(fileIDitm,itm_str);
    end
end
fclose(fileIDitm);

%----Generate Condition file-----%

fileIDcnd = fopen([ITMCND_name  '_CND.txt'],'w');
fprintf(fileIDcnd,cnd_header);

%---Balance feautre instances of rewarded stimuli as best as possible---%
[rewarded_rows,~] = find(rewarded_item_locc_index == 1);

if isempty(rewarded_rows)
    error('Where are all of the rewarded items?')
end

rewarded_rows = unique(rewarded_rows);
if length(rewarded_rows) < num_trials
    num_reps = ceil(num_trials/length(rewarded_rows));
    all_rewarded_rows = [];
    for rep = 1:num_reps
        all_rewarded_rows = [all_rewarded_rows; rewarded_rows(randperm(length(rewarded_rows)))];
    end
else
    all_rewarded_rows = rewarded_rows(randperm(length(rewarded_rows)));
end

[neutral_row,~] = find(is_neutral == 1);%should just be 1 row
neutral_row = unique(neutral_row);
if length(neutral_row) > 1
    error('Why is more than item 1 row neutral?')
end

%---Balance Distractors as Much As Possible---%
distractor_rows = 1:size(rewarded_item_locc_index,1);
distractor_rows(rewarded_rows) = [];
distractor_rows(distractor_rows == neutral_row) = [];

%---Determine number of distractors per trial
all_num_Distractors =[];
all_num_neutral = [];
if contains(task_type,'FS')
    num_reps = ceil(num_trials/length(num_distractors));%should be integer if we're smart about it but just in case
    for rep = 1:num_reps
        all_num_Distractors = [all_num_Distractors num_distractors];
    end
    all_num_Distractors = all_num_Distractors(randperm(length(all_num_Distractors)));
    all_num_neutral = num_locations*ones(1,num_trials)-all_num_Distractors-1;
else
    all_num_Distractors = num_distractors*ones(1,num_trials);
    all_num_neutral = num_neutral*ones(1,num_trials);
end

for t = 1:num_trials
    location_index = randperm(length(stim_locs));%randomize location index
    
    %---Choose distractors, varies by task type---%
    if strcmpi(task_type,'FLU')
        %Want only 1 instance of each feature value
        used_dims = cell(1,5);
        for dim = 1:5
            if target_distractors_dimVals{all_rewarded_rows(t)}(dim) ~= 0
                used_dims{dim} = target_distractors_dimVals{all_rewarded_rows(t)}(dim);
            end
        end
        these_distractor_rows = distractor_rows(randperm(length(distractor_rows)));
        selected_distractor_rows = NaN(1,num_distractors);
        found_distractors = 0;
        for row = 1:length(these_distractor_rows)
            overlap = false;
            for dim = 1:5
                if any(target_distractors_dimVals{these_distractor_rows(row)}(dim) == used_dims{dim})
                    overlap = true;
                end
            end
            if ~overlap
                found_distractors = found_distractors + 1;
                selected_distractor_rows(found_distractors) = these_distractor_rows(row);
                for dim = 1:5
                    if target_distractors_dimVals{these_distractor_rows(row)}(dim) ~= 0
                        used_dims{dim} = [used_dims{dim} target_distractors_dimVals{these_distractor_rows(row)}(dim)];
                    end
                end
                if found_distractors == num_distractors
                    break
                end
            else
                continue
            end
        end

    elseif strcmpi(task_type,'FS')
        %just choose at random
        if all_num_Distractors(t) <= length(distractor_rows)
            these_distractor_rows = distractor_rows(randperm(length(distractor_rows)));
        else
            distractor_rows2 = [distractor_rows distractor_rows];
            these_distractor_rows = distractor_rows2(randperm(length(distractor_rows2)));
        end
       selected_distractor_rows = these_distractor_rows(1:all_num_Distractors(t));
    elseif strcmpi(task_type,'CFS')
        disp('Need CFS code')
    else
        eror('Uknown task type')
    end
    
    %make trial and context strings, adding num distractors makes post-processing easier
    str1 = ['TrialNum' num2str(t+1000*all_num_Distractors(t)) '\t' num2str(t+1000*all_num_Distractors(t)) '\t'];
    str2 = ['contextNum' num2str(context) '\t'  num2str(context)  '\t'];
    
    %make target string
    str3 = [num2str(item_locc_index(all_rewarded_rows(t),location_index(1))) '\t'];
    
    %make distractor string
    str4 = [];
    for stim = 1:all_num_Distractors(t)
        str4= [str4  num2str(item_locc_index(selected_distractor_rows(stim),location_index(stim+1)))];
        if all_num_neutral(t) > 0 || (stim ~= all_num_Distractors(t) &&  all_num_neutral(t) == 0)
            str4 = [str4 '\t'];
        end
    end
    
    %make neutral string
    str5= [];
    for stim = 1:all_num_neutral(t)
        str5= [str5  num2str(item_locc_index(neutral_row,location_index(stim+1+all_num_Distractors(t))))];
        if stim ~= all_num_neutral(t)
            str5 = [str5 '\t'];
        end
    end

    fprintf(fileIDcnd,[str1 str2 str3  str4 str5 '\n']);
    
end
fclose(fileIDcnd);
end