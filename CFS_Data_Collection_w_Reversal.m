%Written by Seth Koenig May 13, 2019
%Code builds CFS task with variable number of distractors from trial to
%trial and reverses context-feature rules so that things are more balanced
%and don't have to worry about discriminability. Also introduces larger
%range of distractors so up to 2x uniform probability of being shown.

clear,clc,close all

%seed random number generator, do not change EVER &...
%always run file from the start and generate all files
rng(20190708);

current_dir = pwd;
%% Stim Path Parameters

%Example Paths for Dual-Drive Computer
stim_path = 'D:\\QuaddleRepository_20180515\\';
itemCND_path = 'D:\\\\BlockDefnitions\\\\Feature_Search_Training\\\\';

%Example Paths for Kiosk Computers with 1 drive
%%stim_path = 'C:\\Users\\Womelsdorf Lab\\Documents\\Quaddles\\TexturelessQuaddleRepo\\';
%%itemCND_path = 'C:\\\\Users\\\\Womelsdorf Lab\\\\Documents\\\\BlockDefinitions\\\\Feature_Search_Training\\\\';


%% Block and Trial Count parmaeters
num_feature_values_per_dim = 2;
num_sets_to_generate = 20;
context_nums = 0:4;
bad_context_colors = [NaN 3 4 6 1];%colors that may be harder to discriminate given the background
%gravel, sand, grass, ice, red brick

RewardMag = 1;
RewardProb = 1.0;

num_distractor_Quaddles = [3 6 9 12];

%total trials is # of blocks * (fam_num_trials + trials_per_search_block)
fam_num_trials = 20;
trials_per_search_block = 240;
number_of_blocks = 6;%number of super blocks

%% Quaddle Spacing-Generate All possible locations
spatial_scale = 0.05;
y_default = 0.4;
x_spacing = 4;%also exlucde within 2 units of center, arms make us need more spacing on x-axis
z_spacing = 3;%also exlucde within 1.5 units of center
x_max = 8;%magnitude
z_max = 4.5;%magnitude

xzlocs = [];
count = 0;
for x = -x_max:x_spacing:x_max
    for z = -z_max:z_spacing:z_max
        if x == 0 && z == 0
            continue
        else
            count = count+1;
            xzlocs(1,count) = x;
            xzlocs(2,count) = z;
        end
    end
end

%% Distribution of context switches

num_context_trials_in_a_row = [1 2 3 4 5];
prob_switch = [1/3 1/4 3/16 1/8 1/10];
cumsum_prop_switch = cumsum(prob_switch);
if sum(prob_switch) < 0.99
    error('Switch probabilities dont add up')
end
cumsum_prop_switch(end) = 1;

%% Feature Dimensions and values
shapes = {'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

patterned_colors =   {'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000','C7070240_5000000','C7070286_5000000', 'C7070335_5000000'};
%patternless_colors = {'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070239_6070239','C6070287_6070287', 'C6070335_6070335'};
patternless_colors = {'C7070014_7070014', 'C7070059_7070059', 'C7070106_7070106', 'C7070148_7070148', 'C7070194_7070194','C7070240_7070240','C7070286_7070286', 'C7070335_7070335'};
gray_pattern_color = 'C7000000_5000000';

arms = {'A00_E01', 'A00_E02','A00_E03', 'A01_E00', 'A01_E01', 'A01_E02', 'A01_E03', 'A02_E00', 'A02_E01', 'A02_E02', 'A02_E03'};

neutral_Quaddle = 'S00_P00_C7000000_7000000_T00_A00_E00';
%% Generate ITM and CND Headers which are always the same for all files

itm_header=['StimCode' '\t' 'StimName' '\t' 'StimPath' '\t' ...
    'StimDimVals1' '\t' 'StimDimVals2' '\t' 'StimDimVals3' '\t' 'StimDimVals4' '\t' 'StimDimVals5' '\t' ...
    'StimLocationX' '\t' 'StimLocationY' '\t' 'StimLocationZ' '\t' ...
    'StimRotationX' '\t' 'StimRotationY' '\t' 'StimRotationZ' '\t' ...
    'StimTrialRewardProb' '\t' 'StimTrialRewardMag' '\t' 'TimesUsedInBlock' ...
    '\t' 'isRelevant' '\t' 'SetLocation' '\t' 'SetRotation' '\n'];

cnd_header=['TrialName' '\t' 'TrialCode' '\t' 'ContextNum' '\t' 'ContextName' '\t' ...
    'Stim1' '\t'  'Stim2' '\t'  'Stim3' '\t'  'Stim4' '\t'  'Stim5' '\t'  'Stim6' '\t' ...
    'Stim7' '\t'  'Stim8' '\t'  'Stim9' '\t'  'Stim10' '\t'  'Stim11' '\t'  'Stim12' '\t' ...
    'Stim13' '\t'  'Stim14' '\n'];

%% Generate Context Sets

file_base_name = 'CFS_Reversal_DC_Set';
for file = 1:num_sets_to_generate
    
    %---Pick 2 contexts---%
    these_context_nums = context_nums(randperm(length(context_nums)));
    these_context_nums = these_context_nums(1:2);
    
    %---Pick N-Dimensions---%
    these_dims = 1:4;
    these_dims = these_dims(randperm(4));
    these_dims = sort(these_dims(1:3));
    
    %-----Shuffle feature values-----%
    shape_index = randperm(length(shapes));
    shape_index = sort(shape_index(1:2));
    
    %exclude [patterns 4/5 and 6/7 from being with each other
    all_pattern_index = randperm(length(patterns));
    pattern_index = sort(all_pattern_index(1:2));
    while (any(pattern_index == 4) && any(pattern_index == 5)) ||...
            (any(pattern_index == 6) && any(pattern_index == 7))
        all_pattern_index = randperm(length(patterns));
        pattern_index = sort(all_pattern_index(1:2));
    end
    
    %     %exclude colors right next to each other because background interfers a bit in color discrimnation
    %     color_index = randperm(length(patternless_colors));
    %     while abs(color_index(1)-color_index(2)) == 1 || abs(color_index(1)-color_index(2)) == length(color_index)-1
    %         color_index = randperm(length(patternless_colors));
    %     end
    %     color_index = sort(color_index(1:2));

    these_colors = 1:length(patternless_colors);
    %remove "bad" colors
    for con = 1:2
        if ~isnan(bad_context_colors(these_context_nums(con)+1))
            these_colors(these_colors == bad_context_colors(these_context_nums(con)+1)) = [];
        end
    end
    tryagain = true;
    while tryagain
        color_index = these_colors(randperm(length(these_colors)));
        selected_colors = color_index(1:2);
        if all(pdist(color_index(1:2)') > 1)
            %color map is circular so check for 1 next to 8
            if ~any(selected_colors == 1 | selected_colors == 8)
                tryagain = false;
                color_index = sort(color_index(1:2));
            elseif (any(selected_colors == 1) && ~any(selected_colors == 8)) || ...
                    (any(selected_colors == 8) && ~any(selected_colors == 1))
                tryagain = false;
                color_index = sort(color_index(1:2));
            end
        end
    end
    
    arm_index = randperm(length(arms));
    arm_index = sort(arm_index(1:2));
   
    all_dimensional_indexes = [shape_index; pattern_index; color_index; arm_index];
    
    
    %---Generate All Feature Combinations---%
    %note indexing and math here is a little convoluted...sorry
    %all single feature combos
    all_combos1 = combnk([these_dims 10*these_dims],1);
    
    [all_targets_and_distractors1,all_target_and_distractors_dimVals1,all_targets_and_distractors_full_paths1] = ...
        feature_combos2_QuaddleNames(all_combos1,these_dims,stim_path,shapes,patterns,...
        patterned_colors,patternless_colors,arms,all_dimensional_indexes);
    
    %all double feature combos
    all_combos2 = combnk([these_dims 10*these_dims],2);
    all_combos2(all_combos2(:,1)*10 == all_combos2(:,2),:) = [];
    
    [all_targets_and_distractors2,all_target_and_distractors_dimVals2,all_targets_and_distractors_full_paths2] = ...
        feature_combos2_QuaddleNames(all_combos2,these_dims,stim_path,shapes,patterns,...
        patterned_colors,patternless_colors,arms,all_dimensional_indexes);
    
    %all tripple feature combos
    all_combos3 = combnk([these_dims 10*these_dims],3);
    all_combos3(all_combos3(:,1)*10 == all_combos3(:,2),:) = [];
    all_combos3(all_combos3(:,2)*10 == all_combos3(:,3),:) = [];
    all_combos3(all_combos3(:,1)*10 == all_combos3(:,3),:) = [];
    
    [all_targets_and_distractors3,all_target_and_distractors_dimVals3,all_targets_and_distractors_full_paths3] = ...
        feature_combos2_QuaddleNames(all_combos3,these_dims,stim_path,shapes,patterns,...
        patterned_colors,patternless_colors,arms,all_dimensional_indexes);
    
    %Combine together & add Neutral Quadddle to list of targets and distractors
    all_targets_and_distractors = [all_targets_and_distractors3 all_targets_and_distractors2 all_targets_and_distractors1];
    all_target_and_distractors_dimVals = [all_target_and_distractors_dimVals3 all_target_and_distractors_dimVals2 all_target_and_distractors_dimVals1];
    all_targets_and_distractors_full_paths = [all_targets_and_distractors_full_paths3 all_targets_and_distractors_full_paths2 all_targets_and_distractors_full_paths1];
    
    all_targets_and_distractors{end+1} = [neutral_Quaddle '.fbx'];
    all_targets_and_distractors_full_paths{end+1} = [Determine_Quaddle_Path([neutral_Quaddle '.fbx'],stim_path,false) [neutral_Quaddle '.fbx']];
    all_target_and_distractors_dimVals{end+1} = zeros(1,5);
    
    %1 for context 1 and 2 for context!
    rewarded = zeros(length(all_targets_and_distractors),2);
    rewarded(1,1) = 1;
    rewarded(8,1) = 2;
    
    %---First Generate the BlockDef----%
    %for fam blocks
    num_trials1f = fam_num_trials;
    num_trials2f = fam_num_trials;
    
    %for search blocks
    num_trials1 = trials_per_search_block;
    num_trials2 = trials_per_search_block;
    
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    for block = 1:number_of_blocks
        
        %Fam blocks
        blockpath = [itemCND_path filebase '\\\\' filebase '_B' num2str(block) '_Fam'];
        bdstr2 = ['{"BlockID": "blockid.' [filebase '_B' num2str(block) '_Fam'] '", "TrialRange": [' num2str(num_trials1f) ',' num2str(num_trials2f) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        bdstr3 = '\n\n';
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        %Search blocks
        blockpath = [itemCND_path filebase '\\\\' filebase '_B' num2str(block) '_Search'];
        bdstr22 = ['{"BlockID": "blockid.' [filebase '_B' num2str(block) '_Search'] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if block == number_of_blocks
            bdstr33 =  ']\n//end of blockDef[]';
        else
            bdstr33 = '\n\n';
        end
        fprintf(blockfID,bdstr22);
        fprintf(blockfID,bdstr33);
    end
    fclose(blockfID);
    
    %-----2nd Generate Item & Condition Files---%
    for block = 1:number_of_blocks
        
        %using an ABABAB design so switch context-target associations every
        %block to counter-balance
        if rem(block,2) == 1 
            reward_opposite = false;
        else
            reward_opposite = true;
        end
        
        %---Generate itm/cnd files for Familarization trials---%
        fileIDitm = fopen([filebase '_B' num2str(block) '_Fam' '_Itm.txt'],'w');
        fprintf(fileIDitm,itm_header);
        item_num = 0;
        item_locc_index = NaN(length(all_targets_and_distractors),length(xzlocs),2);
        rewarded_item_locc_index = NaN(length(all_targets_and_distractors),length(xzlocs),2);
        for contxt = 1:2
            if contxt == 1
                other_context = 2;
            else
                other_context = 1;
            end
            for Q = 1:length(all_targets_and_distractors)
                for locs = 1:length(xzlocs)
                    item_num = item_num+1;
                    item_locc_index(Q,locs,contxt) = item_num;
                    if (rewarded(Q) == contxt && ~reward_opposite) || ...
                        (rewarded(Q) == other_context && reward_opposite)    
                        rewarded_item_locc_index(Q,locs,contxt) = contxt;
                        str = [num2str(item_num) '\t' all_targets_and_distractors{Q} '\t' all_targets_and_distractors_full_paths{Q} '\t' ...
                            num2str(all_target_and_distractors_dimVals{Q}(1)) '\t' num2str(all_target_and_distractors_dimVals{Q}(2)) '\t' ...
                            num2str(all_target_and_distractors_dimVals{Q}(3)) '\t' num2str(all_target_and_distractors_dimVals{Q}(4)) '\t' ...
                            num2str(all_target_and_distractors_dimVals{Q}(5)) '\t' ...
                            num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                            '80' '\t' '10' '\t' '10' '\t' ...
                            num2str(RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                            'true' '\t' 'true' '\t' 'true' '\n'];
                    else
                        rewarded_item_locc_index(Q,locs,contxt) = 0;
                        str = [num2str(item_num) '\t' all_targets_and_distractors{Q} '\t' all_targets_and_distractors_full_paths{Q} '\t' ...
                            num2str(all_target_and_distractors_dimVals{Q}(1)) '\t' num2str(all_target_and_distractors_dimVals{Q}(2)) '\t' ...
                            num2str(all_target_and_distractors_dimVals{Q}(3)) '\t' num2str(all_target_and_distractors_dimVals{Q}(4)) '\t' ...
                            num2str(all_target_and_distractors_dimVals{Q}(5)) '\t' ...
                            num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                            '80' '\t' '10' '\t' '10' '\t' ...
                            num2str(1-RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                            'true' '\t' 'true' '\t' 'true' '\n'];
                    end
                    fprintf(fileIDitm,str);
                end
            end
        end
        fclose(fileIDitm);
        
        %----Generate Condition file for Familiarization Trials-----%
        fileIDcnd = fopen([filebase '_B' num2str(block) '_Fam' '_CND.txt'],'w');
        fprintf(fileIDcnd,cnd_header);
        
        %uniformly generate context orders
        context_dist = [ones(1,num_trials2f/2) 2*ones(1,num_trials2f/2)];
        context_dist = context_dist(randperm(length(context_dist)));
        
        for t = 1:num_trials2f
            current_context = context_dist(t);
            item_index = randperm(length(xzlocs));%randomize location index
            numD = 0;
            
            target_row = find(rewarded_item_locc_index(:,1,current_context) == current_context);
            
            %make trial and context strings
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(these_context_nums(current_context)) '\t'  num2str(these_context_nums(current_context))  '\t'];
            
            %make target string
            str3 = num2str(item_locc_index(target_row,item_index(1),current_context));
            if numD > 0
                str3 = [str3 '\t'];
            end
            
            %make distractor string
            str4 = [];
%             if current_context == 2
%                disp('now') 
%             end
            fprintf(fileIDcnd,[str1 str2 str3  str4 '\n']);
        end
        fclose(fileIDcnd);
        
        %---Generate itm/cnd files for Search trials---%
        fileIDitm = fopen([filebase '_B' num2str(block) '_Search' '_Itm.txt'],'w');
        fprintf(fileIDitm,itm_header);
        item_num = 0;
        item_locc_index = NaN(length(all_targets_and_distractors),length(xzlocs),2);
        rewarded_item_locc_index = NaN(length(all_targets_and_distractors),length(xzlocs),2);
        for contxt = 1:2
            if contxt == 1
                other_context = 2;
            else
                other_context = 1;
            end
            for Q = 1:length(all_targets_and_distractors)
                for locs = 1:length(xzlocs)
                    item_num = item_num+1;
                    item_locc_index(Q,locs,contxt) = item_num;
                    if (rewarded(Q) == contxt && ~reward_opposite) || ...
                            (rewarded(Q) == other_context && reward_opposite)
                        rewarded_item_locc_index(Q,locs,contxt) = contxt;
                        str = [num2str(item_num) '\t' all_targets_and_distractors{Q} '\t' all_targets_and_distractors_full_paths{Q} '\t' ...
                            num2str(all_target_and_distractors_dimVals{Q}(1)) '\t' num2str(all_target_and_distractors_dimVals{Q}(2)) '\t' ...
                            num2str(all_target_and_distractors_dimVals{Q}(3)) '\t' num2str(all_target_and_distractors_dimVals{Q}(4)) '\t' ...
                            num2str(all_target_and_distractors_dimVals{Q}(5)) '\t' ...
                            num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                            '80' '\t' '10' '\t' '10' '\t' ...
                            num2str(RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                            'true' '\t' 'true' '\t' 'true' '\n'];
                    else
                        rewarded_item_locc_index(Q,locs,contxt) = 0;
                        str = [num2str(item_num) '\t' all_targets_and_distractors{Q} '\t' all_targets_and_distractors_full_paths{Q} '\t' ...
                            num2str(all_target_and_distractors_dimVals{Q}(1)) '\t' num2str(all_target_and_distractors_dimVals{Q}(2)) '\t' ...
                            num2str(all_target_and_distractors_dimVals{Q}(3)) '\t' num2str(all_target_and_distractors_dimVals{Q}(4)) '\t' ...
                            num2str(all_target_and_distractors_dimVals{Q}(5)) '\t' ...
                            num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                            '80' '\t' '10' '\t' '10' '\t' ...
                            num2str(1-RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                            'true' '\t' 'true' '\t' 'true' '\n'];
                    end
                    fprintf(fileIDitm,str);
                end
            end
        end
        fclose(fileIDitm);
        
        %----Generate Condition file for Search Trials-----%
        fileIDcnd = fopen([filebase '_B' num2str(block) '_Search' '_CND.txt'],'w');
        fprintf(fileIDcnd,cnd_header);
        
        %generate contexts based on switch rate above
        context_dist = NaN(1,trials_per_search_block);
        t1 = 1;
        t2 = t1;
        while t2(end) < trials_per_search_block
            t1 = t2(end);
            if t1 == 1
                if rand(1) < 0.5
                    current_context = 1;
                else
                    current_context = 2;
                end
            else
                if current_context == 1
                    current_context = 2;
                else
                    current_context = 1;
                end
            end
            rnd = rand(1);
            rind = find(rnd < cumsum_prop_switch);
            if isempty(rind)
                rind = 1;
            else
                rind = rind(1);
            end
            
            t2 = t1+num_context_trials_in_a_row(rind)-1;
            context_dist(t1:t2) = current_context;
        end
        context_dist = context_dist(1:trials_per_search_block);
        
        %uniformly distribute the number of distractors
        all_numD = ones(trials_per_search_block/length(num_distractor_Quaddles),1)*num_distractor_Quaddles;
        all_numD = all_numD(:);
        all_numD = all_numD(randperm(length(all_numD)));
        
        for t = 1:num_trials2
            current_context = context_dist(t);
            item_index = randperm(length(xzlocs));%randomize location index
            numD = all_numD(t);%select number of distractors for this trial
            
            target_row = find(rewarded_item_locc_index(:,1,current_context) == current_context);

            distractors = 1:size(rewarded_item_locc_index,1);
            distractors(isnan(rewarded_item_locc_index(:,1,current_context))) = [];
            distractors(distractors == target_row) = [];
            
            %             %double the distribution of distractors to get more variability
            %             all_distractors = [distractors distractors];
            %             all_distractors = all_distractors(randperm(length(all_distractors)));
            all_distractors = distractors(randperm(length(distractors)));
            
            %make trial and context strings
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(these_context_nums(current_context)) '\t'  num2str(these_context_nums(current_context))  '\t'];
            
            %make target string
            str3 = num2str(item_locc_index(target_row,item_index(1),current_context));
            if numD > 0
                str3 = [str3 '\t'];
            end
            
            %make distractor string
            str4 = [];
            for stim = 1:numD
                str4= [str4  num2str(item_locc_index(all_distractors(stim),item_index(stim+1),current_context))];
                if stim ~= numD
                    str4 = [str4 '\t'];
                end
            end
            fprintf(fileIDcnd,[str1 str2 str3  str4 '\n']);
            
        end
        fclose(fileIDcnd);
    end
end
fclose all
cd(current_dir)