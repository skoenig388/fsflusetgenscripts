%Written by Seth Koenig February 25, 2019
%Do not use does not work....old code
clear,clc,close all

rng(20190225); %seed random number generator, do not change EVER &...
%always run file from the start and generate all files

current_dir = pwd;
%% Stim Path Parameters

%Example Paths for Dual-Drive Computer
stim_path = 'D:\\QuaddleRepository_20180515\\';
itemCND_path = 'D:\\\\BlockDefnitions\\\\Feature_Search_Training\\\\';

%Example Paths for Kiosk Computers with 1 drive
%%stim_path = 'C:\\Users\\Womelsdorf Lab\\Documents\\Quaddles\\TexturelessQuaddleRepo\\';
%%itemCND_path = 'C:\\\\Users\\\\Womelsdorf Lab\\\\Documents\\\\BlockDefinitions\\\\Feature_Search_Training\\\\';


%% Block and Trial Cound parmaeters

num_sets_to_generate = 5;

RewardMag = 3;
RewardProb = 1.0;

num_distractor_Quaddles = [0 1 3 5 9 12];
fam_num_trials = 20;
min_num_trials = 40;
max_num_trials = 100;

context_nums = 0:4;
psuedo_random_block_size = 5; %for context balancing
%% Quaddle Spacing-Generate All possible locations

spatial_scale = 0.05;
y_default = 0.4;
x_spacing = 4;%also exlucde within 2 units of center, arms make us need more spacing on x-axis
z_spacing = 3;%also exlucde within 1.5 units of center
x_max = 8;%magnitude
z_max = 4.5;%magnitude

xzlocs = [];
count = 0;
for x = -x_max:x_spacing:x_max
    for z = -z_max:z_spacing:z_max
        if x == 0 && z == 0
            continue
        else
            count = count+1;
            xzlocs(1,count) = x;
            xzlocs(2,count) = z;
        end
    end
end

%% Feature Dimensions and values
shapes = {'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

patternless_colors = {'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070287_6070287', 'C6070335_6070335'};
%patterned_colors =   {'C7000000_5000000', 'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000', 'C7070286_5000000', 'C7070335_5000000'};
gray_pattern_color = 'C7000000_5000000';

arms = {'A00_E01', 'A00_E02','A00_E03', 'A01_E00', 'A01_E01', 'A01_E02', 'A01_E03', 'A02_E00', 'A02_E01', 'A02_E02', 'A02_E03'};

%% Generate ITM and CND Headers which are always the same for all files

itm_header=['StimCode' '\t' 'StimName' '\t' 'StimPath' '\t' ...
    'StimDimVals1' '\t' 'StimDimVals2' '\t' 'StimDimVals3' '\t' 'StimDimVals4' '\t' 'StimDimVals5' '\t' ...
    'StimLocationX' '\t' 'StimLocationY' '\t' 'StimLocationZ' '\t' ...
    'StimRotationX' '\t' 'StimRotationY' '\t' 'StimRotationZ' '\t' ...
    'StimTrialRewardProb' '\t' 'StimTrialRewardMag' '\t' 'TimesUsedInBlock' ...
    '\t' 'isRelevant' '\t' 'SetLocation' '\t' 'SetRotation' '\n'];

cnd_header=['TrialName' '\t' 'TrialCode' '\t' 'ContextNum' '\t' 'ContextName' '\t' ...
    'Stim1' '\t'  'Stim2' '\t'  'Stim3' '\t'  'Stim4' '\t'  'Stim5' '\t'  'Stim6' '\t' ...
    'Stim7' '\t'  'Stim8' '\t'  'Stim9' '\t'  'Stim10' '\t'  'Stim11' '\t'  'Stim12' '\t' ...
    'Stim13' '\t'  'Stim14' '\n'];

%% Generate Different Dimension per context sets
file_base_name = 'CFS_DF01_Set';%DF is for the diffculty mode
for file = 1:num_sets_to_generate
    
    %---Pick 2 contexts & Pick 2 targets---%
    these_context_nums = context_nums(randperm(length(context_nums)));
    these_context_nums = these_context_nums(1:2);
    
    these_dims = 1:4;
    these_dims = these_dims(randperm(4));
    these_dims = these_dims(1:2);
    
    if these_dims(1) == 1 %shape
        target1 = [shapes{randi(length(shapes))} '_P00_C6000000_6000000_T00_A00_E00.fbx'];
    elseif these_dims(1) == 2 %pattern
        target1 = ['S00_' patterns{randi(length(patterns))} '_C7000000_5000000_T00_A00_E00.fbx'];
    elseif  these_dims(1) == 3 %color
        target1 = ['S00_P00_' patternless_colors{randi(length(patternless_colors))} '_T00_A00_E00.fbx'];
    elseif these_dims(1) == 4 %arms
        target1 = ['S00_P00_C6000000_6000000_T00_' arms{randi(length(arms))} '.fbx'];
    end
    if these_dims(2) == 1 %shape
        target2 = [shapes{randi(length(shapes))} '_P00_C6000000_6000000_T00_A00_E00.fbx'];
    elseif these_dims(2) == 2 %pattern
        target2 = ['S00_' patterns{randi(length(patterns))} '_C7000000_5000000_T00_A00_E00.fbx'];
    elseif  these_dims(2) == 3 %color
        target2 = ['S00_P00_' patternless_colors{randi(length(patternless_colors))} '_T00_A00_E00.fbx'];
    elseif these_dims(2) == 4 %arms
        target2 = ['S00_P00_C6000000_6000000_T00_' arms{randi(length(arms))} '.fbx'];
    end
    
    target_full_path1 = [Determine_Quaddle_Path(target1,stim_path,false) target1];
    target_full_path2 = [Determine_Quaddle_Path(target2,stim_path,false) target2];
    
    [dimVals1,neutralVals1] = ParseQuaddleName(target1,patternless_colors,arms);
    [dimVals2,neutralVals2] = ParseQuaddleName(target2,patternless_colors,arms);
    
    neutral = 'S00_P00_C6000000_6000000_T00_A00_E00.fbx';
    neutral_full_path = [Determine_Quaddle_Path(neutral,stim_path,false) neutral];
    
    %---First Generate the BlockDef----%
    filebase = [file_base_name num2str(file)];
    mkdir([current_dir '\' filebase]);
    cd([current_dir '\'  filebase])
    
    blockfID = fopen([filebase '_BlockDef.txt'],'w');
    bdstr1 = ['\nBlockDef[]	blockDefs	[...' '\n'];
    fprintf(blockfID,bdstr1);
    
    %---Second Generate the Item and Condition files for each Block----%
    for numD = 1:length(num_distractor_Quaddles)
        
        if numD == 1%for fam block
            num_trials1 = 2*fam_num_trials;
            num_trials2 = min_num_trials;
        elseif numD  == length(num_distractor_Quaddles)%for last block do lots of trials so won't finish block before stop working
            num_trials1 = 4*max_num_trials;
            num_trials2 = 4*max_num_trials;
        else
            num_trials1 = min_num_trials;
            num_trials2 = max_num_trials;
        end
        
        blockpath = [itemCND_path filebase '\\\\' filebase '_D' num2str(num_distractor_Quaddles(numD))];
        bdstr2 = ['{"BlockID": "blockid.' [filebase '_D' num2str(num_distractor_Quaddles(numD))] '", "TrialRange": [' num2str(num_trials1) ',' num2str(num_trials2) '], "TrialDefPath": "' ...
            blockpath '", "ActiveFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "NumIrrelevantObjectsPerTrial": [0,0] ,"NumIrrelevantObjectsPerBlock": 0,...\n' ...
            '"RuleArray": [{"RelevantFeatureTemplate": [[0],[0],[0],[0],[0]], "ContextNums": [0], "RewardProb": 1.0, "RewardMag": ' num2str(RewardMag) '}...\n' ...
            '],...' '\n'...
            '"BaseRewardProb": 0, "BaseRewardMag": ' num2str(RewardMag) '},...\n'];
        
        if numD == length(num_distractor_Quaddles)
            bdstr3 =  ']\n//end of blockDef[]';
        else
            bdstr3 = '\n\n';
        end
        fprintf(blockfID,bdstr2);
        fprintf(blockfID,bdstr3);
        
        
        %-----Generate Item File---%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_Itm.txt'],'w');
        fprintf(fileID,itm_header);
        item_num = 0;
        item_locc_index = NaN(2,3,length(xzlocs));
        for contxt = 1:2
            for Q = 1:3
                for locs = 1:length(xzlocs)
                    item_num = item_num+1;
                    item_locc_index(contxt,Q,locs) = item_num;
                    if Q == 1%target1
                        if contxt == 1
                            str = [num2str(item_num) '\t' target1 '\t' target_full_path1 '\t' ...
                                num2str(dimVals1(1)) '\t'  num2str(dimVals1(2)) '\t' num2str(dimVals1(3)) '\t' num2str(dimVals1(4)) '\t' num2str(dimVals1(5)) '\t' ...
                                num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                                '80' '\t' '10' '\t' '10' '\t' ...
                                num2str(RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                                'true' '\t' 'true' '\t' 'true' '\n'];
                        else
                            str = [num2str(item_num) '\t' target1 '\t' target_full_path1 '\t' ...
                                num2str(dimVals1(1)) '\t'  num2str(dimVals1(2)) '\t' num2str(dimVals1(3)) '\t' num2str(dimVals1(4)) '\t' num2str(dimVals1(5)) '\t' ...
                                num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                                '80' '\t' '10' '\t' '10' '\t' ...
                                num2str(1-RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                                'true' '\t' 'true' '\t' 'true' '\n'];
                        end
                    elseif Q == 2 %target2
                        if contxt == 2
                            str = [num2str(item_num) '\t' target2 '\t' target_full_path2 '\t' ...
                                num2str(dimVals1(1)) '\t'  num2str(dimVals1(2)) '\t' num2str(dimVals1(3)) '\t' num2str(dimVals1(4)) '\t' num2str(dimVals1(5)) '\t' ...
                                num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                                '80' '\t' '10' '\t' '10' '\t' ...
                                num2str(RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                                'true' '\t' 'true' '\t' 'true' '\n'];
                        else
                            str = [num2str(item_num) '\t' target2 '\t' target_full_path2 '\t' ...
                                num2str(dimVals2(1)) '\t'  num2str(dimVals2(2)) '\t' num2str(dimVals2(3)) '\t' num2str(dimVals2(4)) '\t' num2str(dimVals2(5)) '\t' ...
                                num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                                '80' '\t' '10' '\t' '10' '\t' ...
                                num2str(1-RewardProb) '\t' num2str(RewardMag) '\t' '9999' '\t' ...
                                'true' '\t' 'true' '\t' 'true' '\n'];
                        end
                    else %distractor
                        str = [num2str(item_num) '\t' neutral '\t' neutral_full_path '\t' ...
                            '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                            num2str(xzlocs(1,locs)) '\t' num2str(y_default) '\t'  num2str(xzlocs(2,locs)) '\t' ...
                            '80' '\t' '10' '\t' '10' '\t' ...
                            '0' '\t' '3' '\t' '9999' '\t' ...
                            'true' '\t' 'true' '\t' 'true' '\n'];
                    end
                    fprintf(fileID,str);
                end
            end
        end
        fclose(fileID);
        
        %----Generate Condition file-----%
        fileID = fopen([filebase '_D' num2str(num_distractor_Quaddles(numD)) '_CND.txt'],'w');
        fprintf(fileID,cnd_header);
        
        num_p_blocks = ceil(num_trials2/psuedo_random_block_size/2);%pseudorandomize doesn't have to be perfect but should be good
        
        context_dist = [];
        for npb = 1:num_p_blocks
            this_dist = [ones(1,psuedo_random_block_size) 2*ones(1,psuedo_random_block_size)];
            this_dist = this_dist(randperm(2*psuedo_random_block_size));
            context_dist = [context_dist this_dist];
        end
        
        for t = 1:num_trials2
            
            item_index = randperm(length(xzlocs));
            current_context = context_dist(t);
            
            str1 = ['TrialNum' num2str(t+1000*numD) '\t' num2str(t+1000*numD) '\t'];
            str2 = ['ContextNum' num2str(these_context_nums(current_context)) '\t'  num2str(these_context_nums(current_context))  '\t'];
            
            str3 = num2str(item_locc_index(current_context,current_context,item_index(1)));
            if num_distractor_Quaddles(numD) > 0
                str3 = [str '\t'];
            end
            
            str4 = [];
            for stim = 1:num_distractor_Quaddles(numD)
                str4= [str4  num2str(item_locc_index(2,item_index(stim+1)))];
                if stim == num_distractor_Quaddles(numD)
                    str4 = [str4 '\t'];
                end
                fprintf(fileID,[str1 str2 str3  str4 '\n']);
            end
            fclose(fileID);
        end
    end
    fclose(blockfID);
end