%Code reads in blockdefs from CFS sets to ensure generated distributions of
%stimuli and contexts are what they were intended to be
%Written by Seth Konig 7/10/19

prob_switch = [1/3 1/4 3/16 1/8 1/10];%context history, probabiltiy of swithching
num_distractor_Quaddles = [3 6 9 12];
data_dir = 'Z:\Seth\CFSDC_Test_Files\';

%% Locations
spatial_scale = 0.05;
y_default = 0.4;
x_spacing = 4;%also exlucde within 2 units of center, arms make us need more spacing on x-axis
z_spacing = 3;%also exlucde within 1.5 units of center
x_max = 8;%magnitude
z_max = 4.5;%magnitude

unique_x_locations = -x_max:x_spacing:x_max;
unique_z_locations = -z_max:z_spacing:z_max;

%% Feature Dimensions and values
shapes = {'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

patterned_colors =   {'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000','C7070240_5000000','C7070286_5000000', 'C7070335_5000000'};
%patternless_colors = {'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070239_6070239','C6070287_6070287', 'C6070335_6070335'};
patternless_colors = {'C7070014_7070014', 'C7070059_7070059', 'C7070106_7070106', 'C7070148_7070148', 'C7070194_7070194','C7070240_7070240','C7070286_7070286', 'C7070335_7070335'};
gray_pattern_color = 'C7000000_5000000';

arms = {'A00_E01', 'A00_E02','A00_E03', 'A01_E00', 'A01_E01', 'A01_E02', 'A01_E03', 'A02_E00', 'A02_E01', 'A02_E02', 'A02_E03'};

neutral_Quaddle = 'S00_P00_C7000000_7000000_T00_A00_E00';
%% Import BlockDefs

file_list = dir(data_dir);
session_data = cell(1,length(file_list));
for file = 1:length(file_list)
    if contains(file_list(file).name,'FLU')
        session_data{file} = ImportFLU_DataV2(file_list(file).name,data_dir,'slim');
    end
end

%% Parse Blockdefs

num_sess = length(session_data);

sess_context_counts = cell(1,num_sess);
sess_context_history_counts = cell(1,num_sess);

sess_target_location_counts = cell(1,num_sess);
sess_distractor_location_counts = cell(1,num_sess);

sess_numD_counts = cell(1,num_sess);

sess_distractor_counts = cell(1,num_sess);
for sess = 1:length(session_data)
    if ~isempty(session_data{sess})
        this_blockdef = session_data{sess}.block_def;
        num_blocks = length(this_blockdef);
        
        
        %get all the data
        presented_contexts = cell(1,num_blocks);
        target_locations = cell(1,num_blocks);
        distractor_locations = cell(1,num_blocks);
        numD = cell(1,num_blocks);
        for b = 1:num_blocks
            these_trial_defs = this_blockdef(b).TrialDefs;
            num_trials = length(these_trial_defs);
            presented_contexts{b} = NaN(1,num_trials);
            target_locations{b} = NaN(2,num_trials);
            distractor_locations{b} = [];
            numD{b} = NaN(1,num_trials);
            
            for t = 1:num_trials
                presented_contexts{b}(t) = these_trial_defs(t).ContextNum;
                target_locations{b}(1,t) =  these_trial_defs(t).relevantObjects(1).StimLocation.x;
                target_locations{b}(2,t) =  these_trial_defs(t).relevantObjects(1).StimLocation.z;
                
                numD{b}(t) = length(these_trial_defs(t).relevantObjects)-1;
                for nd = 2:length(these_trial_defs(t).relevantObjects)-1
                    loc = [these_trial_defs(t).relevantObjects(nd).StimLocation.x; ...
                        these_trial_defs(t).relevantObjects(nd).StimLocation.z];
                    distractor_locations{b} = [distractor_locations{b} loc];
                end
            end
        end
        
        %count all of the data
        unique_contexts = unique(presented_contexts{1});
        unique_x_locations = unique(target_locations{2}(1,:));
        unique_z_locations = unique(target_locations{2}(2,:));
        
        sess_context_counts{sess} = NaN(2,num_blocks);
        sess_context_history_counts{sess} = zeros(5,num_blocks);
        
        sess_target_location_counts{sess} = zeros(length(unique_z_locations),length(unique_x_locations));
        sess_distractor_location_counts{sess} = zeros(length(unique_z_locations),length(unique_x_locations));
        sess_numD_counts{sess} = zeros(1,5);
        for b = 1:num_blocks
            
            %count the nunber of distractors per block
            for nD = 1:length(num_distractor_Quaddles) + 1
                if nD == 1
                    sess_numD_counts{sess}(nD) =  sess_numD_counts{sess}(nD) + sum(numD{b} == 0);
                else
                    sess_numD_counts{sess}(nD) = sess_numD_counts{sess}(nD) + sum(numD{b} == num_distractor_Quaddles(nD-1));
                end
            end
            
            %count of the number of contexts per block
            for cntxt = 1:2
                sess_context_counts{sess}(cntxt,b) = sum(presented_contexts{b} == unique_contexts(cntxt));
            end
            
            %get location counts
            for dt = 1:length(distractor_locations{b})
                xind = find(distractor_locations{b}(1,dt) == unique_x_locations);
                yind = find(distractor_locations{b}(2,dt) == unique_z_locations);
                sess_distractor_location_counts{sess}(yind,xind) = sess_distractor_location_counts{sess}(yind,xind) + 1;
            end
            
            for t = 1:length(presented_contexts{b})
                xind = find(target_locations{b}(1,t) == unique_x_locations);
                yind = find(target_locations{b}(2,t) == unique_z_locations);
                sess_target_location_counts{sess}(yind,xind) = sess_target_location_counts{sess}(yind,xind) + 1;
                
                %context trial history
                if t >= 5
                    if all(presented_contexts{b}(t-4:t-1) == presented_contexts{b}(t))
                        nback = 4;
                    elseif all(presented_contexts{b}(t-3:t-1) == presented_contexts{b}(t))
                        nback = 3;
                    elseif all(presented_contexts{b}(t-2:t-1) == presented_contexts{b}(t))
                        nback = 2;
                    elseif all(presented_contexts{b}(t-1) == presented_contexts{b}(t))
                        nback = 1;
                    else
                        nback = 0;
                    end
                else
                    nback = NaN;
                end
                if ~isnan(nback)
                    sess_context_history_counts{sess}(nback+1,b) =  sess_context_history_counts{sess}(nback+1,b)+1;
                end
            end
        end
    end
end

%% Combined Data Across Sets
all_context_sess_counts = [];%proportion of counts of context1
all_fam_context_history = [];
all_search_context_history = [];

all_target_locations = zeros(length(unique_z_locations),length(unique_x_locations));
all_distractor_locations = zeros(length(unique_z_locations),length(unique_x_locations));
location_sess_count = 0;

all_numD_counts = [];
for sess = 1:num_sess
    if ~isempty(sess_context_counts{sess})
        if length(sess_context_counts{sess}) < 12
            error('Where are all my blocks')
        end
        
        %combine context information
        these_counts = sess_context_counts{sess};
        these_counts = these_counts./sum(these_counts);
        all_context_sess_counts = [all_context_sess_counts; these_counts(1,:)];
        
        for b = 1:size(sess_context_history_counts{sess},2)
            these_counts = sess_context_history_counts{sess}(:,b);
            these_counts = these_counts/sum(these_counts);
            if rem(b,2) == 1%fam block
                all_fam_context_history = [all_fam_context_history; these_counts'];
            else %search block
                all_search_context_history = [all_search_context_history; these_counts'];
            end
        end
        
        %combine spatial information
        these_locations = sess_target_location_counts{sess};
        these_locations = these_locations/sum(these_locations(:));
        all_target_locations = all_target_locations + these_locations;
        
        these_locations = sess_distractor_location_counts{sess};
        these_locations = these_locations/sum(these_locations(:));
        all_distractor_locations = all_distractor_locations + these_locations;
        
        location_sess_count = location_sess_count + 1;
        
        %get distractor counts
        total_count = sess_numD_counts{sess};
        total_count = total_count/sum(total_count);
        all_numD_counts = [all_numD_counts; total_count];
    end
end

all_target_locations = all_target_locations/location_sess_count;
all_distractor_locations = all_distractor_locations/location_sess_count;
%% Plot Basic Results
figure
subplot(2,3,1)
bar(mean(all_context_sess_counts));
hold on
errorbar(mean(all_context_sess_counts),std(all_context_sess_counts),'k','linestyle','none')
plot([0 13],[0.5 0.5],'r--')
hold off
xlabel('Block #')
ylabel('Proportion of Trials Context 1')
box off
title('All Context Counts')

subplot(2,3,2)
bar(0:4,mean(all_fam_context_history));
hold on
errorbar(0:4,mean(all_fam_context_history),std(all_fam_context_history),'k','linestyle','none');
for b = 1:5
    plot(b-1,1/2^b,'r*')
end
hold off
box off
xlabel('# of Trials with Same Context')
ylabel('Proportion of Trials')
title('Context History: Familarization blocks')

subplot(2,3,3)
bar(0:4,mean(all_search_context_history));
hold on
errorbar(0:4,mean(all_search_context_history),std(all_search_context_history),'k','linestyle','none');
for b = 1:5
    plot(b-1,prob_switch(b),'r*')
end
plot([0 5],[0.1 0.1],'r--')
hold off
box off
xlabel('# of Trials with Same Context')
ylabel('Proportion of Trials')
title('Context History: Search blocks')

subplot(2,3,4);
bar(mean(all_numD_counts));
hold on
errorbar(1:5,mean(all_numD_counts),std(all_numD_counts),'k','linestyle','none');
hold off
box off
xticks(1:5)
xticklabels({'0','3','6','9','12'})
ylabel('Proportion of Trails')
xlabel('# of Distractors')

subplot(2,3,5)
imagesc(all_target_locations)
caxis([0.04 0.06])
box off
colorbar
title('Target Location Distribution')

subplot(2,3,6)
imagesc(all_distractor_locations)
caxis([0.04 0.06])
box off
colorbar
title('Distractor Location Distribution')

%% Get Distribution of Presetnation Frequencies of All Quaddles

all_Quaddles_shown = cell(num_sess,12);

for sess = 1:num_sess
    if ~isempty(session_data{sess})
        
        this_blockdef = session_data{sess}.block_def;
        num_blocks = length(this_blockdef);
        
        all_Quaddles_shown{sess,b} = NaN(3,27);
        
        for b = 1:num_blocks%fam blocks
            fam_block_trials = this_blockdef(b).TrialDefs;
            target_Quaddle_identitys = NaN(length(fam_block_trials),5);
            which_context = NaN(1,length(fam_block_trials));
            
            %---Determine Which Targets Where Shown in Familiarization Blocks---%
            if rem(b,2) == 1
                for t = 1:length(fam_block_trials) %block 0, loop through all fam block to make sure got target
                    dimVals= ParseQuaddleName(fam_block_trials(t).relevantObjects(1).StimName,patternless_colors,arms);
                    if dimVals(2) ~= 0%patterned object
                        dimVals= ParseQuaddleName(fam_block_trials(t).relevantObjects(1).StimName,patterned_colors,arms);
                    end
                    target_Quaddle_identitys(t,:) = dimVals;
                    which_context(t) = fam_block_trials(t).ContextNum;
                end
                contexts = unique(which_context);
                
                target1 = target_Quaddle_identitys(which_context == contexts(1),:);
                target1 = target1(1,:);
                target2 = target_Quaddle_identitys(which_context == contexts(2),:);
                target2 = target2(1,:);
                total_num_features = sum(target1 ~= 0) + sum(target2 ~= 0); %+1 for neutral
                
                all_Quaddles = [target1; target2];
                all_Quaddle_types = [{'T1'};{'T2'}];
                
                %2D decomposition
                for t = 1:2
                    if t == 1
                        this_target = target1;
                    elseif t == 2
                        this_target = target2;
                    end
                    non_neutral_features = find(this_target ~=0);
                    for nf = 1:length(non_neutral_features)
                        for nnf = 1:length(non_neutral_features)
                            if nnf > nf
                                distractor = zeros(1,5);
                                distractor(non_neutral_features(nf)) = this_target(non_neutral_features(nf));
                                distractor(non_neutral_features(nnf)) = this_target(non_neutral_features(nnf));
                                all_Quaddles = [all_Quaddles; distractor];
                                all_Quaddle_types = [all_Quaddle_types; {['T' num2str(t) char('a'+nf-1) 'T' num2str(t) char('a'+nnf-1)]}];
                            end
                        end
                    end
                end
                
                %1D decomposition
                for t = 1:2
                    if t == 1
                        this_target = target1;
                    elseif t == 2
                        this_target = target2;
                    end
                    non_neutral_features = find(this_target ~=0);
                    for nnf = 1:length(non_neutral_features)
                        distractor = zeros(1,5);
                        distractor(non_neutral_features(nnf)) = this_target(non_neutral_features(nnf));
                        all_Quaddles = [all_Quaddles; distractor];
                        all_Quaddle_types = [all_Quaddle_types; {['T' num2str(t) char('a'+nnf-1)]}];
                    end
                end
                
                %3D recomposition
                non_neutral_features1 = find(target1 ~=0);
                non_neutral_features2 = find(target2 ~=0);
                for dim1 = 1:2
                    for dim2 = 1:2
                        for dim3 = 1:2
                            if dim1 ~= dim2  || dim2 ~= dim3
                                distractor = zeros(1,5);
                                distractor_name = [];
                                if dim1 == 1
                                    distractor(non_neutral_features1(1)) = target1(non_neutral_features1(1));
                                    distractor_name = [distractor_name 'T1a'];
                                else
                                    distractor(non_neutral_features2(1)) = target2(non_neutral_features2(1));
                                    distractor_name = [distractor_name 'T2a'];
                                end
                                if dim2 == 1
                                    distractor(non_neutral_features1(2)) = target1(non_neutral_features1(2));
                                    distractor_name = [distractor_name 'T1b'];
                                else
                                    distractor(non_neutral_features2(2)) = target2(non_neutral_features2(2));
                                    distractor_name = [distractor_name 'T2b'];
                                end
                                if dim3 == 1
                                    distractor(non_neutral_features1(3)) = target1(non_neutral_features1(3));
                                    distractor_name = [distractor_name 'T1c'];
                                else
                                    distractor(non_neutral_features2(3)) = target2(non_neutral_features2(3));
                                    distractor_name = [distractor_name 'T2c'];
                                end
                                all_Quaddles = [all_Quaddles; distractor];
                                all_Quaddle_types = [all_Quaddle_types; {distractor_name}];
                            end
                        end
                    end
                end
                
                %2D recomposition
                non_neutral_features1 = find(target1 ~=0);
                non_neutral_features2 = find(target2 ~=0);
                for t = 1:length(non_neutral_features1)
                    for tt = 1:length(non_neutral_features2)
                        if t ~= tt
                            distractor = zeros(1,5);
                            distractor(non_neutral_features1(t)) = target1(non_neutral_features(t));
                            distractor(non_neutral_features2(tt)) = target2(non_neutral_features2(tt));
                            all_Quaddles = [all_Quaddles; distractor];
                            all_Quaddle_types = [all_Quaddle_types; {['T1' char('a'+t-1) 'T2' char('a' + tt-1)]}];
                        end
                    end
                end
                
                
                all_Quaddles =[all_Quaddles; zeros(1,5)];
                all_Quaddle_types = [all_Quaddle_types; {'neutral'}];
            end
            
            
            %---Determine Which Quaddles where Shown On Each Trial---%
            this_block_trials = this_blockdef(b).TrialDefs;
            num_trials = length(this_block_trials);
            Quaddles_shown = zeros(num_trials,27);
            for t =1:num_trials
                num_objs = length(this_block_trials(t).relevantObjects);
                for o = 1:num_objs
                    dimVals= ParseQuaddleName(this_block_trials(t).relevantObjects(o).StimName,patternless_colors,arms);
                    if dimVals(2) ~= 0%patterned object
                        dimVals= ParseQuaddleName(this_block_trials(t).relevantObjects(o).StimName,patterned_colors,arms);
                    end
                    
                    equiv = (all_Quaddles == dimVals);
                    identity = find(all(equiv'));
                    Quaddles_shown(t,identity) = Quaddles_shown(t,identity)+1;
                end
            end
            
            prop_Quaddles_shown = zeros(3,27);
            for cnt = 0:2
                for id = 1:27
                    prop_Quaddles_shown(cnt+1,id) = sum(Quaddles_shown(:,id) == cnt);
                end
            end
            prop_Quaddles_shown = prop_Quaddles_shown/num_trials;
            all_Quaddles_shown{sess,b} = prop_Quaddles_shown;
        end
    end
end
%% Combined Data Across Sets
all_fam_Quaddles_shown = zeros(3,27);
all_search_Quaddles_shown = zeros(3,27);
num_blocks = 0;
for sess = 1:num_sess
    for b = 1:size(all_Quaddles_shown,2)
        if ~isempty(all_Quaddles_shown{sess,b})
            if rem(b,2) == 1 %fam blocks
                all_fam_Quaddles_shown = all_fam_Quaddles_shown + all_Quaddles_shown{sess,b};
            else
                all_search_Quaddles_shown = all_search_Quaddles_shown + all_Quaddles_shown{sess,b};
            end
            num_blocks = num_blocks + 1;
        end
    end
end
all_fam_Quaddles_shown = all_fam_Quaddles_shown/num_blocks;
all_search_Quaddles_shown = all_search_Quaddles_shown/num_blocks;
%% Plot Average of Quaddles shown per trial
figure
subplot(1,2,1)
imagesc(all_fam_Quaddles_shown)
hold off
box off
xlabel('Quaddle Identity')
yticks(1:3)
yticklabels({'0','1','2'})
ylabel('Distribution of Quaddles per Trial')
colorbar
axis square
title('Familiarization Trials')

subplot(1,2,2)
imagesc(all_search_Quaddles_shown)
hold off
box off
xlabel('Quaddle Identity')
yticks(1:3)
yticklabels({'0','1','2'})
ylabel('Distribution of Quaddles per Trial')
colorbar
axis square
title('Search Trials')

